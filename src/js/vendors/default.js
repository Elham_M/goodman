/**
 * Created by Allen on 21/01/2018.
 */

$(document).ready(function(){
    var $body = $('body');
	
	$body
		.on('click', '.off_canvas_trigger', function(event){
			event.preventDefault();
			var off_canvas_menu = $(this).closest('.off_canvas_menu');
			if ($(this).data('trigger') && $('#'+$(this).data('trigger')).length > 0) {
				off_canvas_menu = $('#'+$(this).data('trigger'));
			}
			var off_canvas_wrapper = off_canvas_menu.closest('.off_canvas_wrapper');
			if (off_canvas_wrapper.hasClass('off_canvas_expand')) {
				off_canvas_wrapper.removeClass('off_canvas_expand off_canvas_expand_left off_canvas_expand_right');
			} else {
				off_canvas_wrapper.addClass('off_canvas_expand');
				if (off_canvas_menu.hasClass('off_canvas_menu_right')) {
					off_canvas_wrapper.addClass('off_canvas_expand_right');
				} else {
					off_canvas_wrapper.addClass('off_canvas_expand_left');
				}
			}
		})
		.on('click', '.pull_down_trigger', function(event){
			event.preventDefault();
			var pull_down_wrapper = $(this).closest('.pull_down_wrapper'),
				off_canvas_menu = $('#main_menu_container');
			if ($(this).data('trigger') && $('#'+$(this).data('trigger')).length > 0) {
				pull_down_wrapper = $('#'+$(this).data('trigger'));
			}
			if (pull_down_wrapper.hasClass('pull_down_expand')) {
				pull_down_wrapper.animate({
					'height':0
				},500,function(){
					$(this).removeClass('pull_down_expand').css({'height':''});
					off_canvas_menu.show();
				});
			} else {
				off_canvas_menu.hide();
				pull_down_wrapper.animate({
					'height':pull_down_wrapper.find('.pull_down_container').height()
				},500,function(){
					$(this).addClass('pull_down_expand').css({'height':''});
				});
			}
		})
		.on('click', '#top_sub_menu_login_container .drop_down_trigger', function(event){
			var drop_down_parent = $(this).closest('.drop_down_parent'),
				off_canvas_menu = $('#main_menu_container');
			if (drop_down_parent.hasClass('drop_down_expand')) {
				off_canvas_menu.show();
			} else {
				off_canvas_menu.hide();
			}
		})

		.on('click', '.property_map_trigger', function(){
			$body.addClass('property_map_active');
		})
		.on('click', '.property_detail_trigger', function(){
			$body.removeClass('property_map_active');
		})
		.on('click', '.property_list_trigger', function(){
			$body.removeClass('property_map_active');
		});

});
