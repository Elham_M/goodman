/*================================================================================
	goodman.control.gallerySliding
================================================================================*/

(function($){

	window.goodman.control.gallerySliding = $.extend(true, window.goodman.control.gallerySliding || {}, {

		galleryRotateIID: -1,
		
		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			$('.gallery_container').each(function(){
				var gallery_container = $(this),
					image_list = gallery_container.find('.gallery_image_container');
				
				if (image_list.length > 1 && $('.gallery_selector_container', gallery_container).length === 0 ) {
					var gallery_navigation_container = $('<div />',{
						'class':'gallery_selector_container'
					});
					gallery_navigation_container.appendTo(gallery_container);
					for(var i=0;i<image_list.length;i++) {
						$('<div />',{
							'class':'gallery_selector'
						}).appendTo(gallery_navigation_container);
					}
					gallery_navigation_container.find('.gallery_selector').eq(0).addClass('gallery_selector_selected');
					gallery_container
						.data('current_index',0)
						.data('total_count',image_list.length)
						.data('auto_advance',true);
					
					$('<div />',{
						'class':'gallery_navigator_vignette'
					}).appendTo(gallery_container);
					$('<div />',{
						'class':'gallery_navigator gallery_navigator_previous'
					}).appendTo(gallery_container);
					$('<div />',{
						'class':'gallery_navigator gallery_navigator_next'
					}).appendTo(gallery_container);

				}
			});
			
			this.enable();
		},

		enable: function() {
			var self = this;
			
			this.$body
				.off('click.gallery')
				.on('mouseenter.gallery', '.gallery_container', function(e){
					$(this).closest('.gallery_container').data('auto_advance',false);
				})
				.on('mouseleave.gallery', '.gallery_container', function(e){
					$(this).closest('.gallery_container').data('auto_advance',true);
				})
				.on('click.gallery','.gallery_container .gallery_selector',function(e){
					e.preventDefault();
					e.stopPropagation();
					if(!$(this).hasClass('gallery_selector_selected')) {
						var selector_container = $(this).closest('.gallery_selector_container'),
							gallery_container = $(this).closest('.gallery_container'),
							target_index = selector_container.find('.gallery_selector').index($(this));
						selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
						selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
						gallery_container.css('text-indent',(-100*target_index)+'%');
					}
				})
				.on('click.gallery','.gallery_container .gallery_navigator_previous',function(e){
					var gallery_container = $(this).closest('.gallery_container'),
						selector_container = gallery_container.find('.gallery_selector_container'),
						target_index = selector_container.find('.gallery_selector').index(selector_container.find('.gallery_selector_selected'));
					e.preventDefault();
					e.stopPropagation();
					target_index--;
					if (target_index < 0) {
						target_index = selector_container.find('.gallery_selector').length - 1;
					}
					selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
					selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
					gallery_container.css('text-indent',(-100*target_index)+'%');
				})
				.on('click.gallery','.gallery_container .gallery_navigator_next',function(e){
					var gallery_container = $(this).closest('.gallery_container'),
						selector_container = gallery_container.find('.gallery_selector_container'),
						target_index = selector_container.find('.gallery_selector').index(selector_container.find('.gallery_selector_selected'));
					e.preventDefault();
					e.stopPropagation();
					target_index++;
					if (target_index > selector_container.find('.gallery_selector').length - 1) {
						target_index = 0;
					}
					selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
					selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
					gallery_container.css('text-indent',(-100*target_index)+'%');
				});
			
			clearInterval(this.galleryRotateIID);
			this.galleryRotateIID = setInterval( self.autoAdvance, goodman.config.galleryAutoRotateDelay );
			
		},
		
		disable: function() {
			this.$body.off('click.gallery');
			clearInterval(this.galleryRotateIID);
		},
		
		autoAdvance: function() {
			$('.gallery_container').each(function(){
				var gallery_container = $(this);
				if ( gallery_container.data('total_count') > 1 && gallery_container.data('auto_advance') ) {
					$('.gallery_navigator_next', gallery_container).trigger('click');
				}
			});
		},
		
		
	});

})(jQuery);


