/*================================================================================
	goodman.interface.map
================================================================================*/

(function($){

	window.goodman.interface.map = $.extend(true, window.goodman.interface.map || {}, {

		init: function() {
			var self = this;
			this.container = $('.property_map_container');
			this.typeContainer = $('#map-type-selector-container');
			this.disclaimerContainer = $('#map-disclaimer-container');
			goodman.observer.subscribe('map-start', self.start, self);
			goodman.observer.subscribe('map-type', self.changeType, self);
			this.render();
			this.enable();
		},

		render: function() {
			this.typeContainer.html( goodman.template['property-map-type'].hbs({types:goodman.config.mapTypes}) );
		},

		enable: function() {
			var self = this;

			this.typeContainer
				.on('click', '.property_map_type_link', function(){
					var newType = $(this).data('value');
					goodman.observer.publish('set-map-type', newType);
				});

			this.container
				.on('click', '.esriAttribution', function(){
					if ($(this).hasClass('esriAttributionOpen')) {
						self.disclaimerContainer.css({'bottom': $(this).height() + 5});
					} else {
						self.disclaimerContainer.css({'bottom': ''});
					}

				});
		},

		start: function() {
			console.log("map disclaimer section")
			if (!goodman.config.isInternal && goodman.config.disclaimerURL) {
				$('<div class="map-disclaimer">Images displayed in this map are subject to <a href="'+goodman.config.disclaimerURL+'" target="_blank">this disclaimer</a></div>').appendTo(this.disclaimerContainer);
			}
		},

		changeType: function(type) {
			var self = this;
			$('.property_map_type_link',this.typeContainer)
				.removeClass('active')
				.filter('[data-value="'+type+'"]')
				.addClass('active');
		},

	});

})(jQuery);
