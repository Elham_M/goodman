/*================================================================================
	goodman.init

	Initialise the application
================================================================================*/

(function($){

	window.goodman.init = $.extend(true, window.goodman.init || {}, {

		init: function() {
			var self = this;
			goodman.util.log('Initialising');
			
			if (smartspaceConfig !== undefined) {
				window.goodman.config = $.extend(window.goodman.config, smartspaceConfig);
			}

			(goodman.util ? goodman.util.init() : goodman.util.log('Skipping goodman.util'));
			(goodman.handlebars ? goodman.handlebars.init() : goodman.util.log('Skipping goodman.handlebars'));

			(goodman.control.dropdown ? goodman.control.dropdown.init() : goodman.util.log('Skipping goodman.control.dropdown '));
			
			(goodman.interface ? goodman.interface.init() : goodman.util.log('Skipping goodman.interface'));
			(goodman.interface.landing ? goodman.interface.landing.init() : goodman.util.log('Skipping goodman.interface.landing'));
			(goodman.interface.propertysearch ? goodman.interface.propertysearch.init() : goodman.util.log('Skipping goodman.interface.propertysearch'));
			(goodman.interface.propertydetail ? goodman.interface.propertydetail.init() : goodman.util.log('Skipping goodman.interface.propertydetail'));
			(goodman.interface.map ? goodman.interface.map.init() : goodman.util.log('Skipping goodman.interface.map'));
			
			goodman.observer.subscribe('get-location', goodman.util.selfGeoLocate, self);
			//goodman.util.selfGeoLocate();
			
		}

	});

})(jQuery);

$( function(){ goodman.init.init(); } );
