/*================================================================================
	goodman.interface.propertydetail
================================================================================*/

(function($){

	window.goodman.interface.propertydetail = $.extend(true, window.goodman.interface.propertydetail || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			this.container = $('#property-detail');
			this.containerScrollpane = $('.scroll-pane', this.container);
			this.popupGalleryContainer = $('#property-gallery-popup');
			this.popupResourceContainer = $('#property-resource-popup');
			this.localitiesContainer = $('#localities-container');
			this.renderAttributes = {};
			this.renderType = 'property';
			this.pidFromUrl = null;

			goodman.observer.subscribe('detail-request', self.request, self);
			goodman.observer.subscribe('detail-open', self.open, self);
			goodman.observer.subscribe('detail-close', self.close, self);
			goodman.observer.subscribe('return-related', self.related, self);
			goodman.observer.subscribe('return-child-properties', self.childProperties, self);

			this.enable();

			this.displayPropertyFromURL();

		},

		render: function( type ) {
			var detailPaneScroll = goodman.util.destroyCustomSlider(this.containerScrollpane);
			if (type === undefined) { type = this.renderType; }
			this.renderType = type;
			switch(type) {
				case 'property':
					this.containerScrollpane.html( goodman.template['property-detail'].hbs(this.renderAttributes) );
					break;
				case 'lease':
					this.containerScrollpane.html( goodman.template['property-detail-lease'].hbs(this.renderAttributes) );
					break;
			}
			this.popupGalleryContainer.html( goodman.template['property-detail-popup-gallery'].hbs({attributes:this.renderAttributes, type:type}) );
			goodman.control.gallery.init();
			goodman.control.overlaypopup.init();
			goodman.util.addCustomSlider(this.containerScrollpane, detailPaneScroll, false, {}, 50);
			this.renderLocalities();
		},

		enable: function() {
			var self = this;
			this.container
				.on('click', '.js-property-detail-close', function(){
					goodman.observer.publish('detail-close');
				})
				.on('click', '.drop_down_trigger', function(){
					var parent = $(this).parent();
					$('.property_detail_tool_bar .drop_down_expand').not(parent).find('.drop_down_trigger').trigger('click');
				})
				.on('click', '[data-propertyview]', function() {
					var propertyview = $(this).data('propertyview');
					switch (propertyview) {
						case 'child':
							self.requestFisrtChild();
							break;
						case 'lease':
							self.requestChildProperties();
							self.render( propertyview );
							break;
						default:
							self.render( propertyview );
					}
			 	})
				.on('click', '[data-scrollto]', function() {
				 	var scrollDest = $( $(this).data('scrollto'), self.container );
				 	goodman.util.scrollTopCustomSlider(self.container, -1 * scrollDest.offset().top);
			 	});

			this.localitiesContainer
				.on('click', '[data-locobjid]', function(){
					var localityid = $(this).data('locobjid'),
						propertyid = $(this).data('propobjid');
					$('.active', self.localitiesContainer).removeClass('active');
					$(this).addClass('active');
					goodman.observer.publish('select-locality', localityid, propertyid );
				});

			this.resourcePopupEnable();
		},

		requestFromPropertyId: function(oid) {
			this.renderAttributes = {};
			goodman.observer.publish('search-detail-from-propertyid', oid, 'detail-open' );


		},

		request: function(oid) {
			var searchObj = { objectid:oid, where:'objectid='+oid };
			this.renderAttributes = {};
			goodman.observer.publish('search-detail', oid, 'detail-open' );
			goodman.observer.publish('get-related', oid, 'resource' );
			goodman.observer.publish('get-related', oid, 'sitefeature' );
			goodman.observer.publish('get-related', oid, 'site' );
			goodman.observer.publish('get-related', oid, 'locality' );
			goodman.observer.publish('get-related', oid, 'localityshortestpath' );
		},

		requestFisrtChild: function() {
			if (this.renderAttributes.childproperties === undefined || this.renderAttributes.childproperties.length === 0) {
				return false;
			}
			var childObjectID = this.renderAttributes.childproperties[0].attributes.objectid;
			this.request(childObjectID);
		},

		open: function(data) {
			var feature = data.features[0],
				attributes = feature.attributes,
				type = this.renderType;

			if ( attributes.micrositeurl || attributes.videourl ) {
				attributes.showToolBar = true;
			}

			this.renderAttributes = $.extend(this.renderAttributes, attributes, {
				isInternal: goodman.config.isInternal,
				displaySizeLease: this.displaySizeLease(attributes),
				sizeUnit: goodman.config.sizeUnit,
				shareBaseUrl: goodman.config.shareBaseUrl});

			if (attributes.parentid !== goodman.config.emptyPropertyID) {
				type = 'lease';
				this.requestChildProperties();
			}
			this.render(type);
			if (!goodman.util.isMobileSize) {
				goodman.util.jumpToCustomSlider(this.container, 0);
			}
			$('#goodman-smartspace-app').addClass('property-detail-open').removeClass('property_map_active');
		},

		close: function() {
			var self = this;
			$('#goodman-smartspace-app').removeClass('property-detail-open');
			this.closeLocalities();
			this.renderType = 'property';
			this.renderAttributes = {};
		},

		/* ===============================================
		 *  Related info
		 * =============================================*/

		related: function( relatedObj ) {
			var features = relatedObj.features,
				type = relatedObj.type,
				attributes,
				mergeObj = {};

			if (features === undefined) { return; }
			if (features.features !== undefined) { features = features.features; }

			if (type === 'resource') {
				mergeObj.resource = {};
				this.renderAttributes.resource = {};
				for(key in features) {
					attributes = features[key].attributes;
					if (mergeObj.resource[attributes.type] === undefined) { mergeObj.resource[attributes.type] = []; }
					mergeObj.resource[attributes.type].push(attributes);
					// Separate brochures and plans
					if ( attributes.type === 'download' ) {
						if ( attributes.title.match(/brochure/i) ) {
							if (mergeObj.resource['brochure'] === undefined) { mergeObj.resource['brochure'] = []; }
							mergeObj.resource['brochure'].push(attributes);
						}
						if ( attributes.title.match(/plan/i) ) {
							if (mergeObj.resource['plan'] === undefined) { mergeObj.resource['plan'] = []; }
							mergeObj.resource['plan'].push(attributes);
						}
					}

				}
			} else if (type === 'sitefeature') {
				mergeObj.sitefeature = [];
				this.renderAttributes.sitefeature = {};
				for(key in features) {
					attributes = features[key].attributes;
					mergeObj.sitefeature.push({name:attributes.title, class:attributes.title.toLowerCase().replace(/\s/g,'-').replace(/Ã©/g,'e') });
				}
			} else {
				mergeObj[type] = [];
				this.renderAttributes[type] = {};
				for(key in features) {
					attributes = features[key].attributes;
					mergeObj[type].push(attributes);
				}
			}

			if (mergeObj.locality && mergeObj.locality.length > 0) {
				mergeObj.locality.splice(4);
				mergeObj.localityColumnSize = 12 / mergeObj.locality.length;
			}

			if ( mergeObj.resource ) {
				if ( mergeObj.resource.brochure || mergeObj.resource.plan || mergeObj.resource.virtualtour || mergeObj.resource['360view'] || mergeObj.resource.webcam || mergeObj.resource.three_d_model || mergeObj.resource.location_map) {
					mergeObj.showToolBar = true;
				}
			}

			this.renderAttributes = $.extend(this.renderAttributes, mergeObj);
			this.mergeLocalities();
			this.render();
		},

		mergeLocalities: function() {
			var keyL, keyD;
			if (this.renderAttributes.locality && this.renderAttributes.localityshortestpath) {
				for(keyL in this.renderAttributes.locality) {
					for(keyD in this.renderAttributes.localityshortestpath) {
						if (this.renderAttributes.locality[keyL].localityid === this.renderAttributes.localityshortestpath[keyD].localityid) {
							this.renderAttributes.locality[keyL].distance = this.renderAttributes.localityshortestpath[keyD].distance;
							this.renderAttributes.locality[keyL].traveltime = this.renderAttributes.localityshortestpath[keyD].traveltime;
							break;
						}
					}
				}
			}
		},


		/* ===============================================
		 * Child/Sibling Properties
		 * =============================================*/

		requestChildProperties: function() {
			var selfID = this.renderAttributes.propertyid,
				selfParentID = this.renderAttributes.parentid,
				parentToHaveID = (selfParentID !== goodman.config.emptyPropertyID)? selfParentID : selfID;
			goodman.observer.publish('get-children', selfID, parentToHaveID, 'return-child-properties' );
		},

		childProperties: function(data) {
			if (data.features) {
				this.renderAttributes = $.extend(this.renderAttributes, {'childproperties':data.features});
				this.render();
			}
		},


		/* ===============================================
		 * Localities
		 * =============================================*/

		renderLocalities: function() {
			this.localitiesContainer.html( goodman.template['property-map-locality'].hbs( this.renderAttributes ) );
			var $locals = $('.property_map_bottom_link', this.localitiesContainer),
				maxHeight = -1;
			$locals.each(function(){
				maxHeight = Math.max(maxHeight, Math.abs($(this).outerHeight()));
			});
			if (maxHeight > 92) {
				$locals.css({'padding-left': 8}).find('.locality-type').hide();
				maxHeight = -1;
				$locals.each(function(){
					maxHeight = Math.max(maxHeight, Math.abs($(this).outerHeight()));
				});
			}

			$locals.css( 'min-height', maxHeight );
		},

		closeLocalities: function() {
			this.localitiesContainer.html( '' );
		},


		/* ===============================================
		 *  Display Size
		 * =============================================*/

		displaySizeLease: function(attributes) {
			var sizeArr = [];
			if (goodman.config.sizeUnit === 'sqft') {
				if (attributes.minleasingsize > 0) { sizeArr.push(goodman.util.numberFormat(goodman.util.sqm2sqft(attributes.minleasingsize))); }
				if (attributes.maxleasingsize > 0 && attributes.minleasingsize !== attributes.maxleasingsize) { sizeArr.push(goodman.util.numberFormat(goodman.util.sqm2sqft(attributes.maxleasingsize))); }
			} else {
				if (attributes.minleasingsize > 0) { sizeArr.push(goodman.util.numberFormat(attributes.minleasingsize)); }
				if (attributes.maxleasingsize > 0 && attributes.minleasingsize !== attributes.maxleasingsize) { sizeArr.push(goodman.util.numberFormat(attributes.maxleasingsize)); }
			}
			return sizeArr.join(' - ') + ' ' + goodman.config.sizeUnit;
		},

		/* ===============================================
		 *  Open resources in light box
		 * =============================================*/

		resourcePopupEnable: function() {
			var self = this;

			this.container
				.on('click', '.property_detail_tool_bar a[target="_blank"]:not([download])', function(e){
					if (goodman.config.isMobileSize) { return; }
					var url = $(this).attr('href');
					e.preventDefault();
					self.resourcePopupOpen(url);
				});

			this.popupResourceContainer
				.on('click', '[href="#copyInput"]', function(e) {
					e.preventDefault();
					var copyText = document.getElementById('urlToCopy');
					copyText.select();
					document.execCommand("copy");
				});
		},

		resourcePopupOpen: function(url) {
			this.popupResourceContainer.html( goodman.template['property-detail-popup-iframe'].hbs({url:url}) );
			if (!document.execCommand) { $('[href="#copyInput"]', this.popupResourceContainer).hide(); }
			this.$body.removeClass('gallery_popup_active').addClass('resource_popup_active');
		},

		/* ===============================================
		 *  Display property details based on URL
		 * =============================================*/

		displayPropertyFromURL: function() {
			var self = this;
			// var urlStr = window.top.location.search.match(/pid=(\d+)/),
			// pid = (urlStr)?urlStr[1]||-1:-1;
			var pid = this.getUrlParameter('pid');

			console.log("displayPropertyFromURL", pid)

			if ( pid === -1 || pid === undefined ) { return; }
			pid = pid.split("/")[0]
			this.pidFromUrl = pid;
			goodman.observer.subscribe('map-start', self.openDetailFromURL, self);
		},

		getUrlParameter: function(sParam) {
			// https://stackoverflow.com/a/21903119/906814 - added by Stephen Lead
	    var sPageURL = window.parent.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }
		},

		openDetailFromURL: function() {
			var self = this;
			goodman.interface.landing.close();
			this.requestFromPropertyId(this.pidFromUrl);
			goodman.observer.unsubscribe('map-start', self.openDetailFromURL, self);
		},

	});

})(jQuery);


