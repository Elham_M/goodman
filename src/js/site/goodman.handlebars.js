/*================================================================================
	goodman.handlebars

	Register Handlebars helpers and partials
================================================================================*/

(function($){
	window.goodman.handlebars = $.extend(true, window.goodman.handlebars || {}, {

		init: function() {

			Handlebars.registerHelper('if_eq', function(a, b, opts) {
				return (a === b)?opts.fn(this):opts.inverse(this);
			});

			Handlebars.registerHelper('if_not_eq', function(a, b, opts) {
				return (a !== b)?opts.fn(this):opts.inverse(this);
			});

			Handlebars.registerHelper('if_bigger', function(a, b, opts) {
				return (a > b)?opts.fn(this):opts.inverse(this);
			});

			/**
			 * Replace all occurrences of `a` with `b`.
			 *
			 * ```handlebars
			 * {{replace "a b a b a b" "a" "z"}}
			 * //=> 'z b z b z b'
			 * ```
			 * @param  {String} `str`
			 * @param  {String} `a`
			 * @param  {String} `b`
			 * @return {String}
			 * @api public
			 */
			Handlebars.registerHelper('replace', function(str, a, b) {
				if (str && typeof str === 'string') {
					if (!a || typeof a !== 'string') return str;
					if (!b || typeof b !== 'string') b = '';
					return str.split(a).join(b);
				}
			});

			Handlebars.registerHelper('hilite', function(str, a, b) {
				if (str && typeof str === 'string') {
					if (!a || typeof a !== 'string') return str;
					if (!b || typeof b !== 'string') b = 'matched_text';
					var re = new RegExp('('+a+')', 'i');
					return new Handlebars.SafeString(str.replace(re, '<span class="'+b+'">$1</span>'));
				}
			});

			Handlebars.registerHelper('linkify', function(str){
				var urlre = /^http(s)?:\/\//i;
				if (urlre.test(str)) {
					str = '<a href="' + str + '" target="_blank" class="external_link">Link</a>';
				}
				return str;
			});

			Handlebars.registerHelper('current_year', function() {
				return new Date().getFullYear();
			});

			// instead of {{> partialName}} use {{partial "templateName"}}
			Handlebars.registerHelper('partial', function (templateName) {
				return new Handlebars.SafeString( goodman.template[templateName].hbs(this) );
			});

			Handlebars.registerHelper("inc", function(value, opts) {
				return parseInt(value) + 1;
			});

			Handlebars.registerHelper ('truncate', function (str, len) {
				if (str && str.length > len && str.length > 0) {
					var new_str = str + " ";
					new_str = str.substr (0, len);
					new_str = str.substr (0, new_str.lastIndexOf(" "));
					new_str = (new_str.length > 0) ? new_str : str.substr (0, len);
					return new Handlebars.SafeString ( new_str +'...' );
				}
				return new Handlebars.SafeString (str);
			});

			Handlebars.registerHelper('pluralize', function(number, single, plural) {
				if (number === 1) { return single; }
				else { return plural; }
			});

			Handlebars.registerHelper('debug', function (context) {
				return new Handlebars.SafeString(
					'<div class="debug">' + JSON.stringify(context) + '</div>'
				);
			});

			Handlebars.registerHelper('dateformat', function(date, longMonth, dateOnly) {
				return new Handlebars.SafeString(
					goodman.util.dateFormat(date, longMonth, dateOnly)
				);
			});

			Handlebars.registerHelper('monthYearFormat', function(date, longMonth) {
				return new Handlebars.SafeString(
					goodman.util.monthYearFormat(date, longMonth)
				);
			});

			Handlebars.registerHelper('if_future', function(date, opts) {
				return goodman.util.isFuture(date)?opts.fn(this):opts.inverse(this);
			});

			Handlebars.registerHelper('numberformat', function(number, decimals, dec_point, thousands_sep) {
				return new Handlebars.SafeString(
					goodman.util.numberFormat(number, decimals, dec_point, thousands_sep)
				);
			});


			/*=======================================================================
			 *	Goodman specific
			 *=====================================================================*/

			Handlebars.registerHelper('cleanurl', function(url) {
				if (url === undefined) { return url; }
				return new Handlebars.SafeString( url.replace(/^:/, '') );
			});

			Handlebars.registerHelper('goodmantypes', function(types) {
				var typeArr = [];
				if (types === undefined) { return ''; }
				types = types.split('');
				for (var i in types) {
					switch (types[i]) {
						case 'I':
							typeArr.push('Industrial');
							break;
						case 'C':
							typeArr.push('Commercial');
							break;
						case 'L':
							typeArr.push('Land');
							break;
					}
				}
				return new Handlebars.SafeString( typeArr.join(', ') );
			});

			Handlebars.registerHelper('goodmanresources', function(resources, className, singular, plural, download) {
				var returnArr = [],
					title, cleanUrl;
				if (resources === undefined || resources.length === 0) { return ''; }

				if (resources.length > 1) {
					returnArr.push('<div class="property_detail_tool_bar_item property_detail_tool_bar_item_'+ className +' drop_down_parent">');
					returnArr.push('<div class="property_detail_tool_bar_item_title drop_down_trigger">'+ plural +'</div>');
					returnArr.push('<div class="drop_down_wrapper"><div class="drop_down_container property_detail_tool_bar_item_sub_list">');
					for (resource in resources) {
						title = resources[resource].title;
						cleanUrl = resources[resource].url.replace(/^:/, '');
						returnArr.push('<a href="'+cleanUrl+'" title="'+title+'" '+((download)?'download target="_blank"':'target="_blank"')+' class="property_detail_tool_bar_item_sub_list_item">'+title+'</a>');
					}
					returnArr.push('</div></div></div>');
				} else {
					title = resources[0].title;
					cleanUrl = resources[0].url.replace(/^:/, '');
					//returnArr.push('<a href="'+cleanUrl+'" title="'+title+'" '+((download)?'download target="_blank"':'target="_blank"')+' class="property_detail_tool_bar_item property_detail_tool_bar_item_'+ className +'">');
					returnArr.push('<a href="'+cleanUrl+'" title="'+title+'" target="_blank" class="property_detail_tool_bar_item property_detail_tool_bar_item_'+ className +'">');
					returnArr.push('<div class="property_detail_tool_bar_item_title">'+ singular +'</div></a>');
				}

				return new Handlebars.SafeString( returnArr.join('') );
			});

			Handlebars.registerHelper('goodmansize', function(size, showUnits) {
				if (showUnits === undefined) { showUnits = true; }
				showUnits = (showUnits)?' '+goodman.config.sizeUnit:'';
				if (goodman.config.sizeUnit === 'sqft') {
					size = goodman.util.sqm2sqft(size);
				}
				return new Handlebars.SafeString(
					goodman.util.numberFormat(size, 0) + showUnits
				);
			});

			Handlebars.registerHelper('goodmansizeunit', function() {
				return new Handlebars.SafeString(
					goodman.config.sizeUnit
				);
			});

			Handlebars.registerHelper('goodmannameurlsafe', function(name) {
				var newName = name;
				try{
					newName = new Handlebars.SafeString(
						name.replace(/\s{2,}/g,"").toLowerCase().split(' ').join('-')
					);
				} catch(err) {
						console.log("There was a problem updating the property name");
				}
				return (newName)
			});

			Handlebars.registerHelper('goodmanaddress', function(address, suburb, region, country, linebreak) {
				var returnArr = [],
					euroAddress = false;

				if (linebreak === undefined) { linebreak = false; }

				/*
					address will end with a comma
					subburb = postcode
					region = subburb

				*/

				if ( $.inArray(country, goodman.config.europeanAddress) > -1 ) {
					euroAddress = true;
				}
				if ( suburb === '' && region === '' ) {
					returnArr.push(address.replace(/,$/, ''));
				} else {
					//returnArr.push(address + ((linebreak)?'<br>':''));
					returnArr.push(address + ',');
					if ( euroAddress ) {
						returnArr.push(suburb);
						returnArr.push(region);
					} else {
						returnArr.push(region);
						returnArr.push(suburb);
					}
				}

				return new Handlebars.SafeString( returnArr.join(' ') );

			});

		}

	});

})(jQuery);
