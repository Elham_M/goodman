/*================================================================================
 goodman.config

 Configuration data
================================================================================*/

(function($){
	window.goodman.config = $.extend(true, window.goodman.config || {}, {

		// Allow use of console.log (as qldglobe.util.log)
		allowLog: true,

		// Show the landing screen
		showLanding: true,

		// Is this Goodman' internal view
		isInternal: false,

		// URL for map image disclaimer
		disclaimerURL: 'http://www.goodman.com/property/global-property-portfolio/disclaimer',

		// Small screen layout, should match mobile CSS media query in index.html
		mobileWidth: 991,
		mobileHeight: 991,
		isMobile: false,
		isMobileSize: false,
		isMobileOS: false,
		isIOS: false,
		isIE: false,
		isMacintosh: false,
		isWindows: false,

		// Previous searches cookie
		previousSearchCookie: 'SmartSpaceSearches',
		previousSearchCookieMaxLength: 4,
		previousSearchCookieMaxExpiry: 7, // Days

		// Gallery auto rotate
		galleryAutoRotateDelay: 3000,

		emptyPropertyID: '00000000-0000-0000-0000-000000000000',

		sizeStepsProperty: [0, 15000, 30000, 50000, 70000, 'unlimited'],
		sizeStepsLease: [0, 2000, 5000, 10000, 30000, 'unlimited'],
		sizeSteps: [0, 15000, 30000, 50000, 70000, 'unlimited'],
		sizeUnit: 'sqm',

		shareBaseUrl: window.top.location.origin + window.top.location.pathname,
		//shareBaseUrl: window.top.location,

		funds: [
			'GNAP',
			'GADP',
			'GAIP',
			'GAP',
			'KGIP',
			'GMT',
			'GCLP',
			'GHKLP',
			'GJCP',
			'GEP',
			'KGG',
			'GUKP',
			'GMG'
		],

		mapTypes: [
			{name:'Streets', value:'streets'},
			{name:'Satellite', value:'satellite'}
		],

		countryCode: {
			AUS: 'Australia',
			NZL: 'New Zealand',
			CHN: 'Greater China',
			HKG: 'Greater China',
			JPN: 'Japan',
			BRA: 'Brazil',
			USA: 'USA',
			BEL: 'Belgium',
			CZE: 'Czech Republic',
			DNK: 'Denmark',
			FRA: 'France',
			DEU: 'Germany',
			HUN: 'Hungary',
			ITA: 'Italy',
			NLD: 'Netherlands',
			POL: 'Poland',
			SVK: 'Slovakia',
			ESP: 'Spain',
			SWE: 'Sweden',
			GBR: 'United Kingdom'
		},

		countryAbbrRemap: {
			uk: 'United Kingdom',
			'great britain': 'United Kingdom',
			'united states': 'USA',
			america: 'USA',
			'north america': 'USA',
			nz: 'New Zealand'
		},

		europeanAddress: [
			"Belgium",
			"Czech Republic",
			"Denmark",
			"France",
			"Germany",
			"Hungary",
			"Italy",
			"Netherlands",
			"Poland",
			"Slovakia",
			"Spain",
			"Sweden"
		],

	});

})(jQuery);
