/*================================================================================
	goodman.util
	
	Utility functions
================================================================================*/

(function($){
	window.goodman.util = $.extend(true, window.goodman.util || {}, {
	
		_timeAtStart: 0,
		
		init: function() {
			this._timeAtStart = (new Date()).getTime();
			goodman.config.isMobile = this.isMobile();
			goodman.config.isMobileSize = this.isMobileSize();
			goodman.config.isMobileOS = this.isMobileOS();
			goodman.config.isIOS = this.isIOS();
			goodman.config.isIE = this.isIE();
			goodman.config.isMacintosh = this.isMacintosh();
			goodman.config.isWindows = this.isWindows();
			$('html')
				.removeClass('no-js')
				.addClass('js')
				.toggleClass('is-mobile', goodman.config.isMobile)
				.toggleClass('is-mobile-size', goodman.config.isMobileSize)
				.toggleClass('is-ios', goodman.config.isIOS)
				.toggleClass('is-ie', goodman.config.isIE)
				.toggleClass('is-macintosh', goodman.config.isMacintosh)
				.toggleClass('is-windows', goodman.config.isWindows)
				.toggleClass('is-mobile-os', goodman.config.isMobileOS);
			this.resizer();
			// if (goodman.config.isMobileOS) { this.stopScrollBounce(); }
		},
		
		log: function() { 
			if(goodman.config.allowLog && typeof console !== 'undefined' && !goodman.config.isIOS) { 
				if (console.log.apply) {
					console.log.apply(console,arguments);
				} else {
					console.log(arguments[0]);
				}
			}
		},
		
		isMobile: function() {
			if (this.isMobileOS()) {
				return this.isMobileSize();
			} else {
				return false; // Always return false if it isn't a mobile OS
			}
		},
		
		isMobileSize: function() {
			return ( $(window).width() <= goodman.config.mobileWidth || $(window).height() <= goodman.config.mobileHeight );
		},
		
		isMobileOS: function() {
			return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
		},
		
		isIOS: function() {
			return /iPhone|iPad|iPod/i.test(navigator.userAgent);
		},
		
		isIE: function() {
			return /MSIE|Trident.*rv\:11\./.test(navigator.userAgent);
		},
		
		isMacintosh: function() {
			return /Mac/.test(navigator.platform);
		},
		
		isWindows: function() {
			return /Win/.test(navigator.platform);
		},
		
		stopScrollBounce: function() {
			$(document).on('touchmove', function(e) {
				var $target = $(e.target);
				if( !$target.hasClass('scroll-pane') ) {
					e.preventDefault(); 
				}
			});
		},
		
		startMapApp: function() {
			if ( !goodman.config.mapAppStarted ) {
				goodman.observer.publish('map-start');
				goodman.config.mapAppStarted = true;
			}
		},
		
		/* ====================================================
		 *	Resizer
		 * ==================================================*/
		
		resizer: function() {
			var self = this,
				resizerIID = -1;
			$(window)
				.on('resize', function(){
					clearTimeout(resizerIID);
					resizerIID = setTimeout(function(){
						var nowIsMobile = self.isMobile();
						goodman.observer.publish('window-resize', {w:$(window).width(), h:$(window).height()});
						/*
						if (goodman.config.isMobile !== nowIsMobile) {
							goodman.config.isMobile = nowIsMobile;
							goodman.observer.publish('layout-change', nowIsMobile);
						}
						*/
					}, 250);
				});
		},
		
		/*===================================================================
		Data storage
		===================================================================*/

		hasLocalStorage: function() {
			try {
				return ('localStorage' in window && window['localStorage'] !== null);
			} catch (e) {
				return false;
			}
		},
		
		
		/* ====================================================
		 *	Geolocation
		 * ==================================================*/
		
		hasGeolocation: function() {
			//return (navigator.geolocation)?true:false;
		},
		
		selfGeoLocate: function() {
			var oldLocation;
			/*
			if ( goodman.util.hasLocalStorage() ) {
				goodman.util.log('selfGeoLocate send stored location', localStorage.getItem('goodman.location'));
				try { oldLocation = JSON.parse( localStorage.getItem('goodman.location') ); } catch (err) {}
				if (oldLocation !== 'undefined' && oldLocation !== '{}') {
					try {
						goodman.observer.publish('set-location', oldLocation);
					} catch (e) {}
				}
			}
			*/
			
			//if (navigator.geolocation) {
			try {
				navigator.geolocation.getCurrentPosition(
					function(position) {
						goodman.observer.publish('set-location', position);
						/*
						goodman.util.log('selfGeoLocate send returned location', position.coords);
						goodman.util.log('selfGeoLocate send returned JSON', JSON.stringify($.extend({}, {coords:$.extend({}, position.coords)}, position)) );
						if ( goodman.util.hasLocalStorage() ) {
							localStorage.setItem('goodman.location', JSON.stringify($.extend({}, {coords:$.extend({}, position.coords)}, position)) );
						}
						*/
					}, 
					function(error) {
						switch(error.code) {
							case error.PERMISSION_DENIED:
								goodman.util.log("getCurrentPosition error: User denied the request for Geolocation.");
								break;
							case error.POSITION_UNAVAILABLE:
								goodman.util.log("getCurrentPosition error: Location information is unavailable.");
								break;
							case error.TIMEOUT:
								goodman.util.log("getCurrentPosition error: The request to get user location timed out.");
								break;
							case error.UNKNOWN_ERROR:
								goodman.util.log("getCurrentPosition error: An unknown error occurred.");
								break;
						}
					},
					{
						// enableHighAccuracy: true,
						// timeout: 5000,
						// maximumAge: 0
					}
				);
			} catch (err) { 
				goodman.util.log('selfGeoLocate error'); 
			}
			//}
		},
		
		/* 
		 * Local storage
		 */
		
		/* ====================================================
		 *	Text and number formating
		 * ==================================================*/

		addLeadingZeroes: function(number, length) {
			var str = number.toString();
			while (str.length < length) {
				str = "0" + str;
			}
			return str;
		},
		
		numberFormat: function (number, decimals, dec_point, thousands_sep) {
			number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
			var n = !isFinite(+number) ? 0 : +number,
				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
				s = '',
				toFixedFix = function (n, prec) {
					var k = Math.pow(10, prec);
					return '' + Math.round(n * k) / k;
				};
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
			if (s[0].length > 3) {
				s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
			}
			if ((s[1] || '').length < prec) {
				s[1] = s[1] || '';
				s[1] += new Array(prec - s[1].length + 1).join('0');
			}
			return s.join(dec);
		},
		
		numberShorten: function (number) {
			var numberStr = (number + '').replace(/[^0-9+\-Ee.]/g, ''),
				number = +numberStr,
				units = ['','k','m','b','t'],
				unitName = '',
				testLenght, shortValue;
				
			
			if (number < 100) { // Special case for numbers less than 100
				if (numberStr.indexOf('.') > -1) { // is decimal
					return {value: this.numberFormat(number, 3-numberStr.indexOf('.')), unit:''}; 
				} else {
					return {value: number, unit:''};
				}
			}
			
			testLenght = (Math.floor(number) + '').length;
			shortValue = Math.round(number/Math.pow(10,(testLenght-3)));
			
			if ((testLenght%3) > 0) {
				shortValue = shortValue / Math.pow(10,(3-(testLenght%3)));
				unitName = units[Math.floor(testLenght/3)];
			} else {
				unitName = units[Math.floor(testLenght/3)-1];
			}
			
			return {value: shortValue, unit:unitName};
				
		},
		
		numberOrdinalFormat: function (i, superscript) {
			var suffix = ['st','nd','rd','th'],
				j = i%10;
				
			if (superscript) { suffix = ['<sup>st</sup>','<sup>nd</sup>','<sup>rd</sup>','<sup>th</sup>']; }
			if (j === 1 && i%100 !== 11) {
				return i + suffix[0];
			}
			if (j === 2 && i%100 !== 12) {
				return i + suffix[1];
			}
			if (j === 3 && i%100 !== 13) {
				return i + suffix[2];
			}
			return i + suffix[3];
		},
		
		dateFormat: function (date, fullMonthName, dateOnly) {
			var dateArray = [],
				monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
				monthFullArray = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			fullMonthName = fullMonthName||false;
			dateOnly = dateOnly||false;
			if (!date.getDate) { date = new Date(date); }	
			if (isNaN(date)) { return ''; }		
			dateArray.push(date.getDate());
			dateArray.push((fullMonthName)?monthFullArray[date.getMonth()]:monthArray[date.getMonth()]);
			dateArray.push(date.getFullYear());
			if (dateOnly) { return dateArray.join(' '); }
			if (date.getHours() < 12) {
				dateArray.push(date.getHours()+':'+this.addLeadingZeroes(date.getMinutes(),2)+'AM');
			} else if (date.getHours() == 12) {
				dateArray.push('12:'+this.addLeadingZeroes(date.getMinutes(),2)+'PM');
			} else {
				dateArray.push((date.getHours()-12)+':'+this.addLeadingZeroes(date.getMinutes(),2)+'PM');
			}
			return dateArray.join(' ');
		},
		
		monthYearFormat: function (date, fullMonthName) {
			var dateArray = [],
				monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
				monthFullArray = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			fullMonthName = fullMonthName||false;
			if (!date.getDate) { date = new Date(date); }	
			if (isNaN(date)) { return ''; }		
			dateArray.push((fullMonthName)?monthFullArray[date.getMonth()]:monthArray[date.getMonth()]);
			dateArray.push(date.getFullYear());
			return dateArray.join(' ');
		},
		
		isFuture: function (date) {
			if (!date.getDate) { date = new Date(date); }	
			return (!isNaN(date) && date > new Date() );
		},
		
		timeSinceStart: function() {
			return (new Date()).getTime() - goodman.util._timeAtStart;
		},
		
		decimalDegreesToDDMMSS: function(dd, useDirection, isLatitude) {
			// useDirection: True to use + / - degrees, false to use N/E/S/W
			// isLatitude: Append N/S or E/W
			var suffix = '';
			if (useDirection) {
				if (isLatitude)
					suffix = degrees > 0 ? 'N' : 'S';
				else
					suffix = degrees > 0 ? 'W' : 'E';
			};
			
			var degrees = dd | 0;
			if (useDirection)
				degrees = Math.abs(degrees);
			var leftovers = (Math.abs(dd) - degrees) * 60;
			var minutes = leftovers | 0;
			if (minutes < 10)
				minutes = "0" + minutes;
			var seconds = ((leftovers - minutes) * 60) | 0;
			if (seconds < 10)
				seconds = "0" + seconds;
			return degrees.toString() + "Â° " + minutes + "\" " + seconds + "' " + suffix;
		},
		
		// http://www.html5rocks.com/en/mobile/fullscreen/
		enterFullScreen: function() {
			var doc = window.document,
				docEl = doc.documentElement,
				requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
			if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
				requestFullScreen.call(docEl);
			}
		},
		exitFullScreen: function() {
			var doc = window.document,
				cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
			if(doc.fullscreenElement || doc.mozFullScreenElement || doc.webkitFullscreenElement || doc.msFullscreenElement) {
				cancelFullScreen.call(doc);
			}
		},
		toggleFullScreen: function() {
			var doc = window.document,
				docEl = doc.documentElement,
				requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen,
				cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
			
			if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
				requestFullScreen.call(docEl);
			} else {
				cancelFullScreen.call(doc);
			}
		},
		forceFullScreen: function() {
			if(goodman.config.forceFullscreen && goodman.config.isMobile && goodman.config.isMobileOS) {
				this.enterFullScreen();
			}
		},
		
		sqm2sqft: function (sqm) {
			return sqm / 0.092903;
			//return 10.76391041671 * sqm;
			//return 10.764 * sqm;
			//return 10 * sqm;
		},
		
		sizeFormat: function (size, showUnit) {
			
			showUnit = showUnit||false;
			showUnit = (showUnit)?' '+goodman.config.sizeUnit:'';
			if (isNaN(parseInt(size))) { return size + ' ' + showUnit; }
			if (goodman.config.sizeUnit === 'sqft')	{
				return this.numberFormat(this.sqm2sqft(size), 0) + showUnit;
			} else {
				return this.numberFormat(size, 0) + showUnit;
			}
		},
		
		
		/* ====================================================
		 *	Add / Remove scroll
		 * ==================================================*/
		
		addCustomSlider: function(selector, restoreScroll, forceUse, callbacks, scrollSpeed) {
			var $selector = $(selector),
				theme = 'minimal-dark',
				scrollRestore,
				optsObj;
			
			scrollRestore = parseInt(restoreScroll);
			if (isNaN(scrollRestore)) { scrollRestore = 0; }
			
			scrollSpeed = parseInt(scrollSpeed);
			if (isNaN(scrollSpeed)) { scrollSpeed = 75; }

			if (goodman.config.isMobile && !forceUse) {
				$selector.scrollTop(scrollRestore * -1);
			} else {
				optsObj = { 
						theme: theme,
						callbacks: {
							onInit: function(){ 
								$(this).scrollTop(0).find('.mCSB_container').css('top', scrollRestore);
							}
						}
					};
				optsObj.callbacks = $.extend(optsObj.callbacks, callbacks||{});
				
				if (goodman.config.isWindows) {
					//if (goodman.config.isIE) {
					//	optsObj = $.extend(optsObj, { scrollInertia: 60, mouseWheel:{ scrollAmount: scrollSpeed } });
					//} else {
						optsObj = $.extend(optsObj, { scrollInertia: 60, mouseWheel:{ scrollAmount: scrollSpeed } });
					//}
				}
				
				$selector
					.scrollTop(scrollRestore * -1)
					.mCustomScrollbar(optsObj);
			}
		},
		
		destroyCustomSlider: function(selector) {
			var $selector = $(selector),
				restoreScroll = 0;
				
			if ($selector.length === 0) { return 0; }
				
			if ($('.mCSB_container', $selector).length === 0) {
				restoreScroll = $selector.scrollTop();
			} else {
				restoreScroll = parseInt($('.mCSB_container', $selector).css('top'));
				$selector.mCustomScrollbar('destroy');
			}
			return restoreScroll;					
		},
		
		getScrollCustomSlider: function(selector) {
			var $selector = $(selector),
				restoreScroll = 0;
			if ($selector.length === 0) { return 0; }
			if ($('.mCSB_container', $selector).length === 0) {
				restoreScroll = $selector.scrollTop();
			} else {
				restoreScroll = parseInt($('.mCSB_container', $selector).css('top'));
			}
			return restoreScroll;					
		},
		
		scrollTopCustomSlider: function(selector, newTop) {
			var $selector = $(selector);
			if (!newTop || newTop === 'undefined') { newTop = 0; }
			if ($('.mCSB_container', $selector).length === 0) {
				$selector.scrollTop(newTop * -1);
			} else {
				$('.mCSB_container', $selector).animate({'top':newTop}, 300);
			}
		},
		
		jumpToCustomSlider: function(selector, newTop) {
			var $selector = $(selector);
			if (!newTop || newTop === 'undefined') { newTop = 0; }
			if ($('.mCSB_container', $selector).length === 0) {
				$selector.scrollTop(newTop * -1);
			} else {
				$('.mCSB_container', $selector).css({'top':newTop});
			}
		},
		
	});
	
	/**
	 * Patch to add touches and targetTouches properties to jQuery.Event objects.
	 */
	/*
	$.each(['touches', 'targetTouches'], function(i, propName){
		if ( $.inArray(propName, $.event.props) < 0 ) {
			$.event.props.push(propName);
		}
	});
	*/
	
})(jQuery);
