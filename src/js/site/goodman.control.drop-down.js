/*================================================================================
	goodman.control.dropdown
================================================================================*/

(function($){

	window.goodman.control.dropdown = $.extend(true, window.goodman.control.dropdown || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			this.enable();
		},

		enable: function() {
			var self = this;
			this.$body
				.off('click.drop-down')
				.on('click.drop-down', '.drop_down_trigger', function(event){
					event.preventDefault();
					var drop_down_parent = $(this).parents('.drop_down_parent'),
						drop_down_position;
					if ($(this).data('trigger') && $('#'+$(this).data('trigger')).length > 0) {
						drop_down_parent = $('#'+$(this).data('trigger'));
					}
					var drop_down_wrapper = drop_down_parent.children('.drop_down_wrapper');
					if (drop_down_parent.hasClass('drop_down_expand')) {
						drop_down_wrapper
							.animate({
								'height':0
							},300,function(){
								drop_down_parent.removeClass('drop_down_expand');
								$(this).css({'height':''});
							});
					} else {
						drop_down_position = self.getPosition(drop_down_parent);
						drop_down_wrapper
							.toggleClass('left', (drop_down_position === 'left'))
							.toggleClass('right', (drop_down_position === 'right'))
							.toggleClass('half-right', (drop_down_position === 'half-right'))
							.animate({
								'height':drop_down_wrapper.children('.drop_down_container').height()
							},300,function(){
								drop_down_parent.addClass('drop_down_expand');
								$(this).css({'height':''});
							});
					}
				});
			
		},
		
		disable: function() {
			this.$body.off('click.drop-down');
		},
		
		getPosition: function($el) {
			var containerWidth = $el.parent().width(),
				posLeft = $el.position().left,
				relative = posLeft / containerWidth,
				posStr = 'center';
			
			if (relative < 0.34) { 
				posStr = 'left'; 
			} else if (relative > 0.66) { 
				posStr = 'right'; 
			}
			if (relative > 0.5) { 
				posStr = 'half-right'; 
			}
			return posStr;
		},
		
	});

})(jQuery);