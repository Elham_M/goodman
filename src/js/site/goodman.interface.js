/*================================================================================
	goodman.interface
================================================================================*/

(function($){

	window.goodman.interface = $.extend(true, window.goodman.interface || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			goodman.observer.subscribe('set-size-unit', self.setSizeUnit, self); 
			
			this.enable();
		},
		
		enable: function() {
			var self = this;
			this.$body
				.on('click', '.page-left-resizer', function(){
					self.$body.toggleClass('page-left-closed');					
				});
		},
		
		setSizeUnit: function(newUnit) {
			if (newUnit !== goodman.config.sizeUnit) {
				goodman.config.sizeUnit = newUnit;
				goodman.observer.publish('change-size-unit', goodman.config.sizeUnit);
			}
			
		},
		
	});

})(jQuery);