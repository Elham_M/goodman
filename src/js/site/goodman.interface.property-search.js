/*================================================================================
	goodman.interface.propertysearch
================================================================================*/

(function($){

	window.goodman.interface.propertysearch = $.extend(true, window.goodman.interface.propertysearch || {}, {

		init: function() {
			var self = this;
			this.container = $('#property-list');
			this.searchContainer = $('#property-list-search');
			this.filterContainer = $('#property-list-filter');
			this.resultsContainer = $('#property-list-results');
			this.autocompleteContainer = null;
			this.autocompleteContainerIID = -1;
			
			goodman.observer.subscribe('map-start', self.mapStart, self);
			goodman.observer.subscribe('search-busy', self.searchBusy, self);
			goodman.observer.subscribe('search-result', self.searchResult, self);
			goodman.observer.subscribe('search-suggest-results', self.suggestionResult, self);
			goodman.observer.subscribe('set-promoted', self.suggestionPromote, self);
			goodman.observer.subscribe('change-size-unit', self.renderResults, self);
			goodman.observer.subscribe('change-size-unit', self.renderFilterSize, self);
			goodman.observer.subscribe('window-resize', self.resize, self);
			
			this.searchResultData = null;
			this.searchResultRenderData = [];
			this.searchResultPromote = -1;
			this.searchCountryPromote = '';
			this.suggestionResultData = null;

			this.restrictToCountry = null;
			this.returnToObject = null;
			this.filterData = {};
			
			this.render();
			this.searchRebuildQuery();
			this.filterLabelUpdate();
			this.enable();
		},

		render: function() {
			var self = this,
				searchPaneScroll = goodman.util.destroyCustomSlider($('.scroll-pane', this.container));
			this.searchContainer.html( goodman.template['property-list-search'].hbs({ internal: goodman.config.isInternal }) );
			this.filterContainer.html( goodman.template['property-list-filter'].hbs( goodman.config ) );
			goodman.control.checkbox.init();
			goodman.control.range.init();
			this.autocompleteContainer = $('.property_list_search_autocomplete_container', this.searchContainer);
			goodman.util.addCustomSlider($('.scroll-pane', this.container), searchPaneScroll, false, { whileScrolling: function(){ self.scrollResults(this.mcs.top); } });
		},
		
		renderResults: function() {
			var self = this,
				count = 0, key,
				searchPaneScroll = goodman.util.destroyCustomSlider($('.scroll-pane', this.container)),
				renderData;
			if (this.restrictToCountry === null) {
				for (key in this.searchResultRenderData) { if (this.searchResultRenderData.hasOwnProperty(key)) { count++; } }	
				renderData = this.searchResultRenderData;
			} else {
				count = 1;
				renderData = {};
				renderData[this.restrictToCountry] = this.searchResultRenderData[this.restrictToCountry];
			}
			if (goodman.config.isMobile) { $('.scroll-pane-results', this.container).off('scroll.results-scroll'); }
			this.resultsContainer.html( goodman.template['property-list-results'].hbs({countryCount: count, countries:renderData, restrictToCountry:this.restrictToCountry, sizeUnit:goodman.config.sizeUnit}) );
			goodman.control.gallery.init();
			goodman.util.addCustomSlider($('.scroll-pane', this.container), 0, false, { whileScrolling: function(){ self.scrollResults(this.mcs.top); } });
			
			if (goodman.config.isMobile) {
				$('.scroll-pane-results', this.container).off('scroll.results-scroll').on('scroll.results-scroll', function(){ self.scrollResults( $(this).scrollTop() * -1 ); });
			}
			
			if (this.returnToObject !== null) {
				var returnObj = $('[data-object-id="'+this.returnToObject+'"]', this.resultsContainer);
				if ( returnObj.length && this.scrollResultsCanHappan() ) {
					goodman.util.jumpToCustomSlider($('.scroll-pane', this.container), (-1 * returnObj.position().top) - 90 );
					self.scrollResults(  goodman.util.getScrollCustomSlider($('.scroll-pane', this.container)) );
				} else {
					self.scrollResults(0);
				}
			} else {
				self.scrollResults(0);
			}
			
			this.filterLabelUpdate();
		},
		
		resize: function(newSize) {
			var newScrolltop = this.searchContainer.height() + this.filterContainer.height();
			this.resultsContainer.css({'margin-top': newScrolltop});
		},
		
		mapStart: function() {
			$('#property_list_search', this.searchContainer).attr('placeholder', 'City, address, property name').prop('disabled',false);
			$('.property_list_search_container', this.searchContainer).removeClass('loading');
		},
		
		enable: function() {
			var self = this,
				$body = $('#goodman-smartspace-app');
			
			/* Filters */
			$body
				.on('click', '.property_filter_trigger', function(){
					if ($body.hasClass('property_filter_active')) {
						self.filterResetToSelected();
						if ($(this).hasClass('drop_down_trigger')) {
							if ($(this).closest('.drop_down_parent').hasClass('drop_down_expand')) {
								// If clicked filter section is active, close the filter
								$body.removeClass('property_filter_active');
							} else {
								// If clicked filter section is not active, but the filter is active, close the active filter section
								$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
							}
						} else {
							// If the mobile filter trigger is clicked, close the filter and de-active filter section if there's any
							$body.removeClass('property_filter_active');
							$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
						}
					} else {
						$body.addClass('property_filter_active');
						if (!$(this).hasClass('drop_down_trigger') && $('.property_filter_container > .drop_down_expand').length === 0) {
							// If there is no active filter section, make the first section (country filter) active
							if (goodman.config.isMobileSize) {
								goodman.util.jumpToCustomSlider( $('.scroll-pane', self.container), 0);
							}
							$('.property_filter_container > .drop_down_parent:eq(0)').addClass('drop_down_expand');
						}
					}
				})
				.on('click', '.property_filter_button_apply', function(){
					$body.removeClass('property_filter_active');
					$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
					self.search();
				})
				.on('click', '.property_filter_button_cancel', function(){
					self.filterResetToSelected();
					$body.removeClass('property_filter_active');
					$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
				})
				.on('click', '.property_filter_button_reset', function(){
					self.filterReset();
					self.filterLabelUpdate();
					self.search();
				})
				.on('click', '.property_filter_button_zoom_out', function(){
					goodman.observer.publish('map-expand-to-show-results');
				})
				.on('click', '[name="countrygroup"]', function(){
					var $this = $(this),
						checked = $(this).is(':checked'),
						group = $this.closest('.property_filter_sub_section');
					$('[name="country"]', group).prop('checked', checked);
					goodman.control.checkbox.update();
				})
				.on('click', '[name="forleasefilter"]', function(){
					self.searchRebuildQuery();
					self.renderFilterSize();
					self.search();
				})
				.on('click', '[name="size_unit"]', function(){
					goodman.observer.publish('set-size-unit', $(this).val());
				});

			
			/* Search */
			$body
				.on('submit', '#property_list_search_form', function(e){
					e.preventDefault();
				})
				.on('click', '[href="#searchCountry"][data-country]', function(e){
					e.preventDefault();
					var country = $(this).data('country');
					//self.filterCountrySet(country);
					//self.search();
					self.restrictToCountry = country;
					self.renderResults();
				})
				.on('click', '[data-object-id]', function(e){
					var $this = $(this),
						oid = $(this).data('object-id');
					self.returnToObject = oid;
					self.clearCountryOnResult = false;
					goodman.observer.publish('detail-request', oid);
				})
				.on('mouseenter', '[data-object-id]', function(){
					var $this = $(this),
						oid = $(this).data('object-id');
					$this.addClass('hover');
					goodman.observer.publish('property-hover', oid);
				})
				.on('mouseleave', '[data-object-id]', function(){
					var $this = $(this),
						oid = $(this).data('object-id');
					$this.removeClass('hover');
					goodman.observer.publish('property-hover-end', oid);
				})
				.on('click', '[data-location]', function(e){
					var $this = $(this),
						location = $.trim($(this).data('location'));
					if (location !== '') {
						goodman.observer.publish('search-locate', location);
					}
				})
				.on('click', '.property_list_search_clear', function(e){
					$('#property_list_search').val('');
				});


			/* Suggestions */
			this.container
				.on('focus keyup', '#property_list_search', function(e){
					e.preventDefault();
					var value = $.trim($(this).val());
					clearTimeout( self.autocompleteContainerIID );
					if (e.which === 38 || e.which === 40 || e.which === 13) { return; }
					if (value !== '') {
						self.autocompleteContainerIID = setTimeout( function(){ goodman.observer.publish('search-suggest', value); }, 250 );
					} else {
						self.previousSearchShow();
					}
					self.searchClearControlCheck();
				})
				.on('blur', '#property_list_search', function(){
					clearTimeout( self.autocompleteContainerIID );
					self.autocompleteContainerIID = setTimeout( self.suggestionClose, 250 );
				})
				.on('keydown', '#property_list_search', function(e){
					var acWrap = $('.property_list_search_autocomplete_wrapper'),
						suggestions = $('.property_list_search_autocomplete_row', acWrap),
						index = suggestions.filter('.active').index(),
						inputText = $.trim($(this).val());
					switch (e.which) {
						case 38: // Up
							index--;
							if (index === -1) { index = 0; }
							if (index === -2) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 40: // Down
							index++;
							if (index >= suggestions.length) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 13: // Enter
							self.returnToObject = null;
							if (index === -1) {
								goodman.observer.publish('suggest-select', inputText );
								self.suggestionPromote( -1, inputText );
							} else {
								suggestions.filter('.active').trigger('click');
								e.preventDefault();
							}
							break;
					}
				})
				.on('mouseover', '.property_list_search_autocomplete_row', function(){
					$('.property_list_search_autocomplete_row').removeClass('active');
				})
				.on('click', '[data-suggestion-result]', function(){
					self.suggestionSelect( $(this).data('suggestion-result'), $(this).text() );
				})
				.on('click', '[data-suggestion-saved]', function(){
					self.previousSearchSelect( $(this).data('suggestion-saved'), $(this).text() );
				});
			
		},
		
		searchBusy: function(state) {
			goodman.util.log('goodman.interface.propertysearch::searchBusy', state);
		},
		
		searchClearControlCheck: function () {
			$('.property_list_search_clear', this.container).toggle( $.trim($('#property_list_search').val()) !== '' );	
		},
		
		clearCountryOnResult: true,
		
		searchResult: function(data) {
			var renderData = {},
				i, ilen,
				features = data.features,
				attributes, country,
				sortedRenderData;
			
			goodman.util.startMapApp(); // Start app on first search results recieved
			
			// this.restrictToCountry = null;
			
			if (this.clearCountryOnResult) {
				this.countryRestrict();
			} else if ( !$('#goodman-smartspace-app').hasClass('property-detail-open') ) {
				this.clearCountryOnResult = true;
			}
			
			this.searchResultData = data;
			
			for (i=0, ilen=features.length; i<ilen; i++) {
				attributes = features[i].attributes;
				country = $.trim(attributes.country);
				
				//country = this.countryRemap( attributes.objectid, country );  // Clean countries
				
				if (country === '') { country = 'Unknown'; }
				if (renderData[country] === undefined) { renderData[country] = []; }
				
				if (this.searchResultPromote !== -1) {
					if (this.searchResultPromote === attributes.objectid) {
						renderData[country].unshift(attributes);
						this.searchCountryPromote = attributes.country;
					} else {
						renderData[country].push(attributes);
					}
				} else {
					renderData[country].push(attributes);	
				}
			}
			
			if (this.searchCountryPromote !== '') {
				sortedRenderData = {};
				for (country in renderData) {
					if (country.toLowerCase() === this.searchCountryPromote.toLowerCase()) {
						sortedRenderData[country] = renderData[country];
					}
				}
				for (country in renderData) {
					if (country.toLowerCase() !== this.searchCountryPromote.toLowerCase()) {
						sortedRenderData[country] = renderData[country];
					}
				}
				renderData = sortedRenderData;
			}
			
			this.searchResultRenderData = renderData;
			if (!goodman.config.isIOS || $('#landing-container').length === 0) {
				this.renderResults();
			}
			
			if (goodman.config.isMobile) {
				$('#property_list_search').blur();
			}
		},
		
		countryRestrict: function(country) {
			if (country === undefined || country === '') {
				this.restrictToCountry = null;
			} else {
				this.restrictToCountry = this.countryAbbrRemap(country);
			}
		},
		
		searchBuildQuery: function() {
			return this.filterData;
		},
		
		searchRebuildQuery: function() {
			var queryArr = [],
				queryObj = {},
			//	term = $.trim($('#property_list_search').val()),
				forleasefilter = $('[name="forleasefilter"]').prop('checked');
			
			if (!goodman.config.isInternal) {
				queryArr.push('displayonglobal=\'1\'');
				queryObj.view = 'property';
			} else if (forleasefilter) {
				queryArr.push('isleased=\'0\' AND (minleasingsize IS NOT NULL OR maxleasingsize IS NOT NULL)');
				queryObj.view = 'lease';
			} else {
				queryArr.push('displayonglobal=\'1\'');
				queryObj.view = 'property';
			}
			
			// Countries
			/*
			queryObj.country = [];
			$('[name="country"]:checked').each(function(){
				queryObj.country.push($(this).val());
			});
			if (queryObj.country.length) {
				queryArr.push( 'country IN (\'' + queryObj.country.join('\',\'') + '\')' );
			}
			*/
			
			// Types
			queryObj.type = [];
			var typeFilter = $('#property_type_filter_section'),
				checkedTypeFilter = $(':checked', typeFilter),
				typeFilterArray = [];
			checkedTypeFilter.each(function(){
				queryObj.type.push($(this).val());
			});
			if (queryObj.type.length) {
				for (var key in queryObj.type) {
					typeFilterArray.push('LOWER(propertytype) LIKE LOWER(\'%'+ queryObj.type[key] + '%\')');
				}
				queryArr.push( '(' + typeFilterArray.join( ' OR ' ) + ')' );
			}
			
			// Size
			var sizeFilter = $('#property_size_filter_section');
			queryObj.sizeMin = parseInt($('[name="min"]', sizeFilter).val());
			queryObj.sizeMax = parseInt($('[name="max"]', sizeFilter).val());
			if (queryObj.sizeMin > 0) { 
				queryArr.push( ((forleasefilter)?'maxleasingsize':'size_standardunits') + ' >= ' + queryObj.sizeMin );
			}
			//if (queryObj.sizeMax < 100000) { 
			if (!isNaN( queryObj.sizeMax )) {
				queryArr.push( ((forleasefilter)?'minleasingsize':'size_standardunits') + ' <= ' + queryObj.sizeMax );
			}
			
			// Funds
			queryObj.fund = [];
			var fundFilterArray = [];
			$('[name="fund"]:checked').each(function(){
				queryObj.fund.push($(this).val());
			});
			if (queryObj.fund.length) {
				for (var key in queryObj.fund) {
					fundFilterArray.push('LOWER(fund) LIKE LOWER(\'%'+ queryObj.fund[key] + '%\')');
				}
				queryArr.push( '(' + fundFilterArray.join( ' OR ' ) + ')' );
			}
			
			queryObj.where = queryArr.join(' AND ');
			this.filterData = queryObj;
		}, 
		
		searchReset: function() {
			this.filterReset();
			$('#property_list_search').val('');
		},
		
		search: function() {
			this.searchRebuildQuery();
			this.filterLabelUpdate();
			goodman.observer.publish('search', this.searchBuildQuery(), false );
		},
		
		
		/* ====================================================
		 *	Suggestions
		 * ==================================================*/
		
		suggestionResult: function( suggestions ) {
			var self = this;
			this.suggestionResultData = suggestions;
		
			if ( this.suggestionResultData.numResults > 0 ) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-results'].hbs( this.suggestionResultData ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			} else {
				this.autocompleteContainer
					.html( '' )
					.closest('.property_list_search_autocomplete_wrapper')
					.removeClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		suggestionClose: function( ) {
			clearTimeout( this.autocompleteContainerIID );
			$('.property_list_search_autocomplete_wrapper_expand').removeClass('property_list_search_autocomplete_wrapper_expand');	
		},
		
		suggestionPromote: function( objid, country ) {
			var matchArr;
			this.searchResultPromote = objid;
			if (country === undefined || country === '') {
				this.searchCountryPromote = '';
				return;
			}
			country = $.trim(country);
			matchArr = country.match(/([A-Z]{3,3})$/);
			if (matchArr !== null && matchArr.length > 1) {
				this.searchCountryPromote = goodman.config.countryCode[matchArr[1]];
			} else {
				this.searchCountryPromote = this.countryAbbrRemap(country);
			}
		},
		
		suggestionSelect: function( idx, text ) {
			var s = idx.split(','),
				result = this.suggestionResultData.results[s[0]][s[1]];
			$('#property_list_search').val( text );
			if (result.feature && result.feature.attributes.objectid) { 
				this.suggestionPromote( result.feature.attributes.objectid, '' );
			} else {
				this.suggestionPromote( -1, text );
			}
			this.previousSearchSave( result );
			goodman.observer.publish('suggest-select', result);
			this.suggestionClose();
			this.searchClearControlCheck();
		},
		
		/* ====================================================
		 *	Recent Searches
		 * ==================================================*/
		
		previousSearchShow: function() {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie);
			if (cookie !== undefined && cookie !== '' && cookie.length > 0) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-saved'].hbs( cookie ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		previousSearchSave: function( data ) {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie),
				newEntry = ( data.feature && data.feature.attributes )?data.feature.attributes:data;
			if (cookie === undefined || cookie === '') { cookie = []; }
			// if value is already in cookie remove (it will be added back to the start)
			for (var i in cookie) {
				if ( cookie[i].name && cookie[i].name === newEntry.name ) { cookie.splice( i, 1 ); break; }
				else if ( cookie[i].text && cookie[i].text === newEntry.text ) { cookie.splice( i, 1 ); break; }
			}
			if ( newEntry.objectid ) { newEntry.showLease = $('[name="forleasefilter"]').prop('checked'); }
			cookie.unshift( newEntry );
			if (cookie.length > goodman.config.previousSearchCookieMaxLength) {
				cookie.splice(-1, cookie.length - goodman.config.previousSearchCookieMaxLength);
			}
			Cookies.set(goodman.config.previousSearchCookie, cookie, { expires: goodman.config.previousSearchCookieMaxExpiry });
		},
		
		previousSearchClear: function() {
			Cookies.set(goodman.config.previousSearchCookie, '');
		},
		
		previousSearchSelect: function (idx, text) {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie),
				result = cookie[idx];
			$('#property_list_search').val( text );
			
			if (result.objectid) { 
				this.suggestionPromote( result.objectid, '' );
				if (result.showLease !== undefined && result.showLease !== $('[name="forleasefilter"]').prop('checked')) {
					$('[name="forleasefilter"]').prop('checked', result.showLease);
					goodman.control.checkbox.update();
					this.searchRebuildQuery();
				} 
			} else {
				this.suggestionPromote( -1, text );
			}
			// Move selected entry to top of list
			cookie.splice(idx, 1);
			cookie.unshift( result );
			Cookies.set(goodman.config.previousSearchCookie, cookie, { expires: goodman.config.previousSearchCookieMaxExpiry });
			
			goodman.observer.publish('suggest-select', result);
			this.suggestionClose();
			this.searchClearControlCheck();
		},
		
		/* ====================================================
		 *	Filters
		 * ==================================================*/
		
		filterLabelUpdate: function() {
			//this.filterCountryLabelUpdate();
			this.filterTypeLabelUpdate();
			this.filterSizeLabelUpdate();
			this.filterFundLabelUpdate();
			// Hide reset if nothing to reset
			$('.property_filter_button_reset, .hide-on-no-filters-set', this.container).toggle(
				//this.filterData.country.length > 0 ||
				this.filterData.type.length > 0 ||
				this.filterData.sizeMin > 0 ||
				this.filterData.sizeMax < 100000 ||
				this.filterData.fund.length > 0
			);
		},
		
		filterResetToSelected: function() {
			//this.filterCountryReset();
			this.filterTypeReset();
			this.filterSizeReset();
			this.filterFundReset();
		},
		
		filterReset: function() {
			//this.filterCountryClear();
			this.filterTypeClear();
			this.filterSizeClear();
			this.filterFundClear();
			this.searchRebuildQuery();
		},
		
		
		// Filter - Countries
		/*
		filterCountryClear: function() {
			var countryFilter = $('#property_country_filter_section');
			$('[type="checkbox"]', countryFilter).prop('checked', false);
			goodman.control.checkbox.update();
		},
		
		filterCountrySet: function(country) {
			var countryFilter = $('#property_country_filter_section');
			$('[type="checkbox"]', countryFilter).prop('checked', false).filter('[value="'+country+'"]').prop('checked', true);
			goodman.control.checkbox.update();
		},
		
		filterCountryReset: function() {
			var countryFilter = $('#property_country_filter_section'),
				checkboxes = $('[type="checkbox"]', countryFilter),
				country;
			checkboxes.prop('checked', false); // Uncheck all
			if (this.filterData.country !== undefined) {
				for (country in this.filterData.country) {
					checkboxes.filter('[value="'+this.filterData.country[country]+'"]').prop('checked', true);
				}
			} 
			goodman.control.checkbox.update();
		},
		
		filterCountryLabelUpdate: function() {
			var countryFilter = $('#property_country_filter_section'),
				countries = $('[type="checkbox"][name="country"]:checked', countryFilter),
				label = $('.property_filter_trigger', countryFilter);
			if (countries.length === 0) {
				label.text('Country').removeClass('drop_down_trigger_filled');
			} else if (countries.length === 1) {
				label.text(countries.val()).addClass('drop_down_trigger_filled');
			} else {
				label.html('Country &middot '+countries.length).addClass('drop_down_trigger_filled');
			}
		},
		*/
		
		// Filter - Type
		filterTypeClear: function() {
			var typeFilter = $('#property_type_filter_section');
			$('[type="checkbox"]', typeFilter).prop('checked', false);
			goodman.control.checkbox.update();
		},
		
		filterTypeReset: function() {
			var typeFilter = $('#property_type_filter_section'),
				checkboxes = $('[type="checkbox"]', typeFilter),
				type;
			checkboxes.prop('checked', false); // Uncheck all
			if (this.filterData.type !== undefined) {
				for (type in this.filterData.type) {
					checkboxes.filter('[value="'+this.filterData.type[type]+'"]').prop('checked', true);
				}
			} 
			goodman.control.checkbox.update();
		},
		
		filterTypeLabelUpdate: function() {
			var typeFilter = $('#property_type_filter_section'),
				types = $('[type="checkbox"][name="type"]:checked', typeFilter),
				label = $('.property_filter_trigger', typeFilter),
				TypeStr = '';
			if (types.length === 0) {
				label.text('Property type').removeClass('drop_down_trigger_filled');
			} else if (types.length === 1) {
				switch (types.val()) {
					case 'I':
						TypeStr = 'Industrial';
						break;
					case 'C':
						TypeStr = 'Commercial';
						break;
					case 'L':
						TypeStr = 'Land';
						break;
				}
				label.text(TypeStr).addClass('drop_down_trigger_filled');
			} else {
				label.html('Property type &middot '+types.length).addClass('drop_down_trigger_filled');
			}
		},
		
		
		// Filter - Property size
		filterSizeClear: function() {
			var sizeFilter = $('#property_size_filter_section');
			$('[name="min"]', sizeFilter).val(0);
			$('[name="max"]', sizeFilter).val('unlimited');
			goodman.control.range.update();
		},
		
		filterSizeReset: function() {
			var sizeFilter = $('#property_size_filter_section'),
				inputMin = $('[name="min"]', sizeFilter),
				inputMax = $('[name="max"]', sizeFilter),
				minVal = this.filterData.sizeMin,
				maxVal = this.filterData.sizeMax,
				testVal;
			for (test in goodman.config.sizeSteps) {
				testVal = goodman.config.sizeSteps[test];
				if ( testVal == minVal ) { inputMin.val(goodman.config.sizeSteps[test]); }
				if ( testVal == maxVal ) { inputMax.val(goodman.config.sizeSteps[test]); }
			}
			goodman.control.range.update(); 
		},
		
		filterSizeLabelUpdate: function() {
			var sizeFilter = $('#property_size_filter_section'),
				sizeMin = $('[name="min"]', sizeFilter).val(),
				sizeMax = $('[name="max"]', sizeFilter).val(),
				label = $('.property_filter_trigger', sizeFilter);
			if ( sizeMin == 0 && sizeMax == 'unlimited' ) {
				label.text('Property size').removeClass('drop_down_trigger_filled');
			} else {
				label.text(goodman.util.sizeFormat(sizeMin) + ' - ' + goodman.util.sizeFormat(sizeMax) + ' ' + goodman.config.sizeUnit).addClass('drop_down_trigger_filled');
			}
		},
		

		renderFilterSize: function() {
			var rangeContainer = $('.size-range-selector', this.filterContainer),	
				min = $('[name="min"]', rangeContainer).val(),
				max = $('[name="max"]', rangeContainer).val(),
				scaleChanged = false;
			
			// Change size?
			scaleChanged = ((this.filterData.view === 'lease' && goodman.config.sizeSteps !== goodman.config.sizeStepsLease) || 
							(this.filterData.view !== 'lease' && goodman.config.sizeSteps === goodman.config.sizeStepsLease) );
			goodman.config.sizeSteps = (this.filterData.view === 'lease')?goodman.config.sizeStepsLease:goodman.config.sizeStepsProperty;
			
			rangeContainer.html( goodman.template['property-list-filter-size-range'].hbs( goodman.config ) );
			goodman.control.range.init();
			if (scaleChanged) {
				$('[name="min"]', rangeContainer).val(0);
				$('[name="max"]', rangeContainer).val('unlimited');
			} else {
				$('[name="min"]', rangeContainer).val(min);
				$('[name="max"]', rangeContainer).val(max);
			}
			goodman.control.range.update();
			this.filterSizeLabelUpdate();
		},
		
		
		// Filter - Funds
		filterFundClear: function() {
			var fundFilter = $('#property_fund_filter_section');
			$('[type="checkbox"]', fundFilter).prop('checked', false);
			goodman.control.checkbox.update();
		},
		
		filterFundReset: function() {
			var fundFilter = $('#property_fund_filter_section'),
				checkboxes = $('[type="checkbox"]', fundFilter),
				fund;
			checkboxes.prop('checked', false); // Uncheck all
			if (this.filterData.fund !== undefined) {
				for (fund in this.filterData.fund) {
					checkboxes.filter('[value="'+this.filterData.fund[fund]+'"]').prop('checked', true);
				}
			} 
			goodman.control.checkbox.update();
		},
		
		filterFundLabelUpdate: function() {
			var fundFilter = $('#property_fund_filter_section'),
				funds = $('[type="checkbox"][name="fund"]:checked', fundFilter),
				label = $('.property_filter_trigger', fundFilter);
			if (funds.length === 0) {
				label.text('Fund').removeClass('drop_down_trigger_filled');
			} else if (funds.length === 1) {
				label.text(funds.val()).addClass('drop_down_trigger_filled');
			} else {
				label.html('Funds &middot '+funds.length).addClass('drop_down_trigger_filled');
			}
		},
		
		mobileSearchFixed: false,
		
		// scrollResults
		scrollResults: function(newtop) {
			var offset = $('.property_list_title_wrapper').outerHeight();
			
			// If result set is too short do not retract
			if ( !this.scrollResultsCanHappan() ) {
				$('.scroll-pane', this.container).toggleClass('slid-up', false);
				$('#property-list-search, #property-list-filter').css({top:0});
				this.suggestionClose();
				return;
			}
			
			if (newtop < -20) {
				$('.scroll-pane', this.container).toggleClass('slid-up', true);
				$('#property-list-search, #property-list-filter').css({top:-1*offset});
			} else {
				$('.scroll-pane', this.container).toggleClass('slid-up', false);
				$('#property-list-search, #property-list-filter').css({top:0});
			}
			/*
			if (goodman.config.isMobile) {
				offset = offset + 2;
				if ( newtop < offset ) {
					this.mobileSearchFixed = false;
					$('#property-list-search, #property-list-filter').css({top:-1*newtop});
				} else if (!this.mobileSearchFixed) {
					$('#property-list-search, #property-list-filter').css({top:-1*offset});
					this.mobileSearchFixed = true;
				}
			}
			*/
			this.suggestionClose();
		},
		
		scrollResultsCanHappan: function() {
			return $('#property-list-results', this.container).outerHeight() > $('.scroll-pane', this.container).height();
		},
		
		// Country abbriviations remap
		countryAbbrRemap: function( country ) {
			return ( goodman.config.countryAbbrRemap[country.toLowerCase()] )?goodman.config.countryAbbrRemap[country.toLowerCase()]:country;
		},
		
	});

})(jQuery);
