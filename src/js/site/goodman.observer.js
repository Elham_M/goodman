/*================================================================================
	goodman.observer
	
	Observer 
	
	goodman.observer.subscribe('example-event', callbackFunction, [context]);
	goodman.observer.unsubscribe('example-event', callbackFunction, [context]);
	goodman.observer.publish('example-event', data);
	

================================================================================*/

(function($){
	window.goodman.observer = $.extend(true, window.goodman.observer || {}, {
		
		topics: {},
		
		subscribe: function(topic, callback, context) {
			this.register(topic);
			if (context === undefined) {
				this.topics[topic].subscribe( callback );
			} else {
				this.topics[topic].subscribe( function() { callback.apply(context,arguments); } );
			}
		},
		
		unsubscribe: function(topic, callback, context) {
			if (context === undefined) {
				this.topics[topic].unsubscribe( callback );
			} else {
				this.topics[topic].unsubscribe( function() { callback.apply(context,arguments); } );
			}
			
		},
		
		publish: function() {
			var args = Array.prototype.slice.call(arguments),
				topic = args.shift();
			goodman.util.log('goodman.observer.publish', topic, args);
			this.register(topic);
			this.topics[topic].publish.apply(this.topics[topic], args);
		},
		
		register: function(topic) {
			var callbacks,
				method,
				topicObj = topic && this.topics[ topic ];
				
			if ( !topicObj ) {
				callbacks = jQuery.Callbacks();
				topicObj = {
					publish: callbacks.fire,
					subscribe: callbacks.add,
					unsubscribe: callbacks.remove
				};
				if ( topic ) {
					this.topics[ topic ] = topicObj;
				}
			}
		}
		
	});
})(jQuery);