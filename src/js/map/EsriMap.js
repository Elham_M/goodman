http://powerlink.prod.acquia-sites.com/customer-consumer-panel
    var widget;
var selected;
var map;
var voteOnIncident;

require([
  "esri/map",
  "esri/tasks/QueryTask",
  "esri/toolbars/edit",
  "esri/graphic",
  "esri/geometry/Extent",
  "esri/SpatialReference",
  "esri/geometry/Point",
  "esri/tasks/RelationshipQuery",
  "esri/layers/FeatureLayer",
  "esri/symbols/PictureMarkerSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/dijit/editing/Editor",
  "esri/dijit/editing/TemplatePicker",
  "esri/config",
  "esri/request",
  "dojo/_base/array",
  "esri/Color",
  "dojo/parser",
  'dojo/_base/lang',
  "esri/tasks/query",
  "esri/InfoTemplate",
  "dojo/dom",
  "dojo/on",
  "esri/dijit/Search",
  "esri/renderers/ClassBreaksRenderer",
  "extras/ClusterLayer",
  "dojo/domReady!"
], function (
  Map, QueryTask, Edit, Graphic, Extent, SpatialReference, Point, RelationshipQuery,
  FeatureLayer,
  PictureMarkerSymbol, SimpleLineSymbol,
  Editor, TemplatePicker,
  esriConfig, esriRequest,
  arrayUtils, Color, parser, lang, Query, InfoTemplate, dom, on, Search, ClassBreaksRenderer, ClusterLayer
) {
    var propertyLayerName = "property";
    var propertyLayerUrl = "https://smartspace.goodman.com/arcgis/sharing/servers/7f7b7f8251eb46f0b2f8a4a5fefb8692/rest/services/goodman/MapServer/1";
    var localityLayerUrl = "https://smartspace.goodman.com/arcgis/sharing/servers/7f7b7f8251eb46f0b2f8a4a5fefb8692/rest/services/goodman/MapServer/0";
    var shortestpathLayerUrl = "https://smartspace.goodman.com/arcgis/sharing/servers/7f7b7f8251eb46f0b2f8a4a5fefb8692/rest/services/goodman/MapServer/2";
    var propertyLayertDefinitionUrl = "https://smartspace.goodman.com/arcgis/sharing/servers/7f7b7f8251eb46f0b2f8a4a5fefb8692/rest/services/goodman/MapServer/layers";
    esri.arcgis.utils.arcgisUrl = "https://smartspace.goodman.com/arcgis/sharing/rest/content/items";
    var mapid = "93b96dfd694447d29d14ae3800b45724";

    //var propertyLayerName = "property";
    //var propertyLayerUrl = "https://smartspace.goodman.com/arcgis/rest/services/goodman/MapServer/1";
    //var localityLayerUrl = "https://smartspace.goodman.com/arcgis/rest/services/goodman/MapServer/0";
    //var shortestpathLayerUrl = "https://smartspace.goodman.com/arcgis/rest/services/goodman/MapServer/2";
    //var propertyLayertDefinitionUrl = "https://smartspace.goodman.com/arcgis/rest/services/goodman/MapServer/layers";
    //esri.arcgis.utils.arcgisUrl = "https://smartspace.goodman.com/arcgis/sharing/rest/content/items";
    //var mapid = "93b96dfd694447d29d14ae3800b45724";

    //var propertyLayerName = "property";
    //var propertyLayerUrl = "https://goodman-gpp-dev.esriaustraliaonline.com.au/arcgis/rest/services/goodman/MapServer/1";
    //var localityLayerUrl = "https://goodman-gpp-dev.esriaustraliaonline.com.au/arcgis/rest/services/goodman/MapServer/0";
    //var shortestpathLayerUrl = "https://goodman-gpp-dev.esriaustraliaonline.com.au/arcgis/rest/services/goodman/MapServer/2";
    //var propertyLayertDefinitionUrl = "https://goodman-gpp-dev.esriaustraliaonline.com.au/arcgis/rest/services/goodman/MapServer/layers";
    //esri.arcgis.utils.arcgisUrl = "https://goodman-gpp-dev.esriaustraliaonline.com.au/arcgis/sharing/rest/content/items";
    //var mapid = "1f520a1a788c4414aa7bbc10378e761a";

    //var propertyLayerName = "mygdb.gisdba.property";
    //var propertyLayerUrl = "https://lea-304813.services.esriaustralia.com.au/arcgis/rest/services/goodman/MapServer/0";
    //var localityLayerUrl = "https://lea-304813.services.esriaustralia.com.au/arcgis/rest/services/goodman/MapServer/2";
    //var shortestpathLayerUrl = "https://lea-304813.services.esriaustralia.com.au/arcgis/rest/services/goodman/MapServer/1";
    //var propertyLayertDefinitionUrl = "https://lea-304813.services.esriaustralia.com.au/arcgis/rest/services/goodman/MapServer/layers";

    goodman.observer.subscribe('search', searchproperties);
    goodman.observer.subscribe('search-detail', searchPropertyDetails);
    goodman.observer.subscribe('search-detail-from-propertyid', searchPropertyDetailsFromPropertyId);
    goodman.observer.subscribe('get-children', searchPropertyChildren);
    goodman.observer.subscribe('get-related', getRelatedRecord);
    goodman.observer.subscribe('get-related-from-propertyid', getRelatedRecordFromPropertyId);
    goodman.observer.subscribe('search-suggest', searchSuggest);
    goodman.observer.subscribe('search-locate', searchLocate);
    goodman.observer.subscribe('suggest-select', searchSelect);
    goodman.observer.subscribe('set-map-type', changeMapBasemap);
    goodman.observer.subscribe('select-locality', selectLocality);
    goodman.observer.subscribe('detail-open', detailOpen);
    goodman.observer.subscribe('detail-close', detailClose);
    //goodman.observer.subscribe('get-location', getLocation);
    goodman.observer.subscribe('set-location', setLocation);
    goodman.observer.subscribe('property-hover', highlightProperty);
    goodman.observer.subscribe('property-hover-end', removeHighlightedProperty);
    goodman.observer.subscribe('map-expand-to-show-results', expandToShowResults);



    var relationShipsDict;
    //parser.parse();
    // refer to "Using the Proxy Page" for more information:  https://developers.arcgis.com/javascript/3/jshelp/ags_proxy.html
    //esriConfig.defaults.io.proxyUrl = "/proxy/";

    //var map = new Map("map", {
    //    basemap: "streets",
    //    center: [153.029209, -27.59089],
    //    zoom: 10
    //});

    var deferred = esri.arcgis.utils.createMap(mapid, "map", {
        mapOptions: {
            slider: true
        }
    });

    var map;
    deferred.then(function (response) {
        map = response.map;

        map.on("load", function () {
            goodman.observer.publish('map-start');
        });

        map.on("extent-change", function changeHandler(evt) {
            searchproperties(goodman.interface.propertysearch.searchBuildQuery(), true);
            changeMapBasemap(currentMapType);
        });
        // map.setExtent(map.fullExtent, true);

        //Filling relationship dictionary based on relationship names and relationship id
        relationShipsDict = new Object();
        fillRelationShipsDictionary();

        changeMapBasemap('satellite');
        searchproperties('', true);
        searchAttach('searchContainer');
        getLocation();

        esriRequest.setRequestPreCallback(myCallbackFunction);
        function myCallbackFunction(args) {
            if (args.url.indexOf("suggest") > -1)
                args.content['location'] = userLocation; //Adding user location to query string parameters
            return args;
        }

    }, function (error) {
        console.log("Error: ", error.code, " Message: ", error.message);
        deferred.cancel();
    });


    function fillRelationShipsDictionary() {
        var requestHandle = esriRequest({
            url: propertyLayertDefinitionUrl,
            content: { f: "json" },
            callbackParamName: "callback"
        });
        requestHandle.then(processLayerInfoResponse);
    }

    function processLayerInfoResponse(response) {
        var layerListByName = new Array();
        if (response != null && response.layers != null) {
            for (var iCnt = 0; iCnt < response.layers.length; iCnt++) {
                if (response.layers[iCnt].type != "Group Layer" && response.layers[iCnt].type != "Annotation SubLayer") {
                    if (layerListByName[response.layers[iCnt].name] == null) {
                        layerListByName[response.layers[iCnt].name] = response.layers[iCnt];
                        if (layerListByName.length == undefined) layerListByName.length = 1;
                        else layerListByName.length = layerListByName.length + 1;
                    }
                }
            }
        }
        if (response != null && response.tables != null) {
            for (var iCnt = 0; iCnt < response.tables.length; iCnt++) {
                if (layerListByName[response.tables[iCnt].name] == null) {
                    layerListByName[response.tables[iCnt].name] = response.tables[iCnt];
                    if (layerListByName.length == undefined) layerListByName.length = 1;
                    else layerListByName.length = layerListByName.length + 1;
                }
            }
        }

        if (layerListByName != null && layerListByName.length > 0) {
            if (layerListByName[propertyLayerName] != null) {
                var layerInfo = layerListByName[propertyLayerName];
                if (layerInfo.relationships != null && layerInfo.relationships.length > 0) {
                    var relationsShips = layerInfo.relationships;
                    for (iRelCnt = 0; iRelCnt < relationsShips.length; iRelCnt++)
                        relationShipsDict[relationsShips[iRelCnt].name] = relationsShips[iRelCnt].id;
                }
            }
        }
    }

    function searchproperties(whereObj, searchCurrentExtent, returnEvent) {
        //Search always just the current extent of map
        searchCurrentExtent = true;

        if (whereObj.where === undefined) {
            whereObj = goodman.interface.propertysearch.searchBuildQuery();
        }

        //Updating the search widget filter on PropertyLayer/Name source based on user input filter
        if (search) {
            //if (!goodman.interface.propertysearch.searchBuildQuery().where.indexOf("displayonglobal") > -1)
            //    search.sources[1].featureLayer.setDefinitionExpression("displayonglobal = '1' AND " + goodman.interface.propertysearch.searchBuildQuery().where);
            //else
            search.sources[1].featureLayer.setDefinitionExpression(goodman.interface.propertysearch.searchBuildQuery().where);
            search.startup();
        }

        var queryTask = new QueryTask(propertyLayerUrl);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = [
			"objectid",
			"propertyid",
			"name",
			"address",
			"region",
			"suburb",
			"country",
			"gppthumbnailimage",
			"leasingthumbnailimage",
			"minleasingsize",
			"maxleasingsize",
			"size_standardunits",
			"propertytype",
			"isleased"
        ];

        if (returnEvent === undefined) {
            returnEvent = 'search-result';
        }

        if (searchCurrentExtent === true) {
            query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_CONTAINS;
            query.geometry = map.extent;
        }

        if (whereObj != '') { query.where = whereObj.where; }
        //execute query
        queryTask.execute(query, function (featureset) {
            //if search function is called after user have searched through search widget (not after user interacting with the map), 
            //then it needs to expand the map extent until finding something in resultset
            if (searchSelected && featureset.features.length == 0) {
                map.setExtent(map.extent.expand(2));
                map.extent = map.extent.expand(2);
                searchproperties(goodman.interface.propertysearch.searchBuildQuery(), true);
            }
            else {
                searchSelected = false;
                showAndSendResult(featureset, returnEvent);
            }
        });
    }

    function searchPropertyDetailsFromPropertyId(propertyid, returnEvent) {
        var queryTask = new QueryTask(propertyLayerUrl);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = ["*"];

        if (returnEvent === undefined) {
            returnEvent = 'search-result';
        }

        query.where = 'propertyid = ' + "'" + propertyid + "'";
        //execute query
        queryTask.execute(query, function (featureset) {
            goodman.observer.publish(returnEvent, featureset);
            //showAndSendResult(featureset, returnEvent);
            map.centerAndZoom(featureset.features[0].geometry, 15);
        });
        queryTask.executeForIds(query).then(function(results){
            var oid = results[0];
            goodman.observer.publish('search-detail', oid, 'detail-open' );
            goodman.observer.publish('get-related', oid, 'resource' );
			goodman.observer.publish('get-related', oid, 'sitefeature' );
			goodman.observer.publish('get-related', oid, 'site' );
			goodman.observer.publish('get-related', oid, 'locality' );
			goodman.observer.publish('get-related', oid, 'localityshortestpath' );
        });
    }

    function searchPropertyDetails(objectId, returnEvent) {
        var queryTask = new QueryTask(propertyLayerUrl);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = ["*"];

        if (returnEvent === undefined) {
            returnEvent = 'search-result';
        }

        query.where = 'Objectid=' + objectId;
        //execute query
        queryTask.execute(query, function (featureset) {
            goodman.observer.publish(returnEvent, featureset);
            //showAndSendResult(featureset, returnEvent);
            map.centerAndZoom(featureset.features[0].geometry, 15);
        });
    }

    function searchPropertyChildren(propertyId, parentId, returnEvent) {
        var queryTask = new QueryTask(propertyLayerUrl);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = ["*"];

        if (returnEvent === undefined) {
            returnEvent = 'search-result';
        }

        query.where = "parentid='" + parentId + "' AND propertyid<>'" + propertyId + "' AND displayonglobal='0' AND isleased='0'";

        queryTask.execute(query, function (featureset) {
            goodman.observer.publish(returnEvent, featureset);
        });
    }

    function showAndSendResult(featureset, returnEvent) {
        var infoTemplate = new InfoTemplate();
        infoTemplate.setTitle(" ");
        //infoTemplate.setContent('<div class="property_map_popup_thumb_container">' +
        //						'	<img src="${leasingthumbnailimage}" alt="${name}" width="230" height="150">' +
        //						'</div>' +
        //						'<div class="property_map_popup_description_container">' +
        //						'	<p class="property_map_popup_type">${propertytype}</p>' +
        //						'	<h3 class="property_map_popup_title">${name}</h3>' +
        //						'	<p class="property_map_popup_address">${address}, ${region}, ${country}</p>' +
        //						'	<p class="property_map_popup_area">${minleasingsize} - ${maxleasingsize} sqm</p>' +
        //						'</div>');
        infoTemplate.setContent(getTextContent);

        function getTextContent(graphic) {
            var returnArr = [],
				typeArr = [],
            	propertytype = graphic.attributes.propertytype,
				size = graphic.attributes.size_standardunits,
				minsize = graphic.attributes.minleasingsize,
				maxsize = graphic.attributes.maxleasingsize;
            types = propertytype.split('');
            for (var i in types) {
                switch (types[i]) {
                    case 'I':
                        typeArr.push('Industrial');
                        break;
                    case 'C':
                        typeArr.push('Commercial');
                        break;
                    case 'L':
                        typeArr.push('Land');
                        break;
                }
            }

            returnArr.push('<div class="property_map_popup_thumb_container" data-object-id="' + graphic.attributes.objectid + '">');
            returnArr.push('<div class="thumb" style="background-image:url(\'' + graphic.attributes.gppthumbnailimage.replace(/^:/, '') + '\');" title="' + graphic.attributes.name + '"></div>');
            returnArr.push('</div><div class="property_map_popup_description_container" data-object-id="' + graphic.attributes.objectid + '">');
            returnArr.push('<p class="property_map_popup_type">' + typeArr.join(', ') + '</p>');
            returnArr.push('<h3 class="property_map_popup_title">' + graphic.attributes.name + '</h3>');
            if (graphic.attributes.region == "") {
                returnArr.push('<p class="property_map_popup_address">' + graphic.attributes.address + ', ' + graphic.attributes.country + '</p>');
			} else {
                returnArr.push('<p class="property_map_popup_address">' + graphic.attributes.address + ', ' + graphic.attributes.region + ', ' + graphic.attributes.country + '</p>');
			}
            returnArr.push('<p class="property_map_popup_area">');
            if (size) {
                returnArr.push(goodman.util.sizeFormat(size, true));
            } else {
                if (maxsize > 0 && maxsize !== minsize) { 
					returnArr.push(goodman.util.sizeFormat(minsize, false) + ' - ' + goodman.util.sizeFormat(maxsize, true)); 
				} else {
					returnArr.push(goodman.util.sizeFormat(minsize, true));
				}
            }
            returnArr.push('</p>');
            return returnArr.join('');
        }

        //remove all graphics on the maps graphics layer
        map.graphics.clear();

        //Performance enhancer - assign featureSet array to a single variable.
        var resultFeatures = featureset.features;

        //Do Clustering on Feature result set ************************
        var data = arrayUtils.map(resultFeatures, function (p) {
            var attributes = p.attributes;
            return {
                "x": p.geometry.x,
                "y": p.geometry.y,
                "attributes": attributes
            };
        });

        var cl = new ClusterLayer({
            map: map,
            features: resultFeatures,
            data: data,
            resolution: map.extent.getWidth() / map.width,
            distance: 80,
            id: "clusters",
            labelColor: "#fff",
            labelOffset: 0,//20,
            singleColor: "#888",
            singleTemplate: infoTemplate

        });

        var renderer = new ClassBreaksRenderer(symbol, "clusterCount");
        symbol = new PictureMarkerSymbol('assets/images/pin_new_40.png', 40, 40);
        //var green = new PictureMarkerSymbol("assets/images/GM_cluster.png", 55, 55).setOffset(0, 10);
        var green = new PictureMarkerSymbol("assets/images/pin-cluster.png", 64, 64).setOffset(0, 0);
        renderer.addBreak(0, 1, symbol);
        renderer.addBreak(2, 2000, green);

        cl.setRenderer(renderer);
        var clusterLayer = map.getLayer("clusters");
        if (clusterLayer) {
            map.removeLayer(clusterLayer);
        }
        map.addLayer(cl);
        //****************************************************************

        ////Loop through each feature returned
        //for (var i = 0, il = resultFeatures.length; i < il; i++) {
        //    //Get the current feature from the featureSet.
        //    //Feature is a graphic
        //    var graphic = resultFeatures[i];

        //    graphic.setSymbol(symbol);

        //    //Set the infoTemplate.
        //    graphic.setInfoTemplate(infoTemplate);

        //    //Add graphic to the map graphics layer.
        //    map.graphics.add(graphic);
        //}

        goodman.observer.publish(returnEvent, featureset);
        //return featureset;
    }

    function getRelatedRecord(objectid, relationname) {
        var propLayer = new FeatureLayer(propertyLayerUrl, {
            mode: FeatureLayer.MODE_ONDEMAND,
            outFields: ["*"],
            id: "propLayer"
        });

        var relatedQuery = new RelationshipQuery();
        relatedQuery.outFields = ["*"];
        relatedQuery.relationshipId = relationShipsDict[relationname];

        relatedQuery.objectIds = [objectid];
        propLayer.queryRelatedFeatures(relatedQuery, function (featureSet) {
            goodman.observer.publish('return-related', { type: relationname, features: featureSet[objectid] });
            //return featureSet[objectid];
        });
    }

    function getRelatedRecordFromPropertyId(propertyId, relationname) {
        var propLayer = new FeatureLayer(propertyLayerUrl, {
            mode: FeatureLayer.MODE_ONDEMAND,
            outFields: ["*"],
            id: "propLayer"
        });
        throw "need to get objectid from the layer that's returned using propertyid"

        var relatedQuery = new RelationshipQuery();
        relatedQuery.outFields = ["*"];
        relatedQuery.relationshipId = relationShipsDict[relationname];

        relatedQuery.objectIds = [propertyId];
        propLayer.queryRelatedFeatures(relatedQuery, function (featureSet) {
            goodman.observer.publish('return-related', { type: relationname, features: featureSet[propertyId] });
        });
    }

    var search;

    function searchAttach(containerID) {

        //Search Widget 
        search = new Search({
            enableButtonMode: false, //this enables the search widget to display as a single button
            enableLabel: false,
            enableInfoWindow: true,
            maxSuggestions: 4,
            showInfoWindowOnSelect: false,
            map: map
        }, containerID);
        var sources = search.get("sources");

        //Push the sources used to search, by default the ArcGIS Online World geocoder is included. In addition there is a feature layer of US congressional districts. The districts search is set up to find the "DISTRICTID". Also, a feature layer of senator information is set up to find based on the senator name. 
        //create symbol for selected features  
        symbol = new PictureMarkerSymbol('assets/images/pin.png', 44, 56);
        var featureLayer = new FeatureLayer(propertyLayerUrl);
        //Here is where we set the filter
        featureLayer.setDefinitionExpression("displayonglobal = '1'");
        sources.push({
            featureLayer: featureLayer,
            searchFields: ["name", "code"],
            displayField: "name",
            exactMatch: false,
            outFields: ["name", "address", "objectid", "leasingthumbnailimage", "minleasingsize", "maxleasingsize", "country"],
            name: "Property Name",
            highlightSymbol: symbol,
            placeholder: "3708",
            maxResults: 6,
            maxSuggestions: 6,
            zoomScale: 6000,
            enableSuggestions: true,
            minCharacters: 0
        });
        search.set("sources", sources);

        //Assuming Geocoding is the first source
        //search.sources[0].sourceCountry = "AU";
        search.sources[0].categories = ["Address", "Country", "City", "Region", "Zone", "Subregion", "Postal", "Travel and Transport"];
        search.sources[0].outFields = ["country"];
        search.startup();
        search.hide();

        search.on('suggest-results', function (suggestions) { goodman.observer.publish('search-suggest-results', suggestions); });
        // search.on('search-results', function (results) { goodman.observer.publish('search-results', results); }); // I don't need this anymore, Justin

    }

    function searchSuggest(value) {
        search.suggest(value);
    }

    function searchLocate(value) {
        search.search(value);
    }

    var searchSelected = false;
    function searchSelect(value) {
        searchSelected = true;
        //search.select(value);
        if (value.magicKey || value.length) {
            search.search(value);
        } else if (value.name) {
            console.log('Use name', value.name);
            //Search the property Name
            search.activeSourceIndex = 1;
            search.search(value.name);
            //Change to default
            search.activeSourceIndex = 'all';
        } else {
            search.search(value);
        }
    }


    var bmLayers = [];
    var mapLayers = [];
    var currentMapType = "satellite";
    function changeMapBasemap(value) {
        currentMapType = value;
        if (mapLayers) {
            for (var j = 0, jl = map.layerIds.length; j < jl; j++) {
                var currentLayer = map.getLayer(map.layerIds[j]);
                mapLayers.push(currentLayer)
            }
        }

        //var mapLayers = map.getLayersVisibleAtScale();//map.getScale());
        if (mapLayers && bmLayers.length == 0) {
            for (var i = 0; i < mapLayers.length; i++) {
                if (mapLayers[i]._basemapGalleryLayerType) {
                    var bmLayer = map.getLayer(mapLayers[i].id);
                    if (bmLayer) {
                        bmLayers.push(bmLayer);
                    }
                }
            }
        }
        if (bmLayers.length == 3) //DG is available
        {
            if (value === "streets") {
                bmLayers[0].setVisibility(false); //satellite(BingMapsArial)            
                bmLayers[1].setVisibility(false); //satellite (DG)
                bmLayers[2].setVisibility(true);  //streets (World Street Map)
                mapLayers[3].setVisibility(false);//Hybrid Reference Layer Goodman
            } else if (map.getZoom() >= 15) {
                bmLayers[0].setVisibility(false); //satellite(BingMapsArial)       
                bmLayers[1].setVisibility(true);  //satellite (DG)
                bmLayers[2].setVisibility(false); //streets (World Street Map)
                mapLayers[3].setVisibility(true); //Hybrid Reference Layer Goodman
            } else {
                bmLayers[0].setVisibility(true); //satellite(BingMapsArial)       
                bmLayers[1].setVisibility(false); //satellite (DG)
                bmLayers[2].setVisibility(false); //streets (World Street Map)
                mapLayers[3].setVisibility(true); //Hybrid Reference Layer Goodman
            }
        } else //DG is not available
        {
            if (value === "streets") {
                bmLayers[0].setVisibility(false); //satellite            
                bmLayers[1].setVisibility(true);  //streets
                mapLayers[2].setVisibility(false); //Hybrid Reference Layer Goodman
            } else {
                bmLayers[0].setVisibility(true); //satellite
                bmLayers[1].setVisibility(false); //streets
                mapLayers[2].setVisibility(true); //Hybrid Reference Layer Goodman
            }
        }

        goodman.observer.publish('map-type', value);
    }

    function selectLocality(localityid, propertyid) {
        //Show property routes
        var shortestpathLayer = map.getLayer("shortestpathLayer");
        if (shortestpathLayer) {
            map.removeLayer(shortestpathLayer);
        }

        var shortestpathLayer = new FeatureLayer(shortestpathLayerUrl);//,
        //{ mode: esri.layers.FeatureLayer.MODE_SNAPSHOT });
        shortestpathLayer.id = "shortestpathLayer";
        shortestpathLayer.setDefinitionExpression("localityid = '" + localityid + "' and propertyid = '" + propertyid + "'");
        var lineSymbol = new SimpleLineSymbol();
        lineSymbol.color = new Color([165, 65, 150]);
        lineSymbol.width = 5;
        var linerenderer = new esri.renderer.SimpleRenderer(lineSymbol);
        shortestpathLayer.setRenderer(linerenderer);

        var query = new esri.tasks.Query();
        query.where = "localityid = '" + localityid + "' and propertyid = '" + propertyid + "'";
        shortestpathLayer.selectFeatures(query, esri.layers.FeatureLayer.SELECTION_NEW,
            function (features) {
                map.setExtent(features[0].geometry.getExtent().expand(1.8)); //(shortestpathLayer.fullExtent);
            });

        map.addLayer(shortestpathLayer);

        var localityLayer = map.getLayer("localityLayer");
        if (localityLayer) {
            map.removeLayer(localityLayer);
        }

        //Show property localities
        var localityLayer = new FeatureLayer(localityLayerUrl)
        localityLayer.id = "localityLayer";
        localityLayer.setDefinitionExpression("localityid = '" + localityid + "'");
        var pointSymbol = new PictureMarkerSymbol('assets/images/gm_locality.png', 50, 50);
        //var symbol = new esri.symbol.SimpleFillSymbol().setColor(new esri.Color([255, 0, 0, 0.5]));
        var pointrenderer = new esri.renderer.SimpleRenderer(pointSymbol);

        localityLayer.setRenderer(pointrenderer);
        map.addLayer(localityLayer);
    }

    var detailOpenMapExtent;
    function detailOpen(value) {
        detailOpenMapExtent = map.extent;
    }

    function detailClose(value) {
        var localityLayer = map.getLayer("localityLayer");
        if (localityLayer) {
            map.removeLayer(localityLayer);
        }
        var shortestpathLayer = map.getLayer("shortestpathLayer");
        if (shortestpathLayer) {
            map.removeLayer(shortestpathLayer);
        }
        map.setExtent(detailOpenMapExtent);
    }

    var userLocation;
    var isUserInUSAOrUK = false;
    function setLocation(value) {
        userLocation = value.coords.longitude + "," + value.coords.latitude; //This is used as location parameter in search suggest request inorder to prioritize the result.

        console.log("User location is: " + userLocation);

        var locatorSource = search.defaultSource;
        locatorSource.locator.locationToAddress(new Point(value.coords.longitude, value.coords.latitude))
          .then(function (response) {
              var address = response.address;
              var sqftCountries = ["USA", "GBR"];
              console.log(address);
              if ( $.inArray(address.CountryCode, sqftCountries) > -1 ) {
                  isUserInUSAOrUK = true;
                  goodman.observer.publish('set-size-unit', isUserInUSAOrUK ? 'sqft' : 'sqm');
              }
          }, function (error) {
              // Show no address found
              console.log("Error getting user location: ", error.code, " Message: ", error.message);
              isUserInUSAOrUK = false;
              goodman.observer.publish('set-size-unit', isUserInUSAOrUK ? 'sqft' : 'sqm');
          });


        //var usaExtent = new Extent(
        //    -124.7844079,// west long
        //    24.7433195, // south lat
        //    -66.9513812, // east long          
        //    49.3457868, // north lat                     
        //    new SpatialReference({ wkid: 4326 })
        //   );
        //isUserInUSA = usaExtent.intersects(new Point(value.coords.longitude, value.coords.latitude));        
    }


    function getLocation() {
        goodman.observer.publish('get-location');
		/*
		if (goodman.util.hasGeolocation()) {
            navigator.geolocation.getCurrentPosition(function (position) {
                //goodman.observer.subscribe('get-location', getLocation);
                goodman.observer.publish('set-location', position);
            });
        }
		*/
    }
    function highlightProperty(value) {
        var queryTask = new QueryTask(propertyLayerUrl);
        var query = new Query();
        query.returnGeometry = true;
        query.outFields = ["objectid"];
        query.where = "objectid='" + value + "'";

        queryTask.execute(query, function (featureset) {
            map.graphics.clear();
            var graphic = featureset.features[0];
            graphic.setSymbol(new PictureMarkerSymbol('assets/images/pin_new_70.png', 55, 55));
            //graphic.setSymbol(new PictureMarkerSymbol('assets/images/pin_Highlight.png', 50, 50));
            map.graphics.add(graphic);
        });
    }

    function removeHighlightedProperty() {
        map.graphics.clear();
    }

    function expandToShowResults() {
        searchSelected = true;
        map.setExtent(map.extent.expand(2));
        map.extent = map.extent.expand(2);
        searchproperties(goodman.interface.propertysearch.searchBuildQuery(), true);
    }

});




