//gulpfile.js

'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var handlebars = require('gulp-handlebars');
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var AUTOPREFIXER_BROWSERS = [
      'ie >= 9',
      'ie_mob >= 10',
      'ff >= 30',
      'chrome >= 34',
      'safari >= 7',
      'opera >= 23',
      'ios >= 7',
      'android >= 4.4',
      'bb >= 10'
    ];

// Handlebars task
gulp.task('templates', function(){
  return gulp.src('./src/handlebars/*.hbs.html')
    .pipe(handlebars())
    .pipe(wrap('Handlebars.template(<%= contents %>)'))
    .pipe(declare({
      namespace: 'goodman.template',
      noRedeclare: true, // Avoid duplicate declarations
    }))
    .pipe(concat('goodman.templates.js'))
    .pipe(gulp.dest('./smartspace/assets/js/'));
});

// Site JS task
gulp.task('site-js', function(){
    return gulp.src([
			'./src/js/site/goodman.js',
			'./src/js/site/goodman.util.js',
			'./src/js/site/goodman.config.js',
			'./src/js/site/goodman.observer.js',
			'./src/js/site/goodman.handlebars.js',
			'./src/js/site/goodman.control.*.js',
			'./src/js/site/goodman.interface.js',
			'./src/js/site/goodman.interface.*.js',
			'./src/js/site/goodman.init.js'
		])
        .pipe(sourcemaps.init())
        .pipe(concat('goodman.site.js'))
        .pipe(gulp.dest('./smartspace/assets/js/'))
        .pipe(rename('goodman.site.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('sourcemaps/'))
        .pipe(gulp.dest('./smartspace/assets/js/'));
});

// Map JS task
gulp.task('map-js', function(){
    return gulp.src([
			'./src/js/map/*.js'
		])
        .pipe(sourcemaps.init())
        .pipe(concat('goodman.map.js'))
        .pipe(gulp.dest('./smartspace/assets/js/'));
});

// Vendor JS task
gulp.task('vendors-js', function(){
    return gulp.src([
			'./src/js/vendors/*.js'
		])
        .pipe(sourcemaps.init())
        .pipe(concat('goodman.vendors.js'))
        .pipe(gulp.dest('./smartspace/assets/js/'));
});

// iFrame JS task
gulp.task('iframe-js', function(){
    return gulp.src([
			'./src/js/iframe/*.js'
		])
        .pipe(sourcemaps.init())
        .pipe(concat('smartspace-iframe.js'))
        .pipe(gulp.dest('./assets/js/'));
});

// SASS task
gulp.task('sass', function () {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(sourcemaps.write('sourcemaps/'))
        .pipe(gulp.dest('./smartspace/assets/css'));
});

// SASS iframe task
gulp.task('sass-iframe', function () {
    return gulp.src('./src/scss-iframe/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer(AUTOPREFIXER_BROWSERS))
        .pipe(sourcemaps.write('sourcemaps/'))
        .pipe(gulp.dest('./assets/css'));
});


gulp.task('default', ['templates', 'map-js', 'vendors-js', 'site-js', 'iframe-js', 'sass', 'sass-iframe']);

gulp.task('watch', function () {
    gulp.watch([
			'./src/handlebars/*.hbs.html',
			'./src/js/site/**/*.js', 
			'./src/js/map/**/*.js',
			'./src/js/vendors/**/*.js', 
			'./src/js/iframe/**/*.js', 
			'./src/scss/**/*.scss',
			'./src/scss-iframe/**/*.scss'
		], 
		['default']
	);
});



