# Goodman

# Change management

Based on SalesForce ticket# 02882573, Ogilvy indicated they prefer updates via .zip.
So each time a .zip is created and provided to Ogilvy, bump up the version in package.json accordingly.

Work to be included in a production release are merged into an integration branch.
Name of branch formatted as per following:
'prod-integration-[YYYYMMDD]'
where YYYYMMDD is date of expected prod release to Ogilvy (via .zip).

When production integration branch is ready, a pull request is created with 'master' branch as target.

After prod intragration branch is merged into master, a .zip file is created from the latest master version.

## Local Development Setup
  * npm install
  * grunt
## Local Development Setup using gulp
  * activate latest python2 environment
  * npm install
  * npm install natives
  * gulp
# Events

  * goodman.observer.subscribe('example-event', callbackFunction, [context]);
  * goodman.observer.unsubscribe('example-event', callbackFunction, [context]);
  * goodman.observer.publish('example-event', data);

### search

  * Source: JSA
  * Requirement: N/A
  * Purpose: Search  
  * Parameters: `{ type: 'all|industrial|commercial|land', term: 'string-to-search' }`
  
### search-busy

  * Source: ESRI
  * Requirement: N/A
  * Purpose: Indicate when search is processing 
  * Parameters: true|false
  
### search-result

  * Source: Esri
  * Requirement: N/A
  * Purpose: Return the results of a search
  * Parameters: result object  
  
## Search suggestion events

### search-suggest

  * Source: JSA
  * Requirement: N/A
  * Purpose: Request search suggestions
  * Parameters: search term 
  
### search-suggest-results

  * Source: Esri
  * Requirement: N/A
  * Purpose: Return suggestions for a search
  * Parameters: suggestion list object  

### suggest-select

  * Source: JSA
  * Requirement: N/A
  * Purpose: Select a search suggestions
  * Parameters: Suggestion object
  
  
## Property detail events

### detail-request

  * Source: JSA
  * Requirement: N/A
  * Purpose: Request details of single object
  * Parameters: objectID
  
### detail-open

  * Source: Esri
  * Requirement: N/A
  * Purpose: Open detail display panel with supplied data
  * Parameters: Property details object

### get-related
	
  * Source: JSA
  * Requirement: N/A
  * Purpose: Request related resources of single object
  * Parameters: objectID, 'resource'|'sitefeature'|'site'|'locality'
	
### return-related

  * Source: Esri
  * Requirement: N/A
  * Purpose: Return related resources of single object
  * Parameters: Related resources object
  
### get-children
	
  * Source: JSA
  * Requirement: N/A
  * Purpose: Request children/siblings of an object, but not the object itself
  * Parameters: objectID, parentToHaveID (object ID for the parent)

### return-child-properties
	
  * Source: Esri
  * Requirement: N/A
  * Purpose: Return children/siblings of single object
  * Parameters: [featureset of properties]

### select-locality

  * Source: JSA
  * Requirement: N/A
  * Purpose: Select related locality
  * Parameters: locality objectID, property objectID

## Map type

### set-map-type

  * Source: JSA
  * Requirement: N/A
  * Purpose: Request map change type
  * Parameters: "streets"|"satellite"

### map-type

  * Source: Esri
  * Requirement: N/A
  * Purpose: Tell UI the amp type
  * Parameters: "streets"|"satellite"


## Geolocation  
  
### get-location

  * Source: Esri
  * Requirement: N/A
  * Purpose: Request browser return geolocation
  * Parameters: none
  
### set-location

  * Source: JSA
  * Requirement: N/A
  * Purpose: Return browser geolocation
  * Parameters: JS position object 