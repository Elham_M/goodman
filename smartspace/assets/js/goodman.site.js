// Define the goodman namespace.
var goodman = goodman || {};

(function($){

	window.goodman.interface = $.extend(true, window.goodman.interface || {}, {});
	window.goodman.control = $.extend(true, window.goodman.control || {}, {});

})(jQuery);
/*================================================================================
	goodman.util
	
	Utility functions
================================================================================*/

(function($){
	window.goodman.util = $.extend(true, window.goodman.util || {}, {
	
		_timeAtStart: 0,
		
		init: function() {
			this._timeAtStart = (new Date()).getTime();
			goodman.config.isMobile = this.isMobile();
			goodman.config.isMobileSize = this.isMobileSize();
			goodman.config.isMobileOS = this.isMobileOS();
			goodman.config.isIOS = this.isIOS();
			goodman.config.isIE = this.isIE();
			goodman.config.isMacintosh = this.isMacintosh();
			goodman.config.isWindows = this.isWindows();
			$('html')
				.removeClass('no-js')
				.addClass('js')
				.toggleClass('is-mobile', goodman.config.isMobile)
				.toggleClass('is-mobile-size', goodman.config.isMobileSize)
				.toggleClass('is-ios', goodman.config.isIOS)
				.toggleClass('is-ie', goodman.config.isIE)
				.toggleClass('is-macintosh', goodman.config.isMacintosh)
				.toggleClass('is-windows', goodman.config.isWindows)
				.toggleClass('is-mobile-os', goodman.config.isMobileOS);
			this.resizer();
			// if (goodman.config.isMobileOS) { this.stopScrollBounce(); }
		},
		
		log: function() { 
			if(goodman.config.allowLog && typeof console !== 'undefined' && !goodman.config.isIOS) { 
				if (console.log.apply) {
					console.log.apply(console,arguments);
				} else {
					console.log(arguments[0]);
				}
			}
		},
		
		isMobile: function() {
			if (this.isMobileOS()) {
				return this.isMobileSize();
			} else {
				return false; // Always return false if it isn't a mobile OS
			}
		},
		
		isMobileSize: function() {
			return ( $(window).width() <= goodman.config.mobileWidth || $(window).height() <= goodman.config.mobileHeight );
		},
		
		isMobileOS: function() {
			return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
		},
		
		isIOS: function() {
			return /iPhone|iPad|iPod/i.test(navigator.userAgent);
		},
		
		isIE: function() {
			return /MSIE|Trident.*rv\:11\./.test(navigator.userAgent);
		},
		
		isMacintosh: function() {
			return /Mac/.test(navigator.platform);
		},
		
		isWindows: function() {
			return /Win/.test(navigator.platform);
		},
		
		stopScrollBounce: function() {
			$(document).on('touchmove', function(e) {
				var $target = $(e.target);
				if( !$target.hasClass('scroll-pane') ) {
					e.preventDefault(); 
				}
			});
		},
		
		startMapApp: function() {
			if ( !goodman.config.mapAppStarted ) {
				goodman.observer.publish('map-start');
				goodman.config.mapAppStarted = true;
			}
		},
		
		/* ====================================================
		 *	Resizer
		 * ==================================================*/
		
		resizer: function() {
			var self = this,
				resizerIID = -1;
			$(window)
				.on('resize', function(){
					clearTimeout(resizerIID);
					resizerIID = setTimeout(function(){
						var nowIsMobile = self.isMobile();
						goodman.observer.publish('window-resize', {w:$(window).width(), h:$(window).height()});
						/*
						if (goodman.config.isMobile !== nowIsMobile) {
							goodman.config.isMobile = nowIsMobile;
							goodman.observer.publish('layout-change', nowIsMobile);
						}
						*/
					}, 250);
				});
		},
		
		/*===================================================================
		Data storage
		===================================================================*/

		hasLocalStorage: function() {
			try {
				return ('localStorage' in window && window['localStorage'] !== null);
			} catch (e) {
				return false;
			}
		},
		
		
		/* ====================================================
		 *	Geolocation
		 * ==================================================*/
		
		hasGeolocation: function() {
			//return (navigator.geolocation)?true:false;
		},
		
		selfGeoLocate: function() {
			var oldLocation;
			/*
			if ( goodman.util.hasLocalStorage() ) {
				goodman.util.log('selfGeoLocate send stored location', localStorage.getItem('goodman.location'));
				try { oldLocation = JSON.parse( localStorage.getItem('goodman.location') ); } catch (err) {}
				if (oldLocation !== 'undefined' && oldLocation !== '{}') {
					try {
						goodman.observer.publish('set-location', oldLocation);
					} catch (e) {}
				}
			}
			*/
			
			//if (navigator.geolocation) {
			try {
				navigator.geolocation.getCurrentPosition(
					function(position) {
						goodman.observer.publish('set-location', position);
						/*
						goodman.util.log('selfGeoLocate send returned location', position.coords);
						goodman.util.log('selfGeoLocate send returned JSON', JSON.stringify($.extend({}, {coords:$.extend({}, position.coords)}, position)) );
						if ( goodman.util.hasLocalStorage() ) {
							localStorage.setItem('goodman.location', JSON.stringify($.extend({}, {coords:$.extend({}, position.coords)}, position)) );
						}
						*/
					}, 
					function(error) {
						switch(error.code) {
							case error.PERMISSION_DENIED:
								goodman.util.log("getCurrentPosition error: User denied the request for Geolocation.");
								break;
							case error.POSITION_UNAVAILABLE:
								goodman.util.log("getCurrentPosition error: Location information is unavailable.");
								break;
							case error.TIMEOUT:
								goodman.util.log("getCurrentPosition error: The request to get user location timed out.");
								break;
							case error.UNKNOWN_ERROR:
								goodman.util.log("getCurrentPosition error: An unknown error occurred.");
								break;
						}
					},
					{
						// enableHighAccuracy: true,
						// timeout: 5000,
						// maximumAge: 0
					}
				);
			} catch (err) { 
				goodman.util.log('selfGeoLocate error'); 
			}
			//}
		},
		
		/* 
		 * Local storage
		 */
		
		/* ====================================================
		 *	Text and number formating
		 * ==================================================*/

		addLeadingZeroes: function(number, length) {
			var str = number.toString();
			while (str.length < length) {
				str = "0" + str;
			}
			return str;
		},
		
		numberFormat: function (number, decimals, dec_point, thousands_sep) {
			number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
			var n = !isFinite(+number) ? 0 : +number,
				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
				s = '',
				toFixedFix = function (n, prec) {
					var k = Math.pow(10, prec);
					return '' + Math.round(n * k) / k;
				};
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
			if (s[0].length > 3) {
				s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
			}
			if ((s[1] || '').length < prec) {
				s[1] = s[1] || '';
				s[1] += new Array(prec - s[1].length + 1).join('0');
			}
			return s.join(dec);
		},
		
		numberShorten: function (number) {
			var numberStr = (number + '').replace(/[^0-9+\-Ee.]/g, ''),
				number = +numberStr,
				units = ['','k','m','b','t'],
				unitName = '',
				testLenght, shortValue;
				
			
			if (number < 100) { // Special case for numbers less than 100
				if (numberStr.indexOf('.') > -1) { // is decimal
					return {value: this.numberFormat(number, 3-numberStr.indexOf('.')), unit:''}; 
				} else {
					return {value: number, unit:''};
				}
			}
			
			testLenght = (Math.floor(number) + '').length;
			shortValue = Math.round(number/Math.pow(10,(testLenght-3)));
			
			if ((testLenght%3) > 0) {
				shortValue = shortValue / Math.pow(10,(3-(testLenght%3)));
				unitName = units[Math.floor(testLenght/3)];
			} else {
				unitName = units[Math.floor(testLenght/3)-1];
			}
			
			return {value: shortValue, unit:unitName};
				
		},
		
		numberOrdinalFormat: function (i, superscript) {
			var suffix = ['st','nd','rd','th'],
				j = i%10;
				
			if (superscript) { suffix = ['<sup>st</sup>','<sup>nd</sup>','<sup>rd</sup>','<sup>th</sup>']; }
			if (j === 1 && i%100 !== 11) {
				return i + suffix[0];
			}
			if (j === 2 && i%100 !== 12) {
				return i + suffix[1];
			}
			if (j === 3 && i%100 !== 13) {
				return i + suffix[2];
			}
			return i + suffix[3];
		},
		
		dateFormat: function (date, fullMonthName, dateOnly) {
			var dateArray = [],
				monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
				monthFullArray = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			fullMonthName = fullMonthName||false;
			dateOnly = dateOnly||false;
			if (!date.getDate) { date = new Date(date); }	
			if (isNaN(date)) { return ''; }		
			dateArray.push(date.getDate());
			dateArray.push((fullMonthName)?monthFullArray[date.getMonth()]:monthArray[date.getMonth()]);
			dateArray.push(date.getFullYear());
			if (dateOnly) { return dateArray.join(' '); }
			if (date.getHours() < 12) {
				dateArray.push(date.getHours()+':'+this.addLeadingZeroes(date.getMinutes(),2)+'AM');
			} else if (date.getHours() == 12) {
				dateArray.push('12:'+this.addLeadingZeroes(date.getMinutes(),2)+'PM');
			} else {
				dateArray.push((date.getHours()-12)+':'+this.addLeadingZeroes(date.getMinutes(),2)+'PM');
			}
			return dateArray.join(' ');
		},
		
		monthYearFormat: function (date, fullMonthName) {
			var dateArray = [],
				monthArray = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
				monthFullArray = ['January','February','March','April','May','June','July','August','September','October','November','December'];
			fullMonthName = fullMonthName||false;
			if (!date.getDate) { date = new Date(date); }	
			if (isNaN(date)) { return ''; }		
			dateArray.push((fullMonthName)?monthFullArray[date.getMonth()]:monthArray[date.getMonth()]);
			dateArray.push(date.getFullYear());
			return dateArray.join(' ');
		},
		
		isFuture: function (date) {
			if (!date.getDate) { date = new Date(date); }	
			return (!isNaN(date) && date > new Date() );
		},
		
		timeSinceStart: function() {
			return (new Date()).getTime() - goodman.util._timeAtStart;
		},
		
		decimalDegreesToDDMMSS: function(dd, useDirection, isLatitude) {
			// useDirection: True to use + / - degrees, false to use N/E/S/W
			// isLatitude: Append N/S or E/W
			var suffix = '';
			if (useDirection) {
				if (isLatitude)
					suffix = degrees > 0 ? 'N' : 'S';
				else
					suffix = degrees > 0 ? 'W' : 'E';
			};
			
			var degrees = dd | 0;
			if (useDirection)
				degrees = Math.abs(degrees);
			var leftovers = (Math.abs(dd) - degrees) * 60;
			var minutes = leftovers | 0;
			if (minutes < 10)
				minutes = "0" + minutes;
			var seconds = ((leftovers - minutes) * 60) | 0;
			if (seconds < 10)
				seconds = "0" + seconds;
			return degrees.toString() + "Â° " + minutes + "\" " + seconds + "' " + suffix;
		},
		
		// http://www.html5rocks.com/en/mobile/fullscreen/
		enterFullScreen: function() {
			var doc = window.document,
				docEl = doc.documentElement,
				requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
			if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
				requestFullScreen.call(docEl);
			}
		},
		exitFullScreen: function() {
			var doc = window.document,
				cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
			if(doc.fullscreenElement || doc.mozFullScreenElement || doc.webkitFullscreenElement || doc.msFullscreenElement) {
				cancelFullScreen.call(doc);
			}
		},
		toggleFullScreen: function() {
			var doc = window.document,
				docEl = doc.documentElement,
				requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen,
				cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;
			
			if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
				requestFullScreen.call(docEl);
			} else {
				cancelFullScreen.call(doc);
			}
		},
		forceFullScreen: function() {
			if(goodman.config.forceFullscreen && goodman.config.isMobile && goodman.config.isMobileOS) {
				this.enterFullScreen();
			}
		},
		
		sqm2sqft: function (sqm) {
			return sqm / 0.092903;
			//return 10.76391041671 * sqm;
			//return 10.764 * sqm;
			//return 10 * sqm;
		},
		
		sizeFormat: function (size, showUnit) {
			
			showUnit = showUnit||false;
			showUnit = (showUnit)?' '+goodman.config.sizeUnit:'';
			if (isNaN(parseInt(size))) { return size + ' ' + showUnit; }
			if (goodman.config.sizeUnit === 'sqft')	{
				return this.numberFormat(this.sqm2sqft(size), 0) + showUnit;
			} else {
				return this.numberFormat(size, 0) + showUnit;
			}
		},
		
		
		/* ====================================================
		 *	Add / Remove scroll
		 * ==================================================*/
		
		addCustomSlider: function(selector, restoreScroll, forceUse, callbacks, scrollSpeed) {
			var $selector = $(selector),
				theme = 'minimal-dark',
				scrollRestore,
				optsObj;
			
			scrollRestore = parseInt(restoreScroll);
			if (isNaN(scrollRestore)) { scrollRestore = 0; }
			
			scrollSpeed = parseInt(scrollSpeed);
			if (isNaN(scrollSpeed)) { scrollSpeed = 75; }

			if (goodman.config.isMobile && !forceUse) {
				$selector.scrollTop(scrollRestore * -1);
			} else {
				optsObj = { 
						theme: theme,
						callbacks: {
							onInit: function(){ 
								$(this).scrollTop(0).find('.mCSB_container').css('top', scrollRestore);
							}
						}
					};
				optsObj.callbacks = $.extend(optsObj.callbacks, callbacks||{});
				
				if (goodman.config.isWindows) {
					//if (goodman.config.isIE) {
					//	optsObj = $.extend(optsObj, { scrollInertia: 60, mouseWheel:{ scrollAmount: scrollSpeed } });
					//} else {
						optsObj = $.extend(optsObj, { scrollInertia: 60, mouseWheel:{ scrollAmount: scrollSpeed } });
					//}
				}
				
				$selector
					.scrollTop(scrollRestore * -1)
					.mCustomScrollbar(optsObj);
			}
		},
		
		destroyCustomSlider: function(selector) {
			var $selector = $(selector),
				restoreScroll = 0;
				
			if ($selector.length === 0) { return 0; }
				
			if ($('.mCSB_container', $selector).length === 0) {
				restoreScroll = $selector.scrollTop();
			} else {
				restoreScroll = parseInt($('.mCSB_container', $selector).css('top'));
				$selector.mCustomScrollbar('destroy');
			}
			return restoreScroll;					
		},
		
		getScrollCustomSlider: function(selector) {
			var $selector = $(selector),
				restoreScroll = 0;
			if ($selector.length === 0) { return 0; }
			if ($('.mCSB_container', $selector).length === 0) {
				restoreScroll = $selector.scrollTop();
			} else {
				restoreScroll = parseInt($('.mCSB_container', $selector).css('top'));
			}
			return restoreScroll;					
		},
		
		scrollTopCustomSlider: function(selector, newTop) {
			var $selector = $(selector);
			if (!newTop || newTop === 'undefined') { newTop = 0; }
			if ($('.mCSB_container', $selector).length === 0) {
				$selector.scrollTop(newTop * -1);
			} else {
				$('.mCSB_container', $selector).animate({'top':newTop}, 300);
			}
		},
		
		jumpToCustomSlider: function(selector, newTop) {
			var $selector = $(selector);
			if (!newTop || newTop === 'undefined') { newTop = 0; }
			if ($('.mCSB_container', $selector).length === 0) {
				$selector.scrollTop(newTop * -1);
			} else {
				$('.mCSB_container', $selector).css({'top':newTop});
			}
		},
		
	});
	
	/**
	 * Patch to add touches and targetTouches properties to jQuery.Event objects.
	 */
	/*
	$.each(['touches', 'targetTouches'], function(i, propName){
		if ( $.inArray(propName, $.event.props) < 0 ) {
			$.event.props.push(propName);
		}
	});
	*/
	
})(jQuery);

/*================================================================================
 goodman.config

 Configuration data
================================================================================*/

(function($){
	window.goodman.config = $.extend(true, window.goodman.config || {}, {

		// Allow use of console.log (as qldglobe.util.log)
		allowLog: true,

		// Show the landing screen
		showLanding: true,

		// Is this Goodman' internal view
		isInternal: false,

		// URL for map image disclaimer
		disclaimerURL: 'http://www.goodman.com/property/global-property-portfolio/disclaimer',

		// Small screen layout, should match mobile CSS media query in index.html
		mobileWidth: 991,
		mobileHeight: 991,
		isMobile: false,
		isMobileSize: false,
		isMobileOS: false,
		isIOS: false,
		isIE: false,
		isMacintosh: false,
		isWindows: false,

		// Previous searches cookie
		previousSearchCookie: 'SmartSpaceSearches',
		previousSearchCookieMaxLength: 4,
		previousSearchCookieMaxExpiry: 7, // Days

		// Gallery auto rotate
		galleryAutoRotateDelay: 3000,

		emptyPropertyID: '00000000-0000-0000-0000-000000000000',

		sizeStepsProperty: [0, 15000, 30000, 50000, 70000, 'unlimited'],
		sizeStepsLease: [0, 2000, 5000, 10000, 30000, 'unlimited'],
		sizeSteps: [0, 15000, 30000, 50000, 70000, 'unlimited'],
		sizeUnit: 'sqm',

		shareBaseUrl: window.top.location.origin + window.top.location.pathname,
		//shareBaseUrl: window.top.location,

		funds: [
			'GNAP',
			'GADP',
			'GAIP',
			'GAP',
			'KGIP',
			'GMT',
			'GCLP',
			'GHKLP',
			'GJCP',
			'GEP',
			'KGG',
			'GUKP',
			'GMG'
		],

		mapTypes: [
			{name:'Streets', value:'streets'},
			{name:'Satellite', value:'satellite'}
		],

		countryCode: {
			AUS: 'Australia',
			NZL: 'New Zealand',
			CHN: 'Greater China',
			HKG: 'Greater China',
			JPN: 'Japan',
			BRA: 'Brazil',
			USA: 'USA',
			BEL: 'Belgium',
			CZE: 'Czech Republic',
			DNK: 'Denmark',
			FRA: 'France',
			DEU: 'Germany',
			HUN: 'Hungary',
			ITA: 'Italy',
			NLD: 'Netherlands',
			POL: 'Poland',
			SVK: 'Slovakia',
			ESP: 'Spain',
			SWE: 'Sweden',
			GBR: 'United Kingdom'
		},

		countryAbbrRemap: {
			uk: 'United Kingdom',
			'great britain': 'United Kingdom',
			'united states': 'USA',
			america: 'USA',
			'north america': 'USA',
			nz: 'New Zealand'
		},

		europeanAddress: [
			"Belgium",
			"Czech Republic",
			"Denmark",
			"France",
			"Germany",
			"Hungary",
			"Italy",
			"Netherlands",
			"Poland",
			"Slovakia",
			"Spain",
			"Sweden"
		],

	});

})(jQuery);

/*================================================================================
	goodman.observer
	
	Observer 
	
	goodman.observer.subscribe('example-event', callbackFunction, [context]);
	goodman.observer.unsubscribe('example-event', callbackFunction, [context]);
	goodman.observer.publish('example-event', data);
	

================================================================================*/

(function($){
	window.goodman.observer = $.extend(true, window.goodman.observer || {}, {
		
		topics: {},
		
		subscribe: function(topic, callback, context) {
			this.register(topic);
			if (context === undefined) {
				this.topics[topic].subscribe( callback );
			} else {
				this.topics[topic].subscribe( function() { callback.apply(context,arguments); } );
			}
		},
		
		unsubscribe: function(topic, callback, context) {
			if (context === undefined) {
				this.topics[topic].unsubscribe( callback );
			} else {
				this.topics[topic].unsubscribe( function() { callback.apply(context,arguments); } );
			}
			
		},
		
		publish: function() {
			var args = Array.prototype.slice.call(arguments),
				topic = args.shift();
			goodman.util.log('goodman.observer.publish', topic, args);
			this.register(topic);
			this.topics[topic].publish.apply(this.topics[topic], args);
		},
		
		register: function(topic) {
			var callbacks,
				method,
				topicObj = topic && this.topics[ topic ];
				
			if ( !topicObj ) {
				callbacks = jQuery.Callbacks();
				topicObj = {
					publish: callbacks.fire,
					subscribe: callbacks.add,
					unsubscribe: callbacks.remove
				};
				if ( topic ) {
					this.topics[ topic ] = topicObj;
				}
			}
		}
		
	});
})(jQuery);
/*================================================================================
	goodman.handlebars

	Register Handlebars helpers and partials
================================================================================*/

(function($){
	window.goodman.handlebars = $.extend(true, window.goodman.handlebars || {}, {

		init: function() {

			Handlebars.registerHelper('if_eq', function(a, b, opts) {
				return (a === b)?opts.fn(this):opts.inverse(this);
			});

			Handlebars.registerHelper('if_not_eq', function(a, b, opts) {
				return (a !== b)?opts.fn(this):opts.inverse(this);
			});

			Handlebars.registerHelper('if_bigger', function(a, b, opts) {
				return (a > b)?opts.fn(this):opts.inverse(this);
			});

			/**
			 * Replace all occurrences of `a` with `b`.
			 *
			 * ```handlebars
			 * {{replace "a b a b a b" "a" "z"}}
			 * //=> 'z b z b z b'
			 * ```
			 * @param  {String} `str`
			 * @param  {String} `a`
			 * @param  {String} `b`
			 * @return {String}
			 * @api public
			 */
			Handlebars.registerHelper('replace', function(str, a, b) {
				if (str && typeof str === 'string') {
					if (!a || typeof a !== 'string') return str;
					if (!b || typeof b !== 'string') b = '';
					return str.split(a).join(b);
				}
			});

			Handlebars.registerHelper('hilite', function(str, a, b) {
				if (str && typeof str === 'string') {
					if (!a || typeof a !== 'string') return str;
					if (!b || typeof b !== 'string') b = 'matched_text';
					var re = new RegExp('('+a+')', 'i');
					return new Handlebars.SafeString(str.replace(re, '<span class="'+b+'">$1</span>'));
				}
			});

			Handlebars.registerHelper('linkify', function(str){
				var urlre = /^http(s)?:\/\//i;
				if (urlre.test(str)) {
					str = '<a href="' + str + '" target="_blank" class="external_link">Link</a>';
				}
				return str;
			});

			Handlebars.registerHelper('current_year', function() {
				return new Date().getFullYear();
			});

			// instead of {{> partialName}} use {{partial "templateName"}}
			Handlebars.registerHelper('partial', function (templateName) {
				return new Handlebars.SafeString( goodman.template[templateName].hbs(this) );
			});

			Handlebars.registerHelper("inc", function(value, opts) {
				return parseInt(value) + 1;
			});

			Handlebars.registerHelper ('truncate', function (str, len) {
				if (str && str.length > len && str.length > 0) {
					var new_str = str + " ";
					new_str = str.substr (0, len);
					new_str = str.substr (0, new_str.lastIndexOf(" "));
					new_str = (new_str.length > 0) ? new_str : str.substr (0, len);
					return new Handlebars.SafeString ( new_str +'...' );
				}
				return new Handlebars.SafeString (str);
			});

			Handlebars.registerHelper('pluralize', function(number, single, plural) {
				if (number === 1) { return single; }
				else { return plural; }
			});

			Handlebars.registerHelper('debug', function (context) {
				return new Handlebars.SafeString(
					'<div class="debug">' + JSON.stringify(context) + '</div>'
				);
			});

			Handlebars.registerHelper('dateformat', function(date, longMonth, dateOnly) {
				return new Handlebars.SafeString(
					goodman.util.dateFormat(date, longMonth, dateOnly)
				);
			});

			Handlebars.registerHelper('monthYearFormat', function(date, longMonth) {
				return new Handlebars.SafeString(
					goodman.util.monthYearFormat(date, longMonth)
				);
			});

			Handlebars.registerHelper('if_future', function(date, opts) {
				return goodman.util.isFuture(date)?opts.fn(this):opts.inverse(this);
			});

			Handlebars.registerHelper('numberformat', function(number, decimals, dec_point, thousands_sep) {
				return new Handlebars.SafeString(
					goodman.util.numberFormat(number, decimals, dec_point, thousands_sep)
				);
			});


			/*=======================================================================
			 *	Goodman specific
			 *=====================================================================*/

			Handlebars.registerHelper('cleanurl', function(url) {
				if (url === undefined) { return url; }
				return new Handlebars.SafeString( url.replace(/^:/, '') );
			});

			Handlebars.registerHelper('goodmantypes', function(types) {
				var typeArr = [];
				if (types === undefined) { return ''; }
				types = types.split('');
				for (var i in types) {
					switch (types[i]) {
						case 'I':
							typeArr.push('Industrial');
							break;
						case 'C':
							typeArr.push('Commercial');
							break;
						case 'L':
							typeArr.push('Land');
							break;
					}
				}
				return new Handlebars.SafeString( typeArr.join(', ') );
			});

			Handlebars.registerHelper('goodmanresources', function(resources, className, singular, plural, download) {
				var returnArr = [],
					title, cleanUrl;
				if (resources === undefined || resources.length === 0) { return ''; }

				if (resources.length > 1) {
					returnArr.push('<div class="property_detail_tool_bar_item property_detail_tool_bar_item_'+ className +' drop_down_parent">');
					returnArr.push('<div class="property_detail_tool_bar_item_title drop_down_trigger">'+ plural +'</div>');
					returnArr.push('<div class="drop_down_wrapper"><div class="drop_down_container property_detail_tool_bar_item_sub_list">');
					for (resource in resources) {
						title = resources[resource].title;
						cleanUrl = resources[resource].url.replace(/^:/, '');
						returnArr.push('<a href="'+cleanUrl+'" title="'+title+'" '+((download)?'download target="_blank"':'target="_blank"')+' class="property_detail_tool_bar_item_sub_list_item">'+title+'</a>');
					}
					returnArr.push('</div></div></div>');
				} else {
					title = resources[0].title;
					cleanUrl = resources[0].url.replace(/^:/, '');
					//returnArr.push('<a href="'+cleanUrl+'" title="'+title+'" '+((download)?'download target="_blank"':'target="_blank"')+' class="property_detail_tool_bar_item property_detail_tool_bar_item_'+ className +'">');
					returnArr.push('<a href="'+cleanUrl+'" title="'+title+'" target="_blank" class="property_detail_tool_bar_item property_detail_tool_bar_item_'+ className +'">');
					returnArr.push('<div class="property_detail_tool_bar_item_title">'+ singular +'</div></a>');
				}

				return new Handlebars.SafeString( returnArr.join('') );
			});

			Handlebars.registerHelper('goodmansize', function(size, showUnits) {
				if (showUnits === undefined) { showUnits = true; }
				showUnits = (showUnits)?' '+goodman.config.sizeUnit:'';
				if (goodman.config.sizeUnit === 'sqft') {
					size = goodman.util.sqm2sqft(size);
				}
				return new Handlebars.SafeString(
					goodman.util.numberFormat(size, 0) + showUnits
				);
			});

			Handlebars.registerHelper('goodmansizeunit', function() {
				return new Handlebars.SafeString(
					goodman.config.sizeUnit
				);
			});

			Handlebars.registerHelper('goodmannameurlsafe', function(name) {
				var newName = name;
				try{
					newName = new Handlebars.SafeString(
						name.replace(/\s{2,}/g,"").toLowerCase().split(' ').join('-')
					);
				} catch(err) {
						console.log("There was a problem updating the property name");
				}
				return (newName)
			});

			Handlebars.registerHelper('goodmanaddress', function(address, suburb, region, country, linebreak) {
				var returnArr = [],
					euroAddress = false;

				if (linebreak === undefined) { linebreak = false; }

				/*
					address will end with a comma
					subburb = postcode
					region = subburb

				*/

				if ( $.inArray(country, goodman.config.europeanAddress) > -1 ) {
					euroAddress = true;
				}
				if ( suburb === '' && region === '' ) {
					returnArr.push(address.replace(/,$/, ''));
				} else {
					//returnArr.push(address + ((linebreak)?'<br>':''));
					returnArr.push(address + ',');
					if ( euroAddress ) {
						returnArr.push(suburb);
						returnArr.push(region);
					} else {
						returnArr.push(region);
						returnArr.push(suburb);
					}
				}

				return new Handlebars.SafeString( returnArr.join(' ') );

			});

		}

	});

})(jQuery);

/*================================================================================
	goodman.control.checkbox
================================================================================*/

(function($){

	window.goodman.control.checkbox = $.extend(true, window.goodman.control.checkbox || {}, {

		init: function() {
			this.$body = $('#goodman-smartspace-app');
			this.update();
			this.enable();
		},

		enable: function() {
			var self = this;
			this.$body
				.off('click.checkbox')
				.on('click.checkbox','.general_style_checkbox_display',function(){
					var checkbox_container = $(this).closest('.general_style_checkbox_container'),
						checkbox_input = checkbox_container.find('.general_style_checkbox_input');
					checkbox_input.click();
				})
				.on('click.checkbox','.general_style_checkbox_input',function(){
					var checkbox_input = $(this),
						checkbox_container = checkbox_input.closest('.general_style_checkbox_container');
					checkbox_container.toggleClass('general_style_checkbox_container_checked', checkbox_input.prop('checked'));
				})
				.on('click.checkbox','.general_style_checkbox_container .general_style_label',function(){
					if (!$(this).attr('for')) {
						var checkbox_container = $(this).closest('.general_style_checkbox_container'),
							checkbox_input = checkbox_container.find('.general_style_checkbox_input');
						checkbox_input.click();
					}
				})
			
				.on('click.checkbox','.general_style_radio_display',function(){
					var radio_container = $(this).closest('.general_style_radio_container'),
						radio_input = radio_container.find('.general_style_radio_input');
					radio_input.click();
				})
				.on('click.checkbox','.general_style_radio_input',function(){
					setTimeout( self.updateRadio, 10 ); // Timeout to let click register
				})
				.on('click.checkbox','.general_style_radio_container .general_style_label',function(){
					if (!$(this).attr('for')) {
						var radio_container = $(this).closest('.general_style_radio_container'),
							radio_input = radio_container.find('.general_style_radio_input');
						radio_input.click();
					}
				});
		},
		
		disable: function() {
			this.$body.off('click.checkbox');
		},
		
		update: function() {
			$('.general_style_checkbox_input').each(function(){
				var checkbox_input = $(this),
					checkbox_container = $(this).closest('.general_style_checkbox_container');
				checkbox_container.toggleClass('general_style_checkbox_container_checked', checkbox_input.prop('checked'));
			});
			this.updateRadio();
		},
		
		updateRadio: function() {
			$('.general_style_radio_input').each(function(){
				var radio_input = $(this),
					radio_container = $(this).closest('.general_style_radio_container');
				radio_container.toggleClass('general_style_radio_container_checked', radio_input.prop('checked'));
			});	
		},
		
	});

})(jQuery);



/*================================================================================
	goodman.control.dropdown
================================================================================*/

(function($){

	window.goodman.control.dropdown = $.extend(true, window.goodman.control.dropdown || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			this.enable();
		},

		enable: function() {
			var self = this;
			this.$body
				.off('click.drop-down')
				.on('click.drop-down', '.drop_down_trigger', function(event){
					event.preventDefault();
					var drop_down_parent = $(this).parents('.drop_down_parent'),
						drop_down_position;
					if ($(this).data('trigger') && $('#'+$(this).data('trigger')).length > 0) {
						drop_down_parent = $('#'+$(this).data('trigger'));
					}
					var drop_down_wrapper = drop_down_parent.children('.drop_down_wrapper');
					if (drop_down_parent.hasClass('drop_down_expand')) {
						drop_down_wrapper
							.animate({
								'height':0
							},300,function(){
								drop_down_parent.removeClass('drop_down_expand');
								$(this).css({'height':''});
							});
					} else {
						drop_down_position = self.getPosition(drop_down_parent);
						drop_down_wrapper
							.toggleClass('left', (drop_down_position === 'left'))
							.toggleClass('right', (drop_down_position === 'right'))
							.toggleClass('half-right', (drop_down_position === 'half-right'))
							.animate({
								'height':drop_down_wrapper.children('.drop_down_container').height()
							},300,function(){
								drop_down_parent.addClass('drop_down_expand');
								$(this).css({'height':''});
							});
					}
				});
			
		},
		
		disable: function() {
			this.$body.off('click.drop-down');
		},
		
		getPosition: function($el) {
			var containerWidth = $el.parent().width(),
				posLeft = $el.position().left,
				relative = posLeft / containerWidth,
				posStr = 'center';
			
			if (relative < 0.34) { 
				posStr = 'left'; 
			} else if (relative > 0.66) { 
				posStr = 'right'; 
			}
			if (relative > 0.5) { 
				posStr = 'half-right'; 
			}
			return posStr;
		},
		
	});

})(jQuery);
/*================================================================================
	goodman.control.gallery
================================================================================*/

(function($){

	window.goodman.control.gallery = $.extend(true, window.goodman.control.gallery || {}, {

		galleryRotateIID: -1,
		
		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			$('.gallery_container').each(function(){
				var gallery_container = $(this),
					image_list = gallery_container.find('.gallery_image_container');
				
				gallery_container.find('.gallery_image_container:eq(0)').addClass('active');
				
				if (image_list.length > 1 && $('.gallery_selector_container', gallery_container).length === 0 ) {
					var gallery_navigation_container = $('<div />',{
						'class':'gallery_selector_container'
					});
					gallery_navigation_container.appendTo(gallery_container);
					for(var i=0;i<image_list.length;i++) {
						$('<div class="gallery_selector"></div>').appendTo(gallery_navigation_container);
					}
					gallery_navigation_container.find('.gallery_selector').eq(0).addClass('gallery_selector_selected');
					gallery_container
						.data('current_index',0)
						.data('total_count',image_list.length)
						.data('auto_advance',true);
					
					$('<div class="gallery_navigator_vignette"></div><div class="gallery_navigator gallery_navigator_previous"></div><div class="gallery_navigator gallery_navigator_next"></div>')
						.appendTo(gallery_container);

				}
			});
			
			this.enable();
		},

		enable: function() {
			var self = this;
			
			this.$body
				.off('click.gallery')
				.on('mouseenter.gallery', '.gallery_container', function(e){
					$(this).closest('.gallery_container').data('auto_advance',false);
				})
				.on('mouseleave.gallery', '.gallery_container', function(e){
					$(this).closest('.gallery_container').data('auto_advance',true);
				})
				.on('click.gallery','.gallery_container .gallery_selector',function(e){
					e.preventDefault();
					e.stopPropagation();
					if(!$(this).hasClass('gallery_selector_selected')) {
						var selector_container = $(this).closest('.gallery_selector_container'),
							gallery_container = $(this).closest('.gallery_container'),
							target_index = selector_container.find('.gallery_selector').index($(this));
						selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
						selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
						//gallery_container.css('text-indent',(-100*target_index)+'%');
						gallery_container.find('.gallery_image_container')
							.removeClass('active')
							.filter(':eq('+target_index+')')
							.addClass('active');
					}
				})
				.on('click.gallery','.gallery_container .gallery_navigator_previous',function(e){
					var gallery_container = $(this).closest('.gallery_container'),
						selector_container = gallery_container.find('.gallery_selector_container'),
						target_index = selector_container.find('.gallery_selector').index(selector_container.find('.gallery_selector_selected'));
					e.preventDefault();
					e.stopPropagation();
					target_index--;
					if (target_index < 0) {
						target_index = selector_container.find('.gallery_selector').length - 1;
					}
					selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
					selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
					//gallery_container.css('text-indent',(-100*target_index)+'%');
					gallery_container.find('.gallery_image_container')
							.removeClass('active')
							.filter(':eq('+target_index+')')
							.addClass('active');
				})
				.on('click.gallery','.gallery_container .gallery_navigator_next',function(e){
					var gallery_container = $(this).closest('.gallery_container'),
						selector_container = gallery_container.find('.gallery_selector_container'),
						target_index = selector_container.find('.gallery_selector').index(selector_container.find('.gallery_selector_selected'));
					e.preventDefault();
					e.stopPropagation();
					target_index++;
					if (target_index > selector_container.find('.gallery_selector').length - 1) {
						target_index = 0;
					}
					selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
					selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
					//gallery_container.css('text-indent',(-100*target_index)+'%');
					gallery_container.find('.gallery_image_container')
							.removeClass('active')
							.filter(':eq('+target_index+')')
							.addClass('active');
				});
			
			clearInterval(this.galleryRotateIID);
			this.galleryRotateIID = setInterval( self.autoAdvance, goodman.config.galleryAutoRotateDelay );
			
		},
		
		disable: function() {
			this.$body.off('click.gallery');
			clearInterval(this.galleryRotateIID);
		},
		
		autoAdvance: function() {
			$('.gallery_container').each(function(){
				var gallery_container = $(this);
				if ( gallery_container.data('total_count') > 1 && gallery_container.data('auto_advance') ) {
					$('.gallery_navigator_next', gallery_container).trigger('click');
				}
			});
		},
		
		
	});

})(jQuery);



/*================================================================================
	goodman.control.gallerySliding
================================================================================*/

(function($){

	window.goodman.control.gallerySliding = $.extend(true, window.goodman.control.gallerySliding || {}, {

		galleryRotateIID: -1,
		
		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			$('.gallery_container').each(function(){
				var gallery_container = $(this),
					image_list = gallery_container.find('.gallery_image_container');
				
				if (image_list.length > 1 && $('.gallery_selector_container', gallery_container).length === 0 ) {
					var gallery_navigation_container = $('<div />',{
						'class':'gallery_selector_container'
					});
					gallery_navigation_container.appendTo(gallery_container);
					for(var i=0;i<image_list.length;i++) {
						$('<div />',{
							'class':'gallery_selector'
						}).appendTo(gallery_navigation_container);
					}
					gallery_navigation_container.find('.gallery_selector').eq(0).addClass('gallery_selector_selected');
					gallery_container
						.data('current_index',0)
						.data('total_count',image_list.length)
						.data('auto_advance',true);
					
					$('<div />',{
						'class':'gallery_navigator_vignette'
					}).appendTo(gallery_container);
					$('<div />',{
						'class':'gallery_navigator gallery_navigator_previous'
					}).appendTo(gallery_container);
					$('<div />',{
						'class':'gallery_navigator gallery_navigator_next'
					}).appendTo(gallery_container);

				}
			});
			
			this.enable();
		},

		enable: function() {
			var self = this;
			
			this.$body
				.off('click.gallery')
				.on('mouseenter.gallery', '.gallery_container', function(e){
					$(this).closest('.gallery_container').data('auto_advance',false);
				})
				.on('mouseleave.gallery', '.gallery_container', function(e){
					$(this).closest('.gallery_container').data('auto_advance',true);
				})
				.on('click.gallery','.gallery_container .gallery_selector',function(e){
					e.preventDefault();
					e.stopPropagation();
					if(!$(this).hasClass('gallery_selector_selected')) {
						var selector_container = $(this).closest('.gallery_selector_container'),
							gallery_container = $(this).closest('.gallery_container'),
							target_index = selector_container.find('.gallery_selector').index($(this));
						selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
						selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
						gallery_container.css('text-indent',(-100*target_index)+'%');
					}
				})
				.on('click.gallery','.gallery_container .gallery_navigator_previous',function(e){
					var gallery_container = $(this).closest('.gallery_container'),
						selector_container = gallery_container.find('.gallery_selector_container'),
						target_index = selector_container.find('.gallery_selector').index(selector_container.find('.gallery_selector_selected'));
					e.preventDefault();
					e.stopPropagation();
					target_index--;
					if (target_index < 0) {
						target_index = selector_container.find('.gallery_selector').length - 1;
					}
					selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
					selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
					gallery_container.css('text-indent',(-100*target_index)+'%');
				})
				.on('click.gallery','.gallery_container .gallery_navigator_next',function(e){
					var gallery_container = $(this).closest('.gallery_container'),
						selector_container = gallery_container.find('.gallery_selector_container'),
						target_index = selector_container.find('.gallery_selector').index(selector_container.find('.gallery_selector_selected'));
					e.preventDefault();
					e.stopPropagation();
					target_index++;
					if (target_index > selector_container.find('.gallery_selector').length - 1) {
						target_index = 0;
					}
					selector_container.find('.gallery_selector_selected').removeClass('gallery_selector_selected');
					selector_container.find('.gallery_selector').eq(target_index).addClass('gallery_selector_selected');
					gallery_container.css('text-indent',(-100*target_index)+'%');
				});
			
			clearInterval(this.galleryRotateIID);
			this.galleryRotateIID = setInterval( self.autoAdvance, goodman.config.galleryAutoRotateDelay );
			
		},
		
		disable: function() {
			this.$body.off('click.gallery');
			clearInterval(this.galleryRotateIID);
		},
		
		autoAdvance: function() {
			$('.gallery_container').each(function(){
				var gallery_container = $(this);
				if ( gallery_container.data('total_count') > 1 && gallery_container.data('auto_advance') ) {
					$('.gallery_navigator_next', gallery_container).trigger('click');
				}
			});
		},
		
		
	});

})(jQuery);



/*================================================================================
	goodman.control.overlaypopup
================================================================================*/

(function($){

	window.goodman.control.overlaypopup = $.extend(true, window.goodman.control.overlaypopup || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			this.enable();
		},

		enable: function() {
			var self = this;
			this.$body
				.off('click.overlaypopup')
				.on('click.overlaypopup', '.overlay_popup_container', function(e){
					e.stopPropagation();
				})
				.on('click.overlaypopup', '.overlay_popup_close, .overlay_popup_wrapper', function(e){
					self.$body.removeClass('gallery_popup_active resource_popup_active');
				})
				.on('click.overlaypopup', '.gallery_zoom', function(e){
					e.preventDefault();
					self.$body
						.removeClass('gallery_popup_active resource_popup_active')
						.addClass('gallery_popup_active');	
				});
		},
		
		disable: function() {
			this.$body.off('click.overlaypopup');
		},
		
	});

})(jQuery);



/*================================================================================
	goodman.control.checkbox
================================================================================*/

(function($){

	window.goodman.control.range = $.extend(true, window.goodman.control.range || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			// Initilise size bar container
			$('.property_filter_size_bar_container').each(function(){
				var step_point = goodman.config.sizeSteps,
					column_count = step_point.length - 1,
					column_container = $(this).find('.property_filter_size_bar_column_container');
				if ($(this).data('step_point') === undefined) {
					column_container.removeClass('property_filter_size_bar_column_container_5_column');
					column_container.html('');
					for (var i=0;i<column_count;i++) {
						$('<div />',{
							'class':'property_filter_size_bar_column'
						}).appendTo(column_container);
					}
					column_container.addClass('property_filter_size_bar_column_container_'+column_count+'_column');
					$(this).data('step_point',step_point);
				}
			});		
			
			this.update();
			this.enable();
		},

		enable: function() {
			
			this.$body
				.off('.range')
				.on('mousedown.range touchstart.range', '.property_filter_size_bar_container', function(event){
					if ($(event.target).hasClass('property_filter_size_bar_front_start')) {
						$(this).data('mouse_action','set_start');
					}
					if ($(event.target).hasClass('property_filter_size_bar_front_end')) {
						$(this).data('mouse_action','set_end');
					}
				})
				.on('mouseup.range mouseleave.range touchend.range touchcancel.range', '.property_filter_size_bar_container', function(event){
					if ($(this).data('mouse_action')) {
						$(this).data('mouse_action','');
					}
				})
				.on('mousemove.range touchmove.range', '.property_filter_size_bar_container', function(event){
					if (event.touches) { event.pageX = event.touches[0].pageX; }
					if ($(this).data('mouse_action')) {
						var bar_width = $(this).width(),
							bar_left = $(this).offset().left,
							mouse_position = (event.pageX - bar_left) / bar_width,
							bar_step = 1/$(this).find('.property_filter_size_bar_column_container .property_filter_size_bar_column').length,
							bar_start = parseFloat($(this).find('.property_filter_size_bar_front').css('left'))/bar_width,
							bar_end = parseFloat($(this).find('.property_filter_size_bar_front').css('right'))/bar_width;

						if ($(this).data('mouse_action') === 'set_start') {
							var new_bar_start_point = Math.min(Math.round(mouse_position / bar_step), Math.round((1 - bar_end) / bar_step) - 1),
								new_bar_start = new_bar_start_point * bar_step,
								new_bar_start_value = $(this).data('step_point')[new_bar_start_point];
							$(this).parent().find('.property_filter_size_display_start').html( goodman.util.sizeFormat(new_bar_start_value) );
							$(this).parent().find('[name="min"]').val(new_bar_start_value);
							$(this).find('.property_filter_size_bar_front').css('left',(new_bar_start*100)+'%');
							
						}
						if ($(this).data('mouse_action') === 'set_end') {
							var new_bar_end_point = Math.max(Math.round(mouse_position / bar_step), Math.round(bar_start / bar_step) + 1),
								new_bar_end = 1 - new_bar_end_point * bar_step,
								new_bar_end_value = $(this).data('step_point')[new_bar_end_point];
							$(this).parent().find('.property_filter_size_display_end').html( goodman.util.sizeFormat(new_bar_end_value) );
							$(this).parent().find('[name="max"]').val(new_bar_end_value);
							$(this).find('.property_filter_size_bar_front').css('right',(new_bar_end*100)+'%');
						}
					}
				});
			
		},
		
		disable: function() {
			this.$body.off('.range');
		},
		
		update: function() {
			$('.property_filter_size_bar_container').each(function(){
				var $this = $(this),
					$container = $this.closest('.property_filter_content'),
					step_point = $this.data('step_point'),
					bar_step = 1/(step_point.length - 1),
					min = parseInt($('[name="min"]', $container).val()),
					max = parseInt($('[name="max"]', $container).val()),
					minIdx = $.inArray(min, step_point),
					maxIdx = $.inArray(max, step_point);

				if (minIdx === -1) { minIdx = 0; }
				if (maxIdx === -1) { maxIdx = step_point.length - 1; }
				
				var new_bar_start = minIdx * bar_step,
					new_bar_end = 1 - (maxIdx * bar_step);
				
				$container.find('.property_filter_size_display_start').html( goodman.util.sizeFormat(step_point[minIdx]) );
				$this.find('.property_filter_size_bar_front').css('left',(new_bar_start*100)+'%');
				
				$container.find('.property_filter_size_display_end').html( goodman.util.sizeFormat(step_point[maxIdx]) );
				$this.find('.property_filter_size_bar_front').css('right',(new_bar_end*100)+'%');
			});
		}
		
	});

})(jQuery);



/*================================================================================
	goodman.interface
================================================================================*/

(function($){

	window.goodman.interface = $.extend(true, window.goodman.interface || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			
			goodman.observer.subscribe('set-size-unit', self.setSizeUnit, self); 
			
			this.enable();
		},
		
		enable: function() {
			var self = this;
			this.$body
				.on('click', '.page-left-resizer', function(){
					self.$body.toggleClass('page-left-closed');					
				});
		},
		
		setSizeUnit: function(newUnit) {
			if (newUnit !== goodman.config.sizeUnit) {
				goodman.config.sizeUnit = newUnit;
				goodman.observer.publish('change-size-unit', goodman.config.sizeUnit);
			}
			
		},
		
	});

})(jQuery);
/*================================================================================
	goodman.interface.landing
================================================================================*/

(function($){

	window.goodman.interface.landing = $.extend(true, window.goodman.interface.landing || {}, {

		init: function() {
			var self = this;
			this.container = $('#landing-container');
			this.autocompleteContainer = null;
			this.autocompleteContainerIID = -1;
			
			if (!goodman.config.showLanding) { 
				this.container.hide();
				this.container.remove();
				window.parent.postMessage('expand-iframe', '*');
				return false; 
			} 
			
			goodman.observer.subscribe('map-start', self.mapStart, self);
			goodman.observer.subscribe('search-suggest-results', self.suggestionResult, self);
			
			this.container.show();
			this.render();
			this.enable();
		},

		render: function() {
			this.container.html( goodman.template['landing'].hbs({}) );
			this.autocompleteContainer = $('.property_list_search_autocomplete_container', this.container);
			$('.property_list_search_container', this.container).hide();
		},
		
		mapStart: function() {
			$('.loading', this.container).fadeOut(300);
			$('.property_list_search_container', this.container).fadeIn(300);
		},
		
		enable: function() {
			var self = this;
			
			/* Suggestions */
			this.container
				.on('focus.landing keyup.landing', '#property_list_search', function(e){
					e.preventDefault();
					var value = $.trim($(this).val());
					clearTimeout( self.autocompleteContainerIID );
					if (e.which === 38 || e.which === 40 || e.which === 13) { return; }
					if (value !== '') {
						self.autocompleteContainerIID = setTimeout( function(){ goodman.observer.publish('search-suggest', value); }, 250 );
					} else {
						self.previousSearchShow();
					}
				})
				.on('blur.landing', '#property_list_search', function(){
					clearTimeout( self.autocompleteContainerIID );
					self.autocompleteContainerIID = setTimeout( self.suggestionClose, 250 );
				})
				.on('keydown.landing', '#property_list_search', function(e){
					var acWrap = $('.property_list_search_autocomplete_wrapper', self.container),
						suggestions = $('.property_list_search_autocomplete_row', acWrap),
						index = suggestions.filter('.active').index(),
						inputText = $.trim($(this).val());
					switch (e.which) {
						case 38:
							index--;
							if (index === -1) { index = 0; }
							if (index === -2) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 40:
							index++;
							if (index >= suggestions.length) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 13:
							if (index === -1) {
								var text = $.trim($(this).val());
								goodman.observer.publish('suggest-select', inputText );
								goodman.observer.publish('set-promoted', -1, inputText );
								self.copyToSearchAndClose(inputText);
							} else {
								suggestions.filter('.active').trigger('click');
							}
							e.preventDefault();
							
							break;
					}
				
				})
				.on('mouseover.landing', '.property_list_search_autocomplete_row', function(){
					$('.property_list_search_autocomplete_row').removeClass('active');
				})
				.on('click.landing', '[data-suggestion-result]', function(){
					self.suggestionSelect( $(this).data('suggestion-result'), $(this).text() );
				})
				.on('click.landing', '[data-suggestion-saved]', function(){
					self.previousSearchSelect( $(this).data('suggestion-saved'), $(this).text() );
				});
			
		},
		
		disable: function() {
			this.container.off('.landing');
		},
		
		close: function() {
			var self = this;
			window.parent.postMessage('expand-iframe', '*');
			this.container.fadeOut(300, function(){
				self.disable();
				self.container.remove();
				if (goodman.config.isIOS) {
					goodman.interface.propertysearch.renderResults();	
				}
			});		
		},
		
		
		/* ====================================================
		 *	Suggestions
		 * ==================================================*/
		
		suggestionResult: function( suggestions ) {
			var self = this;
			this.suggestionResultData = suggestions;
			
			if ( this.suggestionResultData.numResults > 0 ) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-results'].hbs( this.suggestionResultData ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			} else {
				this.autocompleteContainer
					.html( '' )
					.closest('.property_list_search_autocomplete_wrapper')
					.removeClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		suggestionClose: function() {
			$('.property_list_search_autocomplete_wrapper_expand', this.container).removeClass('property_list_search_autocomplete_wrapper_expand');	
		},
		
		suggestionSelect: function( idx, text ) {
			var self = this,
				s = idx.split(','),
				result = this.suggestionResultData.results[s[0]][s[1]];
			
			$('#property_list_search').val( text );
			if (result.feature && result.feature.attributes.objectid) { 
				goodman.observer.publish('set-promoted', result.feature.attributes.objectid, '' );
			} else {
				goodman.observer.publish('set-promoted', -1, text );
			}
			goodman.interface.propertysearch.previousSearchSave( result );
			goodman.observer.publish('suggest-select', result);
			// this.suggestionClose();
			
			this.copyToSearchAndClose(text);
		},
		
		
		/* ====================================================
		 *	Recent Searches
		 * ==================================================*/
		
		previousSearchShow: function() {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie);
			if (cookie !== undefined && cookie !== '' && cookie.length > 0) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-saved'].hbs( cookie ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		previousSearchClear: function() {
			Cookies.set(goodman.config.previousSearchCookie, '');
		},
		
		previousSearchSelect: function ( idx, text ) {
			var self = this;
			goodman.interface.propertysearch.previousSearchSelect( idx, text );
			// Copy search to search panel
			$('#property-list #property_list_search').val( text ).focus();
			setTimeout ( function() {
				goodman.interface.propertysearch.suggestionClose();
				self.close();
			}, 100);
			
		},
		
		
		/* ====================================================
		 *	Copy to Search and Close
		 * ==================================================*/
		
		copyToSearchAndClose: function ( text ) {
			var self = this,
				searchInput = $('#property-list #property_list_search');
			searchInput.val( text );
			setTimeout ( function() {
				goodman.interface.propertysearch.suggestionClose();
				self.close();
				if (!goodman.config.isMobile) { searchInput.focus(); }
			}, 100);
			
		},
		
		
	});

})(jQuery);



/*================================================================================
	goodman.interface.map
================================================================================*/

(function($){

	window.goodman.interface.map = $.extend(true, window.goodman.interface.map || {}, {

		init: function() {
			var self = this;
			this.container = $('.property_map_container');
			this.typeContainer = $('#map-type-selector-container');
			this.disclaimerContainer = $('#map-disclaimer-container');
			goodman.observer.subscribe('map-start', self.start, self);
			goodman.observer.subscribe('map-type', self.changeType, self);
			this.render();
			this.enable();
		},

		render: function() {
			this.typeContainer.html( goodman.template['property-map-type'].hbs({types:goodman.config.mapTypes}) );
		},

		enable: function() {
			var self = this;

			this.typeContainer
				.on('click', '.property_map_type_link', function(){
					var newType = $(this).data('value');
					goodman.observer.publish('set-map-type', newType);
				});

			this.container
				.on('click', '.esriAttribution', function(){
					if ($(this).hasClass('esriAttributionOpen')) {
						self.disclaimerContainer.css({'bottom': $(this).height() + 5});
					} else {
						self.disclaimerContainer.css({'bottom': ''});
					}

				});
		},

		start: function() {
			console.log("map disclaimer section")
			if (!goodman.config.isInternal && goodman.config.disclaimerURL) {
				$('<div class="map-disclaimer">Images displayed in this map are subject to <a href="'+goodman.config.disclaimerURL+'" target="_blank">this disclaimer</a></div>').appendTo(this.disclaimerContainer);
			}
		},

		changeType: function(type) {
			var self = this;
			$('.property_map_type_link',this.typeContainer)
				.removeClass('active')
				.filter('[data-value="'+type+'"]')
				.addClass('active');
		},

	});

})(jQuery);

/*================================================================================
	goodman.interface.propertydetail
================================================================================*/

(function($){

	window.goodman.interface.propertydetail = $.extend(true, window.goodman.interface.propertydetail || {}, {

		init: function() {
			var self = this;
			this.$body = $('#goodman-smartspace-app');
			this.container = $('#property-detail');
			this.containerScrollpane = $('.scroll-pane', this.container);
			this.popupGalleryContainer = $('#property-gallery-popup');
			this.popupResourceContainer = $('#property-resource-popup');
			this.localitiesContainer = $('#localities-container');
			this.renderAttributes = {};
			this.renderType = 'property';
			this.pidFromUrl = null;

			goodman.observer.subscribe('detail-request', self.request, self);
			goodman.observer.subscribe('detail-open', self.open, self);
			goodman.observer.subscribe('detail-close', self.close, self);
			goodman.observer.subscribe('return-related', self.related, self);
			goodman.observer.subscribe('return-child-properties', self.childProperties, self);

			this.enable();

			this.displayPropertyFromURL();

		},

		render: function( type ) {
			var detailPaneScroll = goodman.util.destroyCustomSlider(this.containerScrollpane);
			if (type === undefined) { type = this.renderType; }
			this.renderType = type;
			switch(type) {
				case 'property':
					this.containerScrollpane.html( goodman.template['property-detail'].hbs(this.renderAttributes) );
					break;
				case 'lease':
					this.containerScrollpane.html( goodman.template['property-detail-lease'].hbs(this.renderAttributes) );
					break;
			}
			this.popupGalleryContainer.html( goodman.template['property-detail-popup-gallery'].hbs({attributes:this.renderAttributes, type:type}) );
			goodman.control.gallery.init();
			goodman.control.overlaypopup.init();
			goodman.util.addCustomSlider(this.containerScrollpane, detailPaneScroll, false, {}, 50);
			this.renderLocalities();
		},

		enable: function() {
			var self = this;
			this.container
				.on('click', '.js-property-detail-close', function(){
					goodman.observer.publish('detail-close');
				})
				.on('click', '.drop_down_trigger', function(){
					var parent = $(this).parent();
					$('.property_detail_tool_bar .drop_down_expand').not(parent).find('.drop_down_trigger').trigger('click');
				})
				.on('click', '[data-propertyview]', function() {
					var propertyview = $(this).data('propertyview');
					switch (propertyview) {
						case 'child':
							self.requestFisrtChild();
							break;
						case 'lease':
							self.requestChildProperties();
							self.render( propertyview );
							break;
						default:
							self.render( propertyview );
					}
			 	})
				.on('click', '[data-scrollto]', function() {
				 	var scrollDest = $( $(this).data('scrollto'), self.container );
				 	goodman.util.scrollTopCustomSlider(self.container, -1 * scrollDest.offset().top);
			 	});

			this.localitiesContainer
				.on('click', '[data-locobjid]', function(){
					var localityid = $(this).data('locobjid'),
						propertyid = $(this).data('propobjid');
					$('.active', self.localitiesContainer).removeClass('active');
					$(this).addClass('active');
					goodman.observer.publish('select-locality', localityid, propertyid );
				});

			this.resourcePopupEnable();
		},

		requestFromPropertyId: function(oid) {
			this.renderAttributes = {};
			goodman.observer.publish('search-detail-from-propertyid', oid, 'detail-open' );


		},

		request: function(oid) {
			var searchObj = { objectid:oid, where:'objectid='+oid };
			this.renderAttributes = {};
			goodman.observer.publish('search-detail', oid, 'detail-open' );
			goodman.observer.publish('get-related', oid, 'resource' );
			goodman.observer.publish('get-related', oid, 'sitefeature' );
			goodman.observer.publish('get-related', oid, 'site' );
			goodman.observer.publish('get-related', oid, 'locality' );
			goodman.observer.publish('get-related', oid, 'localityshortestpath' );
		},

		requestFisrtChild: function() {
			if (this.renderAttributes.childproperties === undefined || this.renderAttributes.childproperties.length === 0) {
				return false;
			}
			var childObjectID = this.renderAttributes.childproperties[0].attributes.objectid;
			this.request(childObjectID);
		},

		open: function(data) {
			var feature = data.features[0],
				attributes = feature.attributes,
				type = this.renderType;

			if ( attributes.micrositeurl || attributes.videourl ) {
				attributes.showToolBar = true;
			}

			this.renderAttributes = $.extend(this.renderAttributes, attributes, {
				isInternal: goodman.config.isInternal,
				displaySizeLease: this.displaySizeLease(attributes),
				sizeUnit: goodman.config.sizeUnit,
				shareBaseUrl: goodman.config.shareBaseUrl});

			if (attributes.parentid !== goodman.config.emptyPropertyID) {
				type = 'lease';
				this.requestChildProperties();
			}
			this.render(type);
			if (!goodman.util.isMobileSize) {
				goodman.util.jumpToCustomSlider(this.container, 0);
			}
			$('#goodman-smartspace-app').addClass('property-detail-open').removeClass('property_map_active');
		},

		close: function() {
			var self = this;
			$('#goodman-smartspace-app').removeClass('property-detail-open');
			this.closeLocalities();
			this.renderType = 'property';
			this.renderAttributes = {};
		},

		/* ===============================================
		 *  Related info
		 * =============================================*/

		related: function( relatedObj ) {
			var features = relatedObj.features,
				type = relatedObj.type,
				attributes,
				mergeObj = {};

			if (features === undefined) { return; }
			if (features.features !== undefined) { features = features.features; }

			if (type === 'resource') {
				mergeObj.resource = {};
				this.renderAttributes.resource = {};
				for(key in features) {
					attributes = features[key].attributes;
					if (mergeObj.resource[attributes.type] === undefined) { mergeObj.resource[attributes.type] = []; }
					mergeObj.resource[attributes.type].push(attributes);
					// Separate brochures and plans
					if ( attributes.type === 'download' ) {
						if ( attributes.title.match(/brochure/i) ) {
							if (mergeObj.resource['brochure'] === undefined) { mergeObj.resource['brochure'] = []; }
							mergeObj.resource['brochure'].push(attributes);
						}
						if ( attributes.title.match(/plan/i) ) {
							if (mergeObj.resource['plan'] === undefined) { mergeObj.resource['plan'] = []; }
							mergeObj.resource['plan'].push(attributes);
						}
					}

				}
			} else if (type === 'sitefeature') {
				mergeObj.sitefeature = [];
				this.renderAttributes.sitefeature = {};
				for(key in features) {
					attributes = features[key].attributes;
					mergeObj.sitefeature.push({name:attributes.title, class:attributes.title.toLowerCase().replace(/\s/g,'-').replace(/Ã©/g,'e') });
				}
			} else {
				mergeObj[type] = [];
				this.renderAttributes[type] = {};
				for(key in features) {
					attributes = features[key].attributes;
					mergeObj[type].push(attributes);
				}
			}

			if (mergeObj.locality && mergeObj.locality.length > 0) {
				mergeObj.locality.splice(4);
				mergeObj.localityColumnSize = 12 / mergeObj.locality.length;
			}

			if ( mergeObj.resource ) {
				if ( mergeObj.resource.brochure || mergeObj.resource.plan || mergeObj.resource.virtualtour || mergeObj.resource['360view'] || mergeObj.resource.webcam || mergeObj.resource.three_d_model || mergeObj.resource.location_map) {
					mergeObj.showToolBar = true;
				}
			}

			this.renderAttributes = $.extend(this.renderAttributes, mergeObj);
			this.mergeLocalities();
			this.render();
		},

		mergeLocalities: function() {
			var keyL, keyD;
			if (this.renderAttributes.locality && this.renderAttributes.localityshortestpath) {
				for(keyL in this.renderAttributes.locality) {
					for(keyD in this.renderAttributes.localityshortestpath) {
						if (this.renderAttributes.locality[keyL].localityid === this.renderAttributes.localityshortestpath[keyD].localityid) {
							this.renderAttributes.locality[keyL].distance = this.renderAttributes.localityshortestpath[keyD].distance;
							this.renderAttributes.locality[keyL].traveltime = this.renderAttributes.localityshortestpath[keyD].traveltime;
							break;
						}
					}
				}
			}
		},


		/* ===============================================
		 * Child/Sibling Properties
		 * =============================================*/

		requestChildProperties: function() {
			var selfID = this.renderAttributes.propertyid,
				selfParentID = this.renderAttributes.parentid,
				parentToHaveID = (selfParentID !== goodman.config.emptyPropertyID)? selfParentID : selfID;
			goodman.observer.publish('get-children', selfID, parentToHaveID, 'return-child-properties' );
		},

		childProperties: function(data) {
			if (data.features) {
				this.renderAttributes = $.extend(this.renderAttributes, {'childproperties':data.features});
				this.render();
			}
		},


		/* ===============================================
		 * Localities
		 * =============================================*/

		renderLocalities: function() {
			this.localitiesContainer.html( goodman.template['property-map-locality'].hbs( this.renderAttributes ) );
			var $locals = $('.property_map_bottom_link', this.localitiesContainer),
				maxHeight = -1;
			$locals.each(function(){
				maxHeight = Math.max(maxHeight, Math.abs($(this).outerHeight()));
			});
			if (maxHeight > 92) {
				$locals.css({'padding-left': 8}).find('.locality-type').hide();
				maxHeight = -1;
				$locals.each(function(){
					maxHeight = Math.max(maxHeight, Math.abs($(this).outerHeight()));
				});
			}

			$locals.css( 'min-height', maxHeight );
		},

		closeLocalities: function() {
			this.localitiesContainer.html( '' );
		},


		/* ===============================================
		 *  Display Size
		 * =============================================*/

		displaySizeLease: function(attributes) {
			var sizeArr = [];
			if (goodman.config.sizeUnit === 'sqft') {
				if (attributes.minleasingsize > 0) { sizeArr.push(goodman.util.numberFormat(goodman.util.sqm2sqft(attributes.minleasingsize))); }
				if (attributes.maxleasingsize > 0 && attributes.minleasingsize !== attributes.maxleasingsize) { sizeArr.push(goodman.util.numberFormat(goodman.util.sqm2sqft(attributes.maxleasingsize))); }
			} else {
				if (attributes.minleasingsize > 0) { sizeArr.push(goodman.util.numberFormat(attributes.minleasingsize)); }
				if (attributes.maxleasingsize > 0 && attributes.minleasingsize !== attributes.maxleasingsize) { sizeArr.push(goodman.util.numberFormat(attributes.maxleasingsize)); }
			}
			return sizeArr.join(' - ') + ' ' + goodman.config.sizeUnit;
		},

		/* ===============================================
		 *  Open resources in light box
		 * =============================================*/

		resourcePopupEnable: function() {
			var self = this;

			this.container
				.on('click', '.property_detail_tool_bar a[target="_blank"]:not([download])', function(e){
					if (goodman.config.isMobileSize) { return; }
					var url = $(this).attr('href');
					e.preventDefault();
					self.resourcePopupOpen(url);
				});

			this.popupResourceContainer
				.on('click', '[href="#copyInput"]', function(e) {
					e.preventDefault();
					var copyText = document.getElementById('urlToCopy');
					copyText.select();
					document.execCommand("copy");
				});
		},

		resourcePopupOpen: function(url) {
			this.popupResourceContainer.html( goodman.template['property-detail-popup-iframe'].hbs({url:url}) );
			if (!document.execCommand) { $('[href="#copyInput"]', this.popupResourceContainer).hide(); }
			this.$body.removeClass('gallery_popup_active').addClass('resource_popup_active');
		},

		/* ===============================================
		 *  Display property details based on URL
		 * =============================================*/

		displayPropertyFromURL: function() {
			var self = this;
			// var urlStr = window.top.location.search.match(/pid=(\d+)/),
			// pid = (urlStr)?urlStr[1]||-1:-1;
			var pid = this.getUrlParameter('pid');

			console.log("displayPropertyFromURL", pid)

			if ( pid === -1 || pid === undefined ) { return; }
			pid = pid.split("/")[0]
			this.pidFromUrl = pid;
			goodman.observer.subscribe('map-start', self.openDetailFromURL, self);
		},

		getUrlParameter: function(sParam) {
			// https://stackoverflow.com/a/21903119/906814 - added by Stephen Lead
	    var sPageURL = window.parent.location.search.substring(1),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
	        }
	    }
		},

		openDetailFromURL: function() {
			var self = this;
			goodman.interface.landing.close();
			this.requestFromPropertyId(this.pidFromUrl);
			goodman.observer.unsubscribe('map-start', self.openDetailFromURL, self);
		},

	});

})(jQuery);



/*================================================================================
	goodman.interface.propertysearch
================================================================================*/

(function($){

	window.goodman.interface.propertysearch = $.extend(true, window.goodman.interface.propertysearch || {}, {

		init: function() {
			var self = this;
			this.container = $('#property-list');
			this.searchContainer = $('#property-list-search');
			this.filterContainer = $('#property-list-filter');
			this.resultsContainer = $('#property-list-results');
			this.autocompleteContainer = null;
			this.autocompleteContainerIID = -1;
			
			goodman.observer.subscribe('map-start', self.mapStart, self);
			goodman.observer.subscribe('search-busy', self.searchBusy, self);
			goodman.observer.subscribe('search-result', self.searchResult, self);
			goodman.observer.subscribe('search-suggest-results', self.suggestionResult, self);
			goodman.observer.subscribe('set-promoted', self.suggestionPromote, self);
			goodman.observer.subscribe('change-size-unit', self.renderResults, self);
			goodman.observer.subscribe('change-size-unit', self.renderFilterSize, self);
			goodman.observer.subscribe('window-resize', self.resize, self);
			
			this.searchResultData = null;
			this.searchResultRenderData = [];
			this.searchResultPromote = -1;
			this.searchCountryPromote = '';
			this.suggestionResultData = null;

			this.restrictToCountry = null;
			this.returnToObject = null;
			this.filterData = {};
			
			this.render();
			this.searchRebuildQuery();
			this.filterLabelUpdate();
			this.enable();
		},

		render: function() {
			var self = this,
				searchPaneScroll = goodman.util.destroyCustomSlider($('.scroll-pane', this.container));
			this.searchContainer.html( goodman.template['property-list-search'].hbs({ internal: goodman.config.isInternal }) );
			this.filterContainer.html( goodman.template['property-list-filter'].hbs( goodman.config ) );
			goodman.control.checkbox.init();
			goodman.control.range.init();
			this.autocompleteContainer = $('.property_list_search_autocomplete_container', this.searchContainer);
			goodman.util.addCustomSlider($('.scroll-pane', this.container), searchPaneScroll, false, { whileScrolling: function(){ self.scrollResults(this.mcs.top); } });
		},
		
		renderResults: function() {
			var self = this,
				count = 0, key,
				searchPaneScroll = goodman.util.destroyCustomSlider($('.scroll-pane', this.container)),
				renderData;
			if (this.restrictToCountry === null) {
				for (key in this.searchResultRenderData) { if (this.searchResultRenderData.hasOwnProperty(key)) { count++; } }	
				renderData = this.searchResultRenderData;
			} else {
				count = 1;
				renderData = {};
				renderData[this.restrictToCountry] = this.searchResultRenderData[this.restrictToCountry];
			}
			if (goodman.config.isMobile) { $('.scroll-pane-results', this.container).off('scroll.results-scroll'); }
			this.resultsContainer.html( goodman.template['property-list-results'].hbs({countryCount: count, countries:renderData, restrictToCountry:this.restrictToCountry, sizeUnit:goodman.config.sizeUnit}) );
			goodman.control.gallery.init();
			goodman.util.addCustomSlider($('.scroll-pane', this.container), 0, false, { whileScrolling: function(){ self.scrollResults(this.mcs.top); } });
			
			if (goodman.config.isMobile) {
				$('.scroll-pane-results', this.container).off('scroll.results-scroll').on('scroll.results-scroll', function(){ self.scrollResults( $(this).scrollTop() * -1 ); });
			}
			
			if (this.returnToObject !== null) {
				var returnObj = $('[data-object-id="'+this.returnToObject+'"]', this.resultsContainer);
				if ( returnObj.length && this.scrollResultsCanHappan() ) {
					goodman.util.jumpToCustomSlider($('.scroll-pane', this.container), (-1 * returnObj.position().top) - 90 );
					self.scrollResults(  goodman.util.getScrollCustomSlider($('.scroll-pane', this.container)) );
				} else {
					self.scrollResults(0);
				}
			} else {
				self.scrollResults(0);
			}
			
			this.filterLabelUpdate();
		},
		
		resize: function(newSize) {
			var newScrolltop = this.searchContainer.height() + this.filterContainer.height();
			this.resultsContainer.css({'margin-top': newScrolltop});
		},
		
		mapStart: function() {
			$('#property_list_search', this.searchContainer).attr('placeholder', 'City, address, property name').prop('disabled',false);
			$('.property_list_search_container', this.searchContainer).removeClass('loading');
		},
		
		enable: function() {
			var self = this,
				$body = $('#goodman-smartspace-app');
			
			/* Filters */
			$body
				.on('click', '.property_filter_trigger', function(){
					if ($body.hasClass('property_filter_active')) {
						self.filterResetToSelected();
						if ($(this).hasClass('drop_down_trigger')) {
							if ($(this).closest('.drop_down_parent').hasClass('drop_down_expand')) {
								// If clicked filter section is active, close the filter
								$body.removeClass('property_filter_active');
							} else {
								// If clicked filter section is not active, but the filter is active, close the active filter section
								$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
							}
						} else {
							// If the mobile filter trigger is clicked, close the filter and de-active filter section if there's any
							$body.removeClass('property_filter_active');
							$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
						}
					} else {
						$body.addClass('property_filter_active');
						if (!$(this).hasClass('drop_down_trigger') && $('.property_filter_container > .drop_down_expand').length === 0) {
							// If there is no active filter section, make the first section (country filter) active
							if (goodman.config.isMobileSize) {
								goodman.util.jumpToCustomSlider( $('.scroll-pane', self.container), 0);
							}
							$('.property_filter_container > .drop_down_parent:eq(0)').addClass('drop_down_expand');
						}
					}
				})
				.on('click', '.property_filter_button_apply', function(){
					$body.removeClass('property_filter_active');
					$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
					self.search();
				})
				.on('click', '.property_filter_button_cancel', function(){
					self.filterResetToSelected();
					$body.removeClass('property_filter_active');
					$('.property_filter_container > .drop_down_expand').removeClass('drop_down_expand');
				})
				.on('click', '.property_filter_button_reset', function(){
					self.filterReset();
					self.filterLabelUpdate();
					self.search();
				})
				.on('click', '.property_filter_button_zoom_out', function(){
					goodman.observer.publish('map-expand-to-show-results');
				})
				.on('click', '[name="countrygroup"]', function(){
					var $this = $(this),
						checked = $(this).is(':checked'),
						group = $this.closest('.property_filter_sub_section');
					$('[name="country"]', group).prop('checked', checked);
					goodman.control.checkbox.update();
				})
				.on('click', '[name="forleasefilter"]', function(){
					self.searchRebuildQuery();
					self.renderFilterSize();
					self.search();
				})
				.on('click', '[name="size_unit"]', function(){
					goodman.observer.publish('set-size-unit', $(this).val());
				});

			
			/* Search */
			$body
				.on('submit', '#property_list_search_form', function(e){
					e.preventDefault();
				})
				.on('click', '[href="#searchCountry"][data-country]', function(e){
					e.preventDefault();
					var country = $(this).data('country');
					//self.filterCountrySet(country);
					//self.search();
					self.restrictToCountry = country;
					self.renderResults();
				})
				.on('click', '[data-object-id]', function(e){
					var $this = $(this),
						oid = $(this).data('object-id');
					self.returnToObject = oid;
					self.clearCountryOnResult = false;
					goodman.observer.publish('detail-request', oid);
				})
				.on('mouseenter', '[data-object-id]', function(){
					var $this = $(this),
						oid = $(this).data('object-id');
					$this.addClass('hover');
					goodman.observer.publish('property-hover', oid);
				})
				.on('mouseleave', '[data-object-id]', function(){
					var $this = $(this),
						oid = $(this).data('object-id');
					$this.removeClass('hover');
					goodman.observer.publish('property-hover-end', oid);
				})
				.on('click', '[data-location]', function(e){
					var $this = $(this),
						location = $.trim($(this).data('location'));
					if (location !== '') {
						goodman.observer.publish('search-locate', location);
					}
				})
				.on('click', '.property_list_search_clear', function(e){
					$('#property_list_search').val('');
				});


			/* Suggestions */
			this.container
				.on('focus keyup', '#property_list_search', function(e){
					e.preventDefault();
					var value = $.trim($(this).val());
					clearTimeout( self.autocompleteContainerIID );
					if (e.which === 38 || e.which === 40 || e.which === 13) { return; }
					if (value !== '') {
						self.autocompleteContainerIID = setTimeout( function(){ goodman.observer.publish('search-suggest', value); }, 250 );
					} else {
						self.previousSearchShow();
					}
					self.searchClearControlCheck();
				})
				.on('blur', '#property_list_search', function(){
					clearTimeout( self.autocompleteContainerIID );
					self.autocompleteContainerIID = setTimeout( self.suggestionClose, 250 );
				})
				.on('keydown', '#property_list_search', function(e){
					var acWrap = $('.property_list_search_autocomplete_wrapper'),
						suggestions = $('.property_list_search_autocomplete_row', acWrap),
						index = suggestions.filter('.active').index(),
						inputText = $.trim($(this).val());
					switch (e.which) {
						case 38: // Up
							index--;
							if (index === -1) { index = 0; }
							if (index === -2) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 40: // Down
							index++;
							if (index >= suggestions.length) { index = suggestions.length - 1; }
							suggestions.removeClass('active').filter(':eq('+index+')').addClass('active');
							e.preventDefault();
							break;
						case 13: // Enter
							self.returnToObject = null;
							if (index === -1) {
								goodman.observer.publish('suggest-select', inputText );
								self.suggestionPromote( -1, inputText );
							} else {
								suggestions.filter('.active').trigger('click');
								e.preventDefault();
							}
							break;
					}
				})
				.on('mouseover', '.property_list_search_autocomplete_row', function(){
					$('.property_list_search_autocomplete_row').removeClass('active');
				})
				.on('click', '[data-suggestion-result]', function(){
					self.suggestionSelect( $(this).data('suggestion-result'), $(this).text() );
				})
				.on('click', '[data-suggestion-saved]', function(){
					self.previousSearchSelect( $(this).data('suggestion-saved'), $(this).text() );
				});
			
		},
		
		searchBusy: function(state) {
			goodman.util.log('goodman.interface.propertysearch::searchBusy', state);
		},
		
		searchClearControlCheck: function () {
			$('.property_list_search_clear', this.container).toggle( $.trim($('#property_list_search').val()) !== '' );	
		},
		
		clearCountryOnResult: true,
		
		searchResult: function(data) {
			var renderData = {},
				i, ilen,
				features = data.features,
				attributes, country,
				sortedRenderData;
			
			goodman.util.startMapApp(); // Start app on first search results recieved
			
			// this.restrictToCountry = null;
			
			if (this.clearCountryOnResult) {
				this.countryRestrict();
			} else if ( !$('#goodman-smartspace-app').hasClass('property-detail-open') ) {
				this.clearCountryOnResult = true;
			}
			
			this.searchResultData = data;
			
			for (i=0, ilen=features.length; i<ilen; i++) {
				attributes = features[i].attributes;
				country = $.trim(attributes.country);
				
				//country = this.countryRemap( attributes.objectid, country );  // Clean countries
				
				if (country === '') { country = 'Unknown'; }
				if (renderData[country] === undefined) { renderData[country] = []; }
				
				if (this.searchResultPromote !== -1) {
					if (this.searchResultPromote === attributes.objectid) {
						renderData[country].unshift(attributes);
						this.searchCountryPromote = attributes.country;
					} else {
						renderData[country].push(attributes);
					}
				} else {
					renderData[country].push(attributes);	
				}
			}
			
			if (this.searchCountryPromote !== '') {
				sortedRenderData = {};
				for (country in renderData) {
					if (country.toLowerCase() === this.searchCountryPromote.toLowerCase()) {
						sortedRenderData[country] = renderData[country];
					}
				}
				for (country in renderData) {
					if (country.toLowerCase() !== this.searchCountryPromote.toLowerCase()) {
						sortedRenderData[country] = renderData[country];
					}
				}
				renderData = sortedRenderData;
			}
			
			this.searchResultRenderData = renderData;
			if (!goodman.config.isIOS || $('#landing-container').length === 0) {
				this.renderResults();
			}
			
			if (goodman.config.isMobile) {
				$('#property_list_search').blur();
			}
		},
		
		countryRestrict: function(country) {
			if (country === undefined || country === '') {
				this.restrictToCountry = null;
			} else {
				this.restrictToCountry = this.countryAbbrRemap(country);
			}
		},
		
		searchBuildQuery: function() {
			return this.filterData;
		},
		
		searchRebuildQuery: function() {
			var queryArr = [],
				queryObj = {},
			//	term = $.trim($('#property_list_search').val()),
				forleasefilter = $('[name="forleasefilter"]').prop('checked');
			
			if (!goodman.config.isInternal) {
				queryArr.push('displayonglobal=\'1\'');
				queryObj.view = 'property';
			} else if (forleasefilter) {
				queryArr.push('isleased=\'0\' AND (minleasingsize IS NOT NULL OR maxleasingsize IS NOT NULL)');
				queryObj.view = 'lease';
			} else {
				queryArr.push('displayonglobal=\'1\'');
				queryObj.view = 'property';
			}
			
			// Countries
			/*
			queryObj.country = [];
			$('[name="country"]:checked').each(function(){
				queryObj.country.push($(this).val());
			});
			if (queryObj.country.length) {
				queryArr.push( 'country IN (\'' + queryObj.country.join('\',\'') + '\')' );
			}
			*/
			
			// Types
			queryObj.type = [];
			var typeFilter = $('#property_type_filter_section'),
				checkedTypeFilter = $(':checked', typeFilter),
				typeFilterArray = [];
			checkedTypeFilter.each(function(){
				queryObj.type.push($(this).val());
			});
			if (queryObj.type.length) {
				for (var key in queryObj.type) {
					typeFilterArray.push('LOWER(propertytype) LIKE LOWER(\'%'+ queryObj.type[key] + '%\')');
				}
				queryArr.push( '(' + typeFilterArray.join( ' OR ' ) + ')' );
			}
			
			// Size
			var sizeFilter = $('#property_size_filter_section');
			queryObj.sizeMin = parseInt($('[name="min"]', sizeFilter).val());
			queryObj.sizeMax = parseInt($('[name="max"]', sizeFilter).val());
			if (queryObj.sizeMin > 0) { 
				queryArr.push( ((forleasefilter)?'maxleasingsize':'size_standardunits') + ' >= ' + queryObj.sizeMin );
			}
			//if (queryObj.sizeMax < 100000) { 
			if (!isNaN( queryObj.sizeMax )) {
				queryArr.push( ((forleasefilter)?'minleasingsize':'size_standardunits') + ' <= ' + queryObj.sizeMax );
			}
			
			// Funds
			queryObj.fund = [];
			var fundFilterArray = [];
			$('[name="fund"]:checked').each(function(){
				queryObj.fund.push($(this).val());
			});
			if (queryObj.fund.length) {
				for (var key in queryObj.fund) {
					fundFilterArray.push('LOWER(fund) LIKE LOWER(\'%'+ queryObj.fund[key] + '%\')');
				}
				queryArr.push( '(' + fundFilterArray.join( ' OR ' ) + ')' );
			}
			
			queryObj.where = queryArr.join(' AND ');
			this.filterData = queryObj;
		}, 
		
		searchReset: function() {
			this.filterReset();
			$('#property_list_search').val('');
		},
		
		search: function() {
			this.searchRebuildQuery();
			this.filterLabelUpdate();
			goodman.observer.publish('search', this.searchBuildQuery(), false );
		},
		
		
		/* ====================================================
		 *	Suggestions
		 * ==================================================*/
		
		suggestionResult: function( suggestions ) {
			var self = this;
			this.suggestionResultData = suggestions;
		
			if ( this.suggestionResultData.numResults > 0 ) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-results'].hbs( this.suggestionResultData ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			} else {
				this.autocompleteContainer
					.html( '' )
					.closest('.property_list_search_autocomplete_wrapper')
					.removeClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		suggestionClose: function( ) {
			clearTimeout( this.autocompleteContainerIID );
			$('.property_list_search_autocomplete_wrapper_expand').removeClass('property_list_search_autocomplete_wrapper_expand');	
		},
		
		suggestionPromote: function( objid, country ) {
			var matchArr;
			this.searchResultPromote = objid;
			if (country === undefined || country === '') {
				this.searchCountryPromote = '';
				return;
			}
			country = $.trim(country);
			matchArr = country.match(/([A-Z]{3,3})$/);
			if (matchArr !== null && matchArr.length > 1) {
				this.searchCountryPromote = goodman.config.countryCode[matchArr[1]];
			} else {
				this.searchCountryPromote = this.countryAbbrRemap(country);
			}
		},
		
		suggestionSelect: function( idx, text ) {
			var s = idx.split(','),
				result = this.suggestionResultData.results[s[0]][s[1]];
			$('#property_list_search').val( text );
			if (result.feature && result.feature.attributes.objectid) { 
				this.suggestionPromote( result.feature.attributes.objectid, '' );
			} else {
				this.suggestionPromote( -1, text );
			}
			this.previousSearchSave( result );
			goodman.observer.publish('suggest-select', result);
			this.suggestionClose();
			this.searchClearControlCheck();
		},
		
		/* ====================================================
		 *	Recent Searches
		 * ==================================================*/
		
		previousSearchShow: function() {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie);
			if (cookie !== undefined && cookie !== '' && cookie.length > 0) {
				this.autocompleteContainer
					.html( goodman.template['property-list-suggestion-saved'].hbs( cookie ) )
					.closest('.property_list_search_autocomplete_wrapper')
					.addClass('property_list_search_autocomplete_wrapper_expand');
			}
		},
		
		previousSearchSave: function( data ) {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie),
				newEntry = ( data.feature && data.feature.attributes )?data.feature.attributes:data;
			if (cookie === undefined || cookie === '') { cookie = []; }
			// if value is already in cookie remove (it will be added back to the start)
			for (var i in cookie) {
				if ( cookie[i].name && cookie[i].name === newEntry.name ) { cookie.splice( i, 1 ); break; }
				else if ( cookie[i].text && cookie[i].text === newEntry.text ) { cookie.splice( i, 1 ); break; }
			}
			if ( newEntry.objectid ) { newEntry.showLease = $('[name="forleasefilter"]').prop('checked'); }
			cookie.unshift( newEntry );
			if (cookie.length > goodman.config.previousSearchCookieMaxLength) {
				cookie.splice(-1, cookie.length - goodman.config.previousSearchCookieMaxLength);
			}
			Cookies.set(goodman.config.previousSearchCookie, cookie, { expires: goodman.config.previousSearchCookieMaxExpiry });
		},
		
		previousSearchClear: function() {
			Cookies.set(goodman.config.previousSearchCookie, '');
		},
		
		previousSearchSelect: function (idx, text) {
			var cookie = Cookies.getJSON(goodman.config.previousSearchCookie),
				result = cookie[idx];
			$('#property_list_search').val( text );
			
			if (result.objectid) { 
				this.suggestionPromote( result.objectid, '' );
				if (result.showLease !== undefined && result.showLease !== $('[name="forleasefilter"]').prop('checked')) {
					$('[name="forleasefilter"]').prop('checked', result.showLease);
					goodman.control.checkbox.update();
					this.searchRebuildQuery();
				} 
			} else {
				this.suggestionPromote( -1, text );
			}
			// Move selected entry to top of list
			cookie.splice(idx, 1);
			cookie.unshift( result );
			Cookies.set(goodman.config.previousSearchCookie, cookie, { expires: goodman.config.previousSearchCookieMaxExpiry });
			
			goodman.observer.publish('suggest-select', result);
			this.suggestionClose();
			this.searchClearControlCheck();
		},
		
		/* ====================================================
		 *	Filters
		 * ==================================================*/
		
		filterLabelUpdate: function() {
			//this.filterCountryLabelUpdate();
			this.filterTypeLabelUpdate();
			this.filterSizeLabelUpdate();
			this.filterFundLabelUpdate();
			// Hide reset if nothing to reset
			$('.property_filter_button_reset, .hide-on-no-filters-set', this.container).toggle(
				//this.filterData.country.length > 0 ||
				this.filterData.type.length > 0 ||
				this.filterData.sizeMin > 0 ||
				this.filterData.sizeMax < 100000 ||
				this.filterData.fund.length > 0
			);
		},
		
		filterResetToSelected: function() {
			//this.filterCountryReset();
			this.filterTypeReset();
			this.filterSizeReset();
			this.filterFundReset();
		},
		
		filterReset: function() {
			//this.filterCountryClear();
			this.filterTypeClear();
			this.filterSizeClear();
			this.filterFundClear();
			this.searchRebuildQuery();
		},
		
		
		// Filter - Countries
		/*
		filterCountryClear: function() {
			var countryFilter = $('#property_country_filter_section');
			$('[type="checkbox"]', countryFilter).prop('checked', false);
			goodman.control.checkbox.update();
		},
		
		filterCountrySet: function(country) {
			var countryFilter = $('#property_country_filter_section');
			$('[type="checkbox"]', countryFilter).prop('checked', false).filter('[value="'+country+'"]').prop('checked', true);
			goodman.control.checkbox.update();
		},
		
		filterCountryReset: function() {
			var countryFilter = $('#property_country_filter_section'),
				checkboxes = $('[type="checkbox"]', countryFilter),
				country;
			checkboxes.prop('checked', false); // Uncheck all
			if (this.filterData.country !== undefined) {
				for (country in this.filterData.country) {
					checkboxes.filter('[value="'+this.filterData.country[country]+'"]').prop('checked', true);
				}
			} 
			goodman.control.checkbox.update();
		},
		
		filterCountryLabelUpdate: function() {
			var countryFilter = $('#property_country_filter_section'),
				countries = $('[type="checkbox"][name="country"]:checked', countryFilter),
				label = $('.property_filter_trigger', countryFilter);
			if (countries.length === 0) {
				label.text('Country').removeClass('drop_down_trigger_filled');
			} else if (countries.length === 1) {
				label.text(countries.val()).addClass('drop_down_trigger_filled');
			} else {
				label.html('Country &middot '+countries.length).addClass('drop_down_trigger_filled');
			}
		},
		*/
		
		// Filter - Type
		filterTypeClear: function() {
			var typeFilter = $('#property_type_filter_section');
			$('[type="checkbox"]', typeFilter).prop('checked', false);
			goodman.control.checkbox.update();
		},
		
		filterTypeReset: function() {
			var typeFilter = $('#property_type_filter_section'),
				checkboxes = $('[type="checkbox"]', typeFilter),
				type;
			checkboxes.prop('checked', false); // Uncheck all
			if (this.filterData.type !== undefined) {
				for (type in this.filterData.type) {
					checkboxes.filter('[value="'+this.filterData.type[type]+'"]').prop('checked', true);
				}
			} 
			goodman.control.checkbox.update();
		},
		
		filterTypeLabelUpdate: function() {
			var typeFilter = $('#property_type_filter_section'),
				types = $('[type="checkbox"][name="type"]:checked', typeFilter),
				label = $('.property_filter_trigger', typeFilter),
				TypeStr = '';
			if (types.length === 0) {
				label.text('Property type').removeClass('drop_down_trigger_filled');
			} else if (types.length === 1) {
				switch (types.val()) {
					case 'I':
						TypeStr = 'Industrial';
						break;
					case 'C':
						TypeStr = 'Commercial';
						break;
					case 'L':
						TypeStr = 'Land';
						break;
				}
				label.text(TypeStr).addClass('drop_down_trigger_filled');
			} else {
				label.html('Property type &middot '+types.length).addClass('drop_down_trigger_filled');
			}
		},
		
		
		// Filter - Property size
		filterSizeClear: function() {
			var sizeFilter = $('#property_size_filter_section');
			$('[name="min"]', sizeFilter).val(0);
			$('[name="max"]', sizeFilter).val('unlimited');
			goodman.control.range.update();
		},
		
		filterSizeReset: function() {
			var sizeFilter = $('#property_size_filter_section'),
				inputMin = $('[name="min"]', sizeFilter),
				inputMax = $('[name="max"]', sizeFilter),
				minVal = this.filterData.sizeMin,
				maxVal = this.filterData.sizeMax,
				testVal;
			for (test in goodman.config.sizeSteps) {
				testVal = goodman.config.sizeSteps[test];
				if ( testVal == minVal ) { inputMin.val(goodman.config.sizeSteps[test]); }
				if ( testVal == maxVal ) { inputMax.val(goodman.config.sizeSteps[test]); }
			}
			goodman.control.range.update(); 
		},
		
		filterSizeLabelUpdate: function() {
			var sizeFilter = $('#property_size_filter_section'),
				sizeMin = $('[name="min"]', sizeFilter).val(),
				sizeMax = $('[name="max"]', sizeFilter).val(),
				label = $('.property_filter_trigger', sizeFilter);
			if ( sizeMin == 0 && sizeMax == 'unlimited' ) {
				label.text('Property size').removeClass('drop_down_trigger_filled');
			} else {
				label.text(goodman.util.sizeFormat(sizeMin) + ' - ' + goodman.util.sizeFormat(sizeMax) + ' ' + goodman.config.sizeUnit).addClass('drop_down_trigger_filled');
			}
		},
		

		renderFilterSize: function() {
			var rangeContainer = $('.size-range-selector', this.filterContainer),	
				min = $('[name="min"]', rangeContainer).val(),
				max = $('[name="max"]', rangeContainer).val(),
				scaleChanged = false;
			
			// Change size?
			scaleChanged = ((this.filterData.view === 'lease' && goodman.config.sizeSteps !== goodman.config.sizeStepsLease) || 
							(this.filterData.view !== 'lease' && goodman.config.sizeSteps === goodman.config.sizeStepsLease) );
			goodman.config.sizeSteps = (this.filterData.view === 'lease')?goodman.config.sizeStepsLease:goodman.config.sizeStepsProperty;
			
			rangeContainer.html( goodman.template['property-list-filter-size-range'].hbs( goodman.config ) );
			goodman.control.range.init();
			if (scaleChanged) {
				$('[name="min"]', rangeContainer).val(0);
				$('[name="max"]', rangeContainer).val('unlimited');
			} else {
				$('[name="min"]', rangeContainer).val(min);
				$('[name="max"]', rangeContainer).val(max);
			}
			goodman.control.range.update();
			this.filterSizeLabelUpdate();
		},
		
		
		// Filter - Funds
		filterFundClear: function() {
			var fundFilter = $('#property_fund_filter_section');
			$('[type="checkbox"]', fundFilter).prop('checked', false);
			goodman.control.checkbox.update();
		},
		
		filterFundReset: function() {
			var fundFilter = $('#property_fund_filter_section'),
				checkboxes = $('[type="checkbox"]', fundFilter),
				fund;
			checkboxes.prop('checked', false); // Uncheck all
			if (this.filterData.fund !== undefined) {
				for (fund in this.filterData.fund) {
					checkboxes.filter('[value="'+this.filterData.fund[fund]+'"]').prop('checked', true);
				}
			} 
			goodman.control.checkbox.update();
		},
		
		filterFundLabelUpdate: function() {
			var fundFilter = $('#property_fund_filter_section'),
				funds = $('[type="checkbox"][name="fund"]:checked', fundFilter),
				label = $('.property_filter_trigger', fundFilter);
			if (funds.length === 0) {
				label.text('Fund').removeClass('drop_down_trigger_filled');
			} else if (funds.length === 1) {
				label.text(funds.val()).addClass('drop_down_trigger_filled');
			} else {
				label.html('Funds &middot '+funds.length).addClass('drop_down_trigger_filled');
			}
		},
		
		mobileSearchFixed: false,
		
		// scrollResults
		scrollResults: function(newtop) {
			var offset = $('.property_list_title_wrapper').outerHeight();
			
			// If result set is too short do not retract
			if ( !this.scrollResultsCanHappan() ) {
				$('.scroll-pane', this.container).toggleClass('slid-up', false);
				$('#property-list-search, #property-list-filter').css({top:0});
				this.suggestionClose();
				return;
			}
			
			if (newtop < -20) {
				$('.scroll-pane', this.container).toggleClass('slid-up', true);
				$('#property-list-search, #property-list-filter').css({top:-1*offset});
			} else {
				$('.scroll-pane', this.container).toggleClass('slid-up', false);
				$('#property-list-search, #property-list-filter').css({top:0});
			}
			/*
			if (goodman.config.isMobile) {
				offset = offset + 2;
				if ( newtop < offset ) {
					this.mobileSearchFixed = false;
					$('#property-list-search, #property-list-filter').css({top:-1*newtop});
				} else if (!this.mobileSearchFixed) {
					$('#property-list-search, #property-list-filter').css({top:-1*offset});
					this.mobileSearchFixed = true;
				}
			}
			*/
			this.suggestionClose();
		},
		
		scrollResultsCanHappan: function() {
			return $('#property-list-results', this.container).outerHeight() > $('.scroll-pane', this.container).height();
		},
		
		// Country abbriviations remap
		countryAbbrRemap: function( country ) {
			return ( goodman.config.countryAbbrRemap[country.toLowerCase()] )?goodman.config.countryAbbrRemap[country.toLowerCase()]:country;
		},
		
	});

})(jQuery);

/*================================================================================
	goodman.init

	Initialise the application
================================================================================*/

(function($){

	window.goodman.init = $.extend(true, window.goodman.init || {}, {

		init: function() {
			var self = this;
			goodman.util.log('Initialising');
			
			if (smartspaceConfig !== undefined) {
				window.goodman.config = $.extend(window.goodman.config, smartspaceConfig);
			}

			(goodman.util ? goodman.util.init() : goodman.util.log('Skipping goodman.util'));
			(goodman.handlebars ? goodman.handlebars.init() : goodman.util.log('Skipping goodman.handlebars'));

			(goodman.control.dropdown ? goodman.control.dropdown.init() : goodman.util.log('Skipping goodman.control.dropdown '));
			
			(goodman.interface ? goodman.interface.init() : goodman.util.log('Skipping goodman.interface'));
			(goodman.interface.landing ? goodman.interface.landing.init() : goodman.util.log('Skipping goodman.interface.landing'));
			(goodman.interface.propertysearch ? goodman.interface.propertysearch.init() : goodman.util.log('Skipping goodman.interface.propertysearch'));
			(goodman.interface.propertydetail ? goodman.interface.propertydetail.init() : goodman.util.log('Skipping goodman.interface.propertydetail'));
			(goodman.interface.map ? goodman.interface.map.init() : goodman.util.log('Skipping goodman.interface.map'));
			
			goodman.observer.subscribe('get-location', goodman.util.selfGeoLocate, self);
			//goodman.util.selfGeoLocate();
			
		}

	});

})(jQuery);

$( function(){ goodman.init.init(); } );
