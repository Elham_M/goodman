this["goodman"] = this["goodman"] || {};
this["goodman"]["template"] = this["goodman"]["template"] || {};
this["goodman"]["template"]["landing"] = this["goodman"]["template"]["landing"] || {};
this["goodman"]["template"]["landing"]["hbs"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "	<section class=\"header\">\r\n		<div class=\"smartspace-container container\">\r\n			<img class=\"smartspace-logo\" src=\"assets/images/smartspace_logo.svg\" alt=\"SmartSpace\">\r\n		</div>\r\n		<div class=\"narrow-container\">\r\n			<h1>Explore our properties</h1>\r\n			<div class=\"loading\">Loading<div class=\"spinner\"></div></div>\r\n			<div class=\"property_list_search_container\">\r\n				<form id=\"property_list_search_form\" action=\"#\">\r\n					<input id=\"property_list_search\" class=\"general_style_input\" type=\"text\" placeholder=\"City, address, property name\" autocomplete=\"off\">\r\n				</form>\r\n				<div class=\"property_list_search_autocomplete_wrapper\">\r\n					<div class=\"property_list_search_autocomplete_container\"></div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</section>	\r\n	\r\n	<section class=\"introduction\">\r\n		<div class=\"narrow-container\">\r\n			<h2>Strategic locations</h2>\r\n			<p>Goodman properties are a symbol of smart investment. By strategically locating modern, high quality properties in key gateway cities around the world, we’ve shortened the distance between business and consumers and put our customers ahead of the market.</p>\r\n		</div>\r\n	</section>	\r\n\r\n";
},"useData":true});
this["goodman"]["template"]["property-detail-lease"] = this["goodman"]["template"]["property-detail-lease"] || {};
this["goodman"]["template"]["property-detail-lease"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.leasingImage : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			<div class=\"gallery_zoom\"></div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "				<div class=\"property_detail_image_container gallery_image_container\" style=\"background-image: url('"
    + alias3((helpers.cleanurl || (depth0 && depth0.cleanurl) || alias2).call(alias1,(depth0 != null ? depth0.url : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "')\" title=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leasingthumbnailimage : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    return "			<div class=\"property_detail_image_container gallery_image_container\" style=\"background-image: url('"
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leasingthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "')\"></div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "";
},"9":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"property_detail_filter_item property_detail_area\">"
    + container.escapeExpression(((helper = (helper = helpers.displaySizeLease || (depth0 != null ? depth0.displaySizeLease : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"displaySizeLease","hash":{},"data":data}) : helper)))
    + "</div>";
},"11":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"property_detail_filter_item property_detail_fund\">Fund: "
    + container.escapeExpression(((helper = (helper = helpers.fund || (depth0 != null ? depth0.fund : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"fund","hash":{},"data":data}) : helper)))
    + "</div>";
},"13":function(container,depth0,helpers,partials,data) {
    return "				<a href=\""
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.micrositeurl : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "\" target=\"_blank\" class=\"property_detail_website\">Website</a>\r\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "				<div class=\"property_detail_more_spaces\" data-scrollto=\".property_detail_content_more_spaces\">More spaces available+</div>\r\n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "			<div class=\"property_detail_tool_bar\"><!--\r\n				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.brochure : stack1),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.videourl : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.plan : stack1),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.virtualtour : stack1),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.micrositeurl : depth0),{"name":"if","hash":{},"fn":container.program(26, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1["360view"] : stack1),{"name":"if","hash":{},"fn":container.program(28, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.webcam : stack1),{"name":"if","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.three_d_model : stack1),{"name":"if","hash":{},"fn":container.program(32, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.location_map : stack1),{"name":"if","hash":{},"fn":container.program(34, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n			--></div>\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.brochure : stack1),"brochure","Brochure","Brochures",true,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"20":function(container,depth0,helpers,partials,data) {
    return "				--><a href=\""
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.videourl : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "\" target=\"_blank\" class=\"property_detail_tool_bar_item property_detail_tool_bar_item_video\"><div class=\"property_detail_tool_bar_item_title\">Video</div></a><!--\r\n";
},"22":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.plan : stack1),"plans","Plan","Plans",true,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"24":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.virtualtour : stack1),"tour","Tour","Tours",false,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"26":function(container,depth0,helpers,partials,data) {
    return "				--><a href=\""
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.micrositeurl : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "\" target=\"_blank\" class=\"property_detail_tool_bar_item property_detail_tool_bar_item_website\"><div class=\"property_detail_tool_bar_item_title\">Website</div></a><!--\r\n";
},"28":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1["360view"] : stack1),"360","360","360",false,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"30":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.webcam : stack1),"webcam","Webcam","Webcams",false,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"32":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.three_d_model : stack1),"3d","3D model","3D models",true,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"34":function(container,depth0,helpers,partials,data) {
    var stack1;

  return " -->"
    + container.escapeExpression((helpers.goodmanresources || (depth0 && depth0.goodmanresources) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.location_map : stack1),"location","Location","Locations",false,{"name":"goodmanresources","hash":{},"data":data}))
    + "<!-- ";
},"36":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		<div class=\"property_detail_content_body_container property_detail_for_lease_content_body_container\">\r\n			<h3>Spaces for lease</h3>\r\n			<div class=\"property_detail_for_lease_space_list\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.site : depth0),{"name":"each","hash":{},"fn":container.program(37, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\r\n		</div>\r\n";
},"37":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "				<div class=\"property_detail_for_lease_space_item_container\"><!--\r\n				--><div class=\"property_detail_for_lease_space_field_name\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div><!--\r\n				--><div class=\"property_detail_for_lease_space_field_area\">"
    + alias4((helpers.goodmansize || (depth0 && depth0.goodmansize) || alias2).call(alias1,(depth0 != null ? depth0.size : depth0),{"name":"goodmansize","hash":{},"data":data}))
    + "</div><!--\r\n				--><div class=\"property_detail_for_lease_space_field_type\">"
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "</div><!--\r\n				--><div class=\"property_detail_for_lease_space_field_available\">"
    + ((stack1 = (helpers.if_future || (depth0 && depth0.if_future) || alias2).call(alias1,(depth0 != null ? depth0.availabledate : depth0),{"name":"if_future","hash":{},"fn":container.program(38, data, 0),"inverse":container.program(40, data, 0),"data":data})) != null ? stack1 : "")
    + "</div></div>\r\n";
},"38":function(container,depth0,helpers,partials,data) {
    return "Available "
    + container.escapeExpression((helpers.monthYearFormat || (depth0 && depth0.monthYearFormat) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.availabledate : depth0),false,{"name":"monthYearFormat","hash":{},"data":data}));
},"40":function(container,depth0,helpers,partials,data) {
    return "Available now";
},"42":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		<div class=\"property_detail_content_footer_container\">\r\n			<h3>Site features</h3>\r\n			<div class=\"property_detail_feature_wrapper\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.sitefeature : depth0),{"name":"each","hash":{},"fn":container.program(43, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			</div>\r\n		</div>\r\n";
},"43":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "				<div class=\"property_detail_feature_container "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\r\n";
},"45":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		<div class=\"property_detail_content_more_spaces\">\r\n			<div class=\"property_list_section_separator\"></div>\r\n			<h3>More spaces available</h3>\r\n			<div class=\"property_list_section_content_container\">\r\n				"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.childproperties : depth0),{"name":"each","hash":{},"fn":container.program(46, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				<div class=\"clear\"></div>\r\n			</div>\r\n		</div>\r\n";
},"46":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["with"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.attributes : depth0),{"name":"with","hash":{},"fn":container.program(47, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"47":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "\r\n				<div class=\"property_list_item_container "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.isleased : depth0),"0",{"name":"if_eq","hash":{},"fn":container.program(48, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-object-id=\""
    + alias4(((helper = (helper = helpers.objectid || (depth0 != null ? depth0.objectid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"objectid","hash":{},"data":data}) : helper)))
    + "\">\r\n					<div class=\"property_list_item_gallery_container gallery_container\">\r\n						<div class=\"property_list_item_image_container gallery_image_container\" \r\n							style=\"background-image:url('"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"if","hash":{},"fn":container.program(50, data, 0),"inverse":container.program(52, data, 0),"data":data})) != null ? stack1 : "")
    + "')\"\r\n							title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n					</div>\r\n					<div class=\"property_list_item_description_container\">\r\n						<p class=\"property_list_item_type high_light\">"
    + alias4((helpers.goodmantypes || (depth0 && depth0.goodmantypes) || alias2).call(alias1,(depth0 != null ? depth0.propertytype : depth0),{"name":"goodmantypes","hash":{},"data":data}))
    + "</p>\r\n						<h3 class=\"property_list_item_title\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h3>\r\n						<p class=\"property_list_item_address\">"
    + alias4((helpers.goodmanaddress || (depth0 && depth0.goodmanaddress) || alias2).call(alias1,(depth0 != null ? depth0.address : depth0),(depth0 != null ? depth0.suburb : depth0),(depth0 != null ? depth0.region : depth0),(depth0 != null ? depth0.country : depth0),true,{"name":"goodmanaddress","hash":{},"data":data}))
    + " </p>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.size_standardunits : depth0),{"name":"if","hash":{},"fn":container.program(54, data, 0),"inverse":container.program(56, data, 0),"data":data})) != null ? stack1 : "")
    + "					</div>\r\n				</div>\r\n				";
},"48":function(container,depth0,helpers,partials,data) {
    return "property_list_lease_item_container";
},"50":function(container,depth0,helpers,partials,data) {
    return container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}));
},"52":function(container,depth0,helpers,partials,data) {
    return container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leasingthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}));
},"54":function(container,depth0,helpers,partials,data) {
    return "							<p class=\"property_list_item_area\">"
    + container.escapeExpression((helpers.goodmansize || (depth0 && depth0.goodmansize) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.size_standardunits : depth0),{"name":"goodmansize","hash":{},"data":data}))
    + "</p>\r\n";
},"56":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "							<p class=\"property_list_item_area\">"
    + alias3((helpers.goodmansize || (depth0 && depth0.goodmansize) || alias2).call(alias1,(depth0 != null ? depth0.minleasingsize : depth0),false,{"name":"goodmansize","hash":{},"data":data}))
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.maxleasingsize : depth0),{"name":"if","hash":{},"fn":container.program(57, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " "
    + alias3(((helper = (helper = helpers.goodmansizeunit || (depth0 != null ? depth0.goodmansizeunit : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"goodmansizeunit","hash":{},"data":data}) : helper)))
    + "</p>\r\n";
},"57":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_not_eq || (depth0 && depth0.if_not_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.minleasingsize : depth0),(depth0 != null ? depth0.maxleasingsize : depth0),{"name":"if_not_eq","hash":{},"fn":container.program(58, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"58":function(container,depth0,helpers,partials,data) {
    return " - "
    + container.escapeExpression((helpers.goodmansize || (depth0 && depth0.goodmansize) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.maxleasingsize : depth0),false,{"name":"goodmansize","hash":{},"data":data}));
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"property_detail_for_lease_container\">\r\n	<div class=\"property_detail_close js-property-detail-close\"></div>\r\n\r\n	<div class=\"property_detail_gallery_container gallery_container\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.leasingImage : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "	</div>\r\n\r\n	<div class=\"property_detail_title_container\"><h1>"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h1></div>\r\n\r\n	<div class=\"page_content_selector_wrapper\">\r\n		<div class=\"page_content_selector_container\">\r\n			<div class=\"property_detail_trigger page_content_selector\">Details</div>\r\n			<div class=\"property_map_trigger page_content_selector\">Map</div>\r\n		</div>\r\n	</div>\r\n\r\n	<div class=\"property_detail_content_container\">\r\n		<div class=\"property_detail_content_header_container\">\r\n			<div class=\"property_detail_address\">"
    + alias4((helpers.goodmanaddress || (depth0 && depth0.goodmanaddress) || alias2).call(alias1,(depth0 != null ? depth0.address : depth0),(depth0 != null ? depth0.suburb : depth0),(depth0 != null ? depth0.region : depth0),(depth0 != null ? depth0.country : depth0),{"name":"goodmanaddress","hash":{},"data":data}))
    + " </div>\r\n			<div class=\"property_detail_filter_container\">\r\n				<div class=\"property_detail_filter_item property_detail_type\">"
    + alias4((helpers.goodmantypes || (depth0 && depth0.goodmantypes) || alias2).call(alias1,(depth0 != null ? depth0.propertytype : depth0),{"name":"goodmantypes","hash":{},"data":data}))
    + "</div>\r\n				"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.displaySizeLease : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.fund : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n			</div>\r\n			<div class=\"property_detail_link_container\">\r\n				<div class=\"clear\"></div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.micrositeurl : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				<!-- <a href=\"mailto:?subject=Take a look at this Goodman property`&body="
    + alias4(((helper = (helper = helpers.shareBaseUrl || (depth0 != null ? depth0.shareBaseUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"shareBaseUrl","hash":{},"data":data}) : helper)))
    + "?pid="
    + alias4(((helper = (helper = helpers.objectid || (depth0 != null ? depth0.objectid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"objectid","hash":{},"data":data}) : helper)))
    + "\" class=\"property_detail_share\">Share</a> -->\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.childproperties : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				<div class=\"clear\"></div>\r\n			</div>\r\n		</div>\r\n\r\n		<div class=\"property_detail_content_body_container\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.showToolBar : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n			"
    + ((stack1 = ((helper = (helper = helpers.leasingdescription || (depth0 != null ? depth0.leasingdescription : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"leasingdescription","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n\r\n		</div>\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.site : depth0),{"name":"if","hash":{},"fn":container.program(36, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.sitefeature : depth0),{"name":"if","hash":{},"fn":container.program(42, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.childproperties : depth0),{"name":"if","hash":{},"fn":container.program(45, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</div>\r\n\r\n</div>";
},"useData":true});
this["goodman"]["template"]["property-detail-popup-gallery"] = this["goodman"]["template"]["property-detail-popup-gallery"] || {};
this["goodman"]["template"]["property-detail-popup-gallery"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.type : depth0),"lease",{"name":"if_eq","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.resource : stack1)) != null ? stack1.leasingImage : stack1),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "					<div class=\"gallery_image_container\"><img src=\""
    + alias3((helpers.cleanurl || (depth0 && depth0.cleanurl) || alias2).call(alias1,(depth0 != null ? depth0.url : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "\" alt=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "\">"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.title : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<p class=\"caption\">"
    + container.escapeExpression(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data}) : helper)))
    + "</p>";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.resource : stack1)) != null ? stack1.SmarSpaceImage : stack1),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"8":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.type : depth0),"lease",{"name":"if_eq","hash":{},"fn":container.program(9, data, 0),"inverse":container.program(11, data, 0),"data":data})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "				<img src=\""
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.resource : stack1)) != null ? stack1.leasingImage : stack1)) != null ? stack1["0"] : stack1)) != null ? stack1.url : stack1),{"name":"cleanurl","hash":{},"data":data}))
    + "\">\n";
},"11":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "				<img src=\""
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = ((stack1 = ((stack1 = ((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.resource : stack1)) != null ? stack1.SmarSpaceImage : stack1)) != null ? stack1["0"] : stack1)) != null ? stack1.url : stack1),{"name":"cleanurl","hash":{},"data":data}))
    + "\">\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div id=\"product_detail_gallery_overlay_popup_wrapper\" class=\"overlay_popup_wrapper\">\n	<div class=\"overlay_popup_container\">\n		<div class=\"gallery_container\">\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.resource : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.attributes : depth0)) != null ? stack1.resource : stack1),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\n		<div class=\"overlay_popup_close\"></div>\n	</div>\n</div>";
},"useData":true});
this["goodman"]["template"]["property-detail-popup-iframe"] = this["goodman"]["template"]["property-detail-popup-iframe"] || {};
this["goodman"]["template"]["property-detail-popup-iframe"]["hbs"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div id=\"product_detail_gallery_overlay_popup_wrapper\" class=\"overlay_popup_wrapper\">\n	<div class=\"overlay_popup_container\">\n		<div class=\"gallery_container\">\n			<div class=\"url-container\">\n				<input id=\"urlToCopy\" type=\"text\" value=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\"><a href=\"#copyInput\">Copy to clipboard</a><a href=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\" target=\"_blank\">Open in new window</a>\n			</div>\n			<iframe src=\""
    + alias4(((helper = (helper = helpers.url || (depth0 != null ? depth0.url : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"url","hash":{},"data":data}) : helper)))
    + "\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe>\n		</div>\n		<div class=\"overlay_popup_close\"></div>\n	</div>\n</div>";
},"useData":true});
this["goodman"]["template"]["property-detail"] = this["goodman"]["template"]["property-detail"] || {};
this["goodman"]["template"]["property-detail"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.SmarSpaceImage : stack1),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		<div class=\"gallery_zoom\"></div>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "			<div class=\"property_detail_image_container gallery_image_container\" style=\"background-image: url('"
    + alias3((helpers.cleanurl || (depth0 && depth0.cleanurl) || alias2).call(alias1,(depth0 != null ? depth0.url : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "')\" title=\""
    + alias3(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    return "		<div class=\"property_detail_image_container gallery_image_container\" style=\"background-image: url('"
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "')\"></div>\r\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "		<div class=\"property_detail_image_container gallery_image_container\" style=\"background-image: url('"
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leasingthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "')\"></div>\r\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.fund : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"10":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"property_detail_filter_item property_detail_fund\">Fund: "
    + container.escapeExpression(((helper = (helper = helpers.fund || (depth0 != null ? depth0.fund : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"fund","hash":{},"data":data}) : helper)))
    + "</div>";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.childproperties : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.program(15, data, 0),"data":data})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    return "					<div class=\"property_detail_view_lease general_style_button general_style_button_green\" data-propertyview=\"child\">View space for lease+</div>\r\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.isleased : depth0),"0",{"name":"if_eq","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"16":function(container,depth0,helpers,partials,data) {
    return "						<div class=\"property_detail_view_lease general_style_button general_style_button_green\" data-propertyview=\"lease\">View space for lease+</div>\r\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.isleased : depth0),"0",{"name":"if_eq","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "				<a target=\"_blank\" href=\"mailto:?subject=Take a look at this Goodman property&body="
    + alias4(((helper = (helper = helpers.shareBaseUrl || (depth0 != null ? depth0.shareBaseUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"shareBaseUrl","hash":{},"data":data}) : helper)))
    + "?pid="
    + alias4(((helper = (helper = helpers.propertyid || (depth0 != null ? depth0.propertyid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"propertyid","hash":{},"data":data}) : helper)))
    + "/"
    + alias4((helpers.goodmannameurlsafe || (depth0 && depth0.goodmannameurlsafe) || alias2).call(alias1,(depth0 != null ? depth0.name : depth0),{"name":"goodmannameurlsafe","hash":{},"data":data}))
    + "\" class=\"property_detail_share\">Share</a>\r\n\r\n";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leaselink : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"20":function(container,depth0,helpers,partials,data) {
    var helper;

  return "					<a href=\""
    + container.escapeExpression(((helper = (helper = helpers.leaselink || (depth0 != null ? depth0.leaselink : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"leaselink","hash":{},"data":data}) : helper)))
    + "\" class=\"property_detail_view_lease general_style_button general_style_button_green\" target=\"_blank\">View space for lease+</a>\r\n";
},"22":function(container,depth0,helpers,partials,data) {
    return "			<a href=\""
    + container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.micrositeurl : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + "\" target=\"_blank\" class=\"property_detail_website\">Website</a>\r\n";
},"24":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "	<div class=\"property_detail_content_footer_container\">\r\n		<h3>Site features</h3>\r\n		<div class=\"property_detail_feature_wrapper\">\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.sitefeature : depth0),{"name":"each","hash":{},"fn":container.program(25, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		</div>\r\n	</div>\r\n";
},"25":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			<div class=\"property_detail_feature_container "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\r\n";
},"27":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.childproperties : depth0),{"name":"if","hash":{},"fn":container.program(28, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"28":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		<div class=\"property_detail_content_more_spaces\">\r\n			<div class=\"property_list_section_separator\"></div>\r\n			<h3>Spaces available</h3>\r\n			<div class=\"property_list_section_content_container\">\r\n				"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.childproperties : depth0),{"name":"each","hash":{},"fn":container.program(29, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n				<div class=\"clear\"></div>\r\n			</div>\r\n		</div>\r\n";
},"29":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["with"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.attributes : depth0),{"name":"with","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"30":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "\r\n				<div class=\"property_list_item_container "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.isleased : depth0),"0",{"name":"if_eq","hash":{},"fn":container.program(31, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-object-id=\""
    + alias4(((helper = (helper = helpers.objectid || (depth0 != null ? depth0.objectid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"objectid","hash":{},"data":data}) : helper)))
    + "\">\r\n					<div class=\"property_list_item_gallery_container gallery_container\">\r\n						<div class=\"property_list_item_image_container gallery_image_container\" \r\n							style=\"background-image:url('"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"if","hash":{},"fn":container.program(33, data, 0),"inverse":container.program(35, data, 0),"data":data})) != null ? stack1 : "")
    + "')\"\r\n							title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"></div>\r\n					</div>\r\n					<div class=\"property_list_item_description_container\">\r\n						<p class=\"property_list_item_type high_light\">"
    + alias4((helpers.goodmantypes || (depth0 && depth0.goodmantypes) || alias2).call(alias1,(depth0 != null ? depth0.propertytype : depth0),{"name":"goodmantypes","hash":{},"data":data}))
    + "</p>\r\n						<h3 class=\"property_list_item_title\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h3>\r\n						<p class=\"property_list_item_address\">"
    + alias4(((helper = (helper = helpers.address || (depth0 != null ? depth0.address : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"address","hash":{},"data":data}) : helper)))
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.suburb : depth0),{"name":"if","hash":{},"fn":container.program(37, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.region : depth0),{"name":"if","hash":{},"fn":container.program(39, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</p>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.size_standardunits : depth0),{"name":"if","hash":{},"fn":container.program(41, data, 0),"inverse":container.program(43, data, 0),"data":data})) != null ? stack1 : "")
    + "					</div>\r\n				</div>\r\n				";
},"31":function(container,depth0,helpers,partials,data) {
    return "property_list_lease_item_container";
},"33":function(container,depth0,helpers,partials,data) {
    return container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}));
},"35":function(container,depth0,helpers,partials,data) {
    return container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leasingthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}));
},"37":function(container,depth0,helpers,partials,data) {
    var helper;

  return " "
    + container.escapeExpression(((helper = (helper = helpers.suburb || (depth0 != null ? depth0.suburb : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"suburb","hash":{},"data":data}) : helper)));
},"39":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<br>"
    + container.escapeExpression(((helper = (helper = helpers.region || (depth0 != null ? depth0.region : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"region","hash":{},"data":data}) : helper)));
},"41":function(container,depth0,helpers,partials,data) {
    return "							<p class=\"property_list_item_area\">"
    + container.escapeExpression((helpers.goodmansize || (depth0 && depth0.goodmansize) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.size_standardunits : depth0),{"name":"goodmansize","hash":{},"data":data}))
    + "</p>\r\n";
},"43":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "							<p class=\"property_list_item_area\">"
    + alias3((helpers.goodmansize || (depth0 && depth0.goodmansize) || alias2).call(alias1,(depth0 != null ? depth0.minleasingsize : depth0),false,{"name":"goodmansize","hash":{},"data":data}))
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.maxleasingsize : depth0),{"name":"if","hash":{},"fn":container.program(44, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + " "
    + alias3(((helper = (helper = helpers.goodmansizeunit || (depth0 != null ? depth0.goodmansizeunit : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"goodmansizeunit","hash":{},"data":data}) : helper)))
    + "</p>\r\n";
},"44":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_not_eq || (depth0 && depth0.if_not_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.minleasingsize : depth0),(depth0 != null ? depth0.maxleasingsize : depth0),{"name":"if_not_eq","hash":{},"fn":container.program(45, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"45":function(container,depth0,helpers,partials,data) {
    return " - "
    + container.escapeExpression((helpers.goodmansize || (depth0 && depth0.goodmansize) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.maxleasingsize : depth0),false,{"name":"goodmansize","hash":{},"data":data}));
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"property_detail_close js-property-detail-close\"></div>\r\n\r\n<div class=\"property_detail_gallery_container gallery_container\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.resource : depth0)) != null ? stack1.SmarSpaceImage : stack1),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</div>\r\n\r\n<div class=\"property_detail_title_container\"><h1>"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h1></div>\r\n\r\n<div class=\"page_content_selector_wrapper\">\r\n	<div class=\"page_content_selector_container\">\r\n		<div class=\"property_detail_trigger page_content_selector\">Details</div>\r\n		<div class=\"property_map_trigger page_content_selector\">Map</div>\r\n	</div>\r\n</div>\r\n\r\n<div class=\"property_detail_content_container\">\r\n	<div class=\"property_detail_content_header_container\">\r\n		<div class=\"property_detail_address\">"
    + alias4((helpers.goodmanaddress || (depth0 && depth0.goodmanaddress) || alias2).call(alias1,(depth0 != null ? depth0.address : depth0),(depth0 != null ? depth0.suburb : depth0),(depth0 != null ? depth0.region : depth0),(depth0 != null ? depth0.country : depth0),{"name":"goodmanaddress","hash":{},"data":data}))
    + " </div>\r\n		<div class=\"property_detail_filter_container\">\r\n			<div class=\"property_detail_filter_item property_detail_type\">"
    + alias4((helpers.goodmantypes || (depth0 && depth0.goodmantypes) || alias2).call(alias1,(depth0 != null ? depth0.propertytype : depth0),{"name":"goodmantypes","hash":{},"data":data}))
    + "</div>\r\n			<div class=\"property_detail_filter_item property_detail_area\">"
    + alias4((helpers.goodmansize || (depth0 && depth0.goodmansize) || alias2).call(alias1,(depth0 != null ? depth0.size_standardunits : depth0),{"name":"goodmansize","hash":{},"data":data}))
    + "</div>\r\n			"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isInternal : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n		</div>\r\n		<div class=\"property_detail_link_container\">\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isInternal : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(18, data, 0),"data":data})) != null ? stack1 : "")
    + "			<div class=\"clear\"></div>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.micrositeurl : depth0),{"name":"if","hash":{},"fn":container.program(22, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			<div class=\"clear\"></div>\r\n		</div>\r\n	</div>\r\n\r\n	<div class=\"property_detail_content_body_container margin-top\">\r\n		"
    + ((stack1 = ((helper = (helper = helpers.gppdescription || (depth0 != null ? depth0.gppdescription : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gppdescription","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\r\n	</div>\r\n\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.sitefeature : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isInternal : depth0),{"name":"if","hash":{},"fn":container.program(27, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\r\n</div>\r\n";
},"useData":true});
this["goodman"]["template"]["property-list-filter-size-range"] = this["goodman"]["template"]["property-list-filter-size-range"] || {};
this["goodman"]["template"]["property-list-filter-size-range"]["hbs"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"property_filter_size_display\"><div class=\"property_filter_size_display_start\">0</div> - <div class=\"property_filter_size_display_end\">unlimited</div> "
    + container.escapeExpression(((helper = (helper = helpers.sizeUnit || (depth0 != null ? depth0.sizeUnit : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"sizeUnit","hash":{},"data":data}) : helper)))
    + "</div>\n<input type=\"hidden\" name=\"min\" value=\"0\">\n<input type=\"hidden\" name=\"max\" value=\"unlimited\">\n<div class=\"property_filter_size_bar_container\">\n	<div class=\"property_filter_size_bar_column_container property_filter_size_bar_column_container_5_column\"><div class=\"property_filter_size_bar_column\"></div><div class=\"property_filter_size_bar_column\"></div><div class=\"property_filter_size_bar_column\"></div><div class=\"property_filter_size_bar_column\"></div><div class=\"property_filter_size_bar_column\"></div></div>\n	<div class=\"property_filter_size_bar_front\"><div class=\"property_filter_size_bar_front_start\"></div><div class=\"property_filter_size_bar_front_end\"></div></div>\n</div>";
},"useData":true});
this["goodman"]["template"]["property-list-filter"] = this["goodman"]["template"]["property-list-filter"] || {};
this["goodman"]["template"]["property-list-filter"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    return "checked";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "		--><div id=\"property_fund_filter_section\" class=\"property_filter_section drop_down_parent\">\n			<div class=\"property_filter_trigger drop_down_trigger general_style_button\">Fund</div>\n			<div class=\"property_filter_section_title\">Fund</div>\n			<div class=\"drop_down_wrapper\">\n				<div class=\"property_filter_content drop_down_container\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.funds : depth0),{"name":"each","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "					<div class=\"property_filter_button_container\">\n						<div class=\"property_filter_button property_filter_button_cancel\">Cancel</div>\n						<div class=\"property_filter_button property_filter_button_apply\">Apply</div>\n					</div>\n				</div>\n			</div>\n		</div><!--\n";
},"4":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "						<div class=\"property_filter_section_row general_style_checkbox_container\"><input class=\"property_filter_section_row_checkbox general_style_checkbox_input\" type=\"checkbox\" name=\"fund\" value=\""
    + alias2(alias1(depth0, depth0))
    + "\"><div class=\"general_style_checkbox_display\"></div><label class=\"general_style_label general_style_label_right\">"
    + alias2(alias1(depth0, depth0))
    + "</label></div>	\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "		<div id=\"property_for_lease_filter_section\" class=\"property_filter_section\">\n			<div class=\"property_filter_section_title\">Lease<span class=\"green\" style=\"font-weight:400;\">+</span></div>\n			<div class=\"property_filter_section_row general_style_checkbox_container\"><input name=\"forleasefilter\" id=\"property_list_filter_show_lease\" class=\"property_list_filter_checkbox general_style_checkbox_input\" type=\"checkbox\"><div class=\"general_style_checkbox_display\"></div><label for=\"property_list_filter_show_lease\" class=\"general_style_label general_style_label_right\">Show space for lease</label></div>\n		</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing;

  return "<div class=\"property_filter_wrapper\">\n	<div class=\"property_filter_title_container\">\n		<div class=\"container\">\n			<div class=\"property_filter_title\">Filters</div>\n			<div class=\"property_filter_title_button_reset property_filter_button_reset\">Reset</div>\n			<div class=\"property_filter_title_button_cancel property_filter_button_cancel\"></div>\n		</div>\n	</div>\n	\n	\n	<div class=\"property_filter_container container\">\n		<!--\n		--><div id=\"property_type_filter_section\" class=\"property_filter_section drop_down_parent\">\n			<div class=\"property_filter_trigger drop_down_trigger general_style_button\">Property type</div>\n			<div class=\"property_filter_section_title\">Property type</div>\n			<div class=\"drop_down_wrapper\">\n				<div class=\"property_filter_content drop_down_container\">\n					<div class=\"property_filter_section_row general_style_checkbox_container\"><input class=\"property_filter_section_row_checkbox general_style_checkbox_input\" type=\"checkbox\" name=\"type\" value=\"I\"><div class=\"general_style_checkbox_display\"></div><label class=\"general_style_label general_style_label_right\">Industrial or logistics<span class=\"property_filter_checkbox_sub_label\">Manufacturing and storage</span></label></div>\n					<div class=\"property_filter_section_row general_style_checkbox_container\"><input class=\"property_filter_section_row_checkbox general_style_checkbox_input\" type=\"checkbox\" name=\"type\" value=\"C\"><div class=\"general_style_checkbox_display\"></div><label class=\"general_style_label general_style_label_right\">Commercial or office<span class=\"property_filter_checkbox_sub_label\">Office and business spaces</span></label></div>\n					<div class=\"property_filter_section_row general_style_checkbox_container\"><input class=\"property_filter_section_row_checkbox general_style_checkbox_input\" type=\"checkbox\" name=\"type\" value=\"L\"><div class=\"general_style_checkbox_display\"></div><label class=\"general_style_label general_style_label_right\">Land for development<span class=\"property_filter_checkbox_sub_label\">Vacant lots to be developed</span></label></div>\n					<div class=\"property_filter_button_container\">\n						<div class=\"property_filter_button property_filter_button_cancel\">Cancel</div>\n						<div class=\"property_filter_button property_filter_button_apply\">Apply</div>\n					</div>\n				</div>\n			</div>\n		</div><!--\n		--><div id=\"property_size_filter_section\" class=\"property_filter_section drop_down_parent\">\n			<div class=\"property_filter_trigger drop_down_trigger general_style_button\">Property size</div>\n			<div class=\"property_filter_section_title\">Property size</div>\n			<div class=\"drop_down_wrapper\">\n				<div class=\"property_filter_content drop_down_container\">\n					<div class=\"size-range-selector\">\n						"
    + container.escapeExpression((helpers.partial || (depth0 && depth0.partial) || alias2).call(alias1,"property-list-filter-size-range",{"name":"partial","hash":{},"data":data}))
    + "\n					</div>\n					<div class=\"size-units-selector\">\n						Show sizes in:\n						<div class=\"general_style_radio_container\">\n							<input class=\"general_style_radio_input\" type=\"radio\" name=\"size_unit\" value=\"sqm\" "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.sizeUnit : depth0),"sqm",{"name":"if_eq","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n							<div class=\"general_style_radio_display\"></div><label class=\"general_style_label\">sqm</label>\n						</div>\n						<div class=\"general_style_radio_container\">\n							<input class=\"general_style_radio_input\" type=\"radio\" name=\"size_unit\" value=\"sqft\" "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.sizeUnit : depth0),"sqft",{"name":"if_eq","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ">\n							<div class=\"general_style_radio_display\"></div><label class=\"general_style_label\">sqft</label>\n						</div>\n					</div>\n					<div class=\"property_filter_button_container\">\n						<div class=\"property_filter_button property_filter_button_cancel\">Cancel</div>\n						<div class=\"property_filter_button property_filter_button_apply\">Apply</div>\n					</div>\n				</div>\n			</div>\n		</div><!--\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isInternal : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		--><div class=\"property_filter_inline_reset property_filter_button_reset\"></div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isInternal : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "		<div class=\"clear\"></div>\n		<div class=\"property_filter_bottom_button_container\"><div class=\"property_filter_bottom_button property_filter_bottom_button_cancel general_style_button property_filter_button_cancel\">Cancel</div><div class=\"property_filter_bottom_button property_filter_bottom_button_apply general_style_button general_style_button_green property_filter_button_apply\">Apply</div></div>\n	</div>\n	\n</div>\n";
},"useData":true});
this["goodman"]["template"]["property-list-results"] = this["goodman"]["template"]["property-list-results"] || {};
this["goodman"]["template"]["property-list-results"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "\n		<div class=\"property_list_section_container\">\n			<div class=\"property_list_section_title_container\">\n				<h2>"
    + alias4(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"key","hash":{},"data":data}) : helper)))
    + "</h2>\n				"
    + ((stack1 = (helpers.if_bigger || (depth0 && depth0.if_bigger) || alias2).call(alias1,(depths[1] != null ? depths[1].countryCount : depths[1]),1,{"name":"if_bigger","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n				<div class=\"property_list_item_count\">"
    + alias4(((helper = (helper = helpers.length || (depth0 != null ? depth0.length : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"length","hash":{},"data":data}) : helper)))
    + " propert"
    + ((stack1 = (helpers.if_bigger || (depth0 && depth0.if_bigger) || alias2).call(alias1,(depth0 != null ? depth0.length : depth0),1,{"name":"if_bigger","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.program(7, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</div>\n			</div>\n			<div class=\"property_list_section_content_container\">\n\n"
    + ((stack1 = helpers.each.call(alias1,depth0,{"name":"each","hash":{},"fn":container.program(9, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n				<div class=\"property_list_section_separator clear\"></div>\n			</div>\n		</div>\n		\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_bigger || (depth0 && depth0.if_bigger) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.length : depth0),3,{"name":"if_bigger","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"property_list_section_navigation_link_all high_light\"><a href=\"#searchCountry\" data-country=\""
    + container.escapeExpression(((helper = (helper = helpers.key || (data && data.key)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"key","hash":{},"data":data}) : helper)))
    + "\">See all+</a></div>";
},"5":function(container,depth0,helpers,partials,data) {
    return "ies";
},"7":function(container,depth0,helpers,partials,data) {
    return "y";
},"9":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depths[2] != null ? depths[2].countryCount : depths[2]),1,{"name":"if_eq","hash":{},"fn":container.program(10, data, 0, blockParams, depths),"inverse":container.program(12, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"10":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.listing_item,depth0,{"name":"listing_item","data":data,"indent":"\t\t\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"12":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = (helpers.if_bigger || (depth0 && depth0.if_bigger) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),3,(data && data.index),{"name":"if_bigger","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"13":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.listing_item,depth0,{"name":"listing_item","data":data,"indent":"\t\t\t\t\t\t\t","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"15":function(container,depth0,helpers,partials,data) {
    return "  		<div class=\"property_list_section_container property_list_section_container_no_results\">\n			<div class=\"property_list_section_title_container\">\n				<h2>No results found</h2>\n				<p>No results in the category you selected were found in this map area.</p>\n				<p>Please try zooming out on the map<span class=\"hide-on-no-filters-set\"> or choosing another category</span>.</p>\n				<div class=\"general_style_button property_filter_button_zoom_out\">Zoom out</div>\n				<div class=\"general_style_button property_filter_button_reset\">Clear filters</div>\n			</div>\n		</div> \n";
},"17":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"property_list_item_container "
    + ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || alias2).call(alias1,(depth0 != null ? depth0.isleased : depth0),"0",{"name":"if_eq","hash":{},"fn":container.program(18, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\" data-object-id=\""
    + alias4(((helper = (helper = helpers.objectid || (depth0 != null ? depth0.objectid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"objectid","hash":{},"data":data}) : helper)))
    + "\">\n	<div class=\"property_list_item_gallery_container gallery_container\">\n		<div class=\"property_list_item_image_container gallery_image_container\" \n			style=\"background-image:url('"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.program(22, data, 0),"data":data})) != null ? stack1 : "")
    + "')\"\n			title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\"></div>\n	</div>\n	<div class=\"property_list_item_description_container\">\n		<p class=\"property_list_item_type high_light\">"
    + alias4((helpers.goodmantypes || (depth0 && depth0.goodmantypes) || alias2).call(alias1,(depth0 != null ? depth0.propertytype : depth0),{"name":"goodmantypes","hash":{},"data":data}))
    + "</p>\n		<h3 class=\"property_list_item_title\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h3>\n		<p class=\"property_list_item_address\">"
    + alias4((helpers.goodmanaddress || (depth0 && depth0.goodmanaddress) || alias2).call(alias1,(depth0 != null ? depth0.address : depth0),(depth0 != null ? depth0.suburb : depth0),(depth0 != null ? depth0.region : depth0),(depth0 != null ? depth0.country : depth0),true,{"name":"goodmanaddress","hash":{},"data":data}))
    + " </p>\n		"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.size_standardunits : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n	</div>\n</div>\n";
},"18":function(container,depth0,helpers,partials,data) {
    return "property_list_lease_item_container";
},"20":function(container,depth0,helpers,partials,data) {
    return container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.gppthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}));
},"22":function(container,depth0,helpers,partials,data) {
    return container.escapeExpression((helpers.cleanurl || (depth0 && depth0.cleanurl) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.leasingthumbnailimage : depth0),{"name":"cleanurl","hash":{},"data":data}));
},"24":function(container,depth0,helpers,partials,data) {
    return "<p class=\"property_list_item_area\">"
    + container.escapeExpression((helpers.goodmansize || (depth0 && depth0.goodmansize) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.size_standardunits : depth0),{"name":"goodmansize","hash":{},"data":data}))
    + "</p>";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return " <div class=\"property_list_section_wrapper\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.countries : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(15, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</div>\n\n\n";
},"main_d":  function(fn, props, container, depth0, data, blockParams, depths) {

  var decorators = container.decorators;

  fn = decorators.inline(fn,props,container,{"name":"inline","hash":{},"fn":container.program(17, data, 0, blockParams, depths),"inverse":container.noop,"args":["listing_item"],"data":data}) || fn;
  return fn;
  }

,"useDecorators":true,"usePartial":true,"useData":true,"useDepths":true});
this["goodman"]["template"]["property-list-search"] = this["goodman"]["template"]["property-list-search"] || {};
this["goodman"]["template"]["property-list-search"]["hbs"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"property_list_title_wrapper\">\n	<div class=\"property_list_title_container\">\n		<img class=\"smartspace-logo\" src=\"assets/images/smartspace_logo.svg\" alt=\"SmartSpace\">\n	</div>\n</div>\n\n<div class=\"property_list_search_container loading\">\n	<form id=\"property_list_search_form\" action=\"#\">\n		<input id=\"property_list_search\" class=\"general_style_input\" type=\"text\" placeholder=\"Loading...\" autocomplete=\"off\" disabled>\n	</form>\n	<div class=\"property_list_search_clear\"></div>\n	<div class=\"property_list_search_autocomplete_wrapper\">\n		<div class=\"property_list_search_autocomplete_container\"></div>\n	</div>\n</div>\n\n<div class=\"page_content_selector_wrapper\">\n	<div class=\"page_content_selector_container\">\n		<div class=\"property_list_trigger page_content_selector\">List</div>\n		<div class=\"property_map_trigger page_content_selector\">Map</div>\n		<div class=\"property_filter_trigger page_content_selector\">Filter</div>\n	</div>\n</div>";
},"useData":true});
this["goodman"]["template"]["property-list-suggestion-results"] = this["goodman"]["template"]["property-list-suggestion-results"] || {};
this["goodman"]["template"]["property-list-suggestion-results"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = (helpers.if_eq || (depth0 && depth0.if_eq) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(data && data.index),0,{"name":"if_eq","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "		<div class=\"property_list_search_autocomplete_row property_list_search_autocomplete_row_location\" data-suggestion-result=\"0,"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3((helpers.hilite || (depth0 && depth0.hilite) || alias2).call(alias1,(depth0 != null ? depth0.text : depth0),(depths[2] != null ? depths[2].value : depths[2]),{"name":"hilite","hash":{},"data":data}))
    + "</div>		\n";
},"5":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"6":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "		<div class=\"property_list_search_autocomplete_row property_list_search_autocomplete_row_suggestion\" data-suggestion-result=\"1,"
    + alias3(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias3((helpers.hilite || (depth0 && depth0.hilite) || alias2).call(alias1,((stack1 = ((stack1 = (depth0 != null ? depth0.feature : depth0)) != null ? stack1.attributes : stack1)) != null ? stack1.name : stack1),(depths[2] != null ? depths[2].value : depths[2]),{"name":"hilite","hash":{},"data":data}))
    + "</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.results : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
this["goodman"]["template"]["property-list-suggestion-saved"] = this["goodman"]["template"]["property-list-suggestion-saved"] || {};
this["goodman"]["template"]["property-list-suggestion-saved"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.objectid : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<div class=\"property_list_search_autocomplete_row property_list_search_autocomplete_row_suggestion\" data-suggestion-saved=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<div class=\"property_list_search_autocomplete_row property_list_search_autocomplete_row_location\" data-suggestion-saved=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</div>		\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<h2 class=\"property_list_search_autocomplete_title\">Recent searches</h2>\n<div>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
this["goodman"]["template"]["property-map-locality"] = this["goodman"]["template"]["property-map-locality"] || {};
this["goodman"]["template"]["property-map-locality"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.locality : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : (container.nullContext || {}), alias3=helpers.helperMissing, alias4="function";

  return "	<div class=\"property_map_bottom_link column column_"
    + alias1(container.lambda((depths[1] != null ? depths[1].localityColumnSize : depths[1]), depth0))
    + "\" data-locobjid=\""
    + alias1(((helper = (helper = helpers.localityid || (depth0 != null ? depth0.localityid : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"localityid","hash":{},"data":data}) : helper)))
    + "\" data-propobjid=\""
    + alias1(((helper = (helper = helpers.propertyid || (depth0 != null ? depth0.propertyid : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"propertyid","hash":{},"data":data}) : helper)))
    + "\">\n		<div class=\"locality-type\" style=\"background-image:url("
    + alias1((helpers.cleanurl || (depth0 && depth0.cleanurl) || alias3).call(alias2,(depth0 != null ? depth0.iconurl : depth0),{"name":"cleanurl","hash":{},"data":data}))
    + ")\"></div>\n		<div class=\"property_map_bottom_link_title\">\n			<div class=\"property_map_bottom_link_name\">"
    + alias1(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"title","hash":{},"data":data}) : helper)))
    + "</div>\n			"
    + ((stack1 = helpers["if"].call(alias2,(depth0 != null ? depth0.distance : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n		</div>\n	</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "<div class=\"property_map_bottom_link_distance\">"
    + container.escapeExpression((helpers.numberformat || (depth0 && depth0.numberformat) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.distance : depth0),1,".",{"name":"numberformat","hash":{},"data":data}))
    + " kms</div>";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.locality : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
this["goodman"]["template"]["property-map-type"] = this["goodman"]["template"]["property-map-type"] || {};
this["goodman"]["template"]["property-map-type"]["hbs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "	<div class=\"property_map_type_container\"><!--\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.types : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	--></div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	--><div class=\"property_map_type_link\" data-value=\""
    + alias4(((helper = (helper = helpers.value || (depth0 != null ? depth0.value : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"value","hash":{},"data":data}) : helper)))
    + "\">\n		<div class=\"property_map_type_link_name\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</div>\n	</div><!--\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.types : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});